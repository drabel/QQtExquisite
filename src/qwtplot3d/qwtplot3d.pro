# pro file for building the makefile for qwtplot3d
#
QMAKE_PROJECT_NAME = QwtPlot3d

TARGET            = qwtplot3d
TEMPLATE          = lib
CONFIG           += qt warn_on opengl thread zlib debug_and_release
MOC_DIR           = tmp
OBJECTS_DIR       = tmp
INCLUDEPATH       = include
DEPENDPATH        = include src
DESTDIR      			= lib
#DESTDIR      			= ../../../lib
QT += opengl
win32:LIBS += -lopengl32 -lglu32
win32:TEMPLATE    = lib
win32:CONFIG     += dll exceptions
win32:dll:DEFINES    += QT_DLL QWT3D_DLL QWT3D_MAKEDLL
win32:QMAKE_CXXFLAGS     += $$QMAKE_CFLAGS_STL

CONFIG += debug_and_release
CONFIG += build_all

#debug:win32:TARGET = $${TARGET}d

# Comment the next line, if you have zlib on your windows system
win32:CONFIG -= zlib
# Comment
# mac下默认有zlib
# linux下需要自己安装一下 zlib-dev. 删除
linux:CONFIG -= zlib
# 嵌入式情况下，估计也要自己安装zlib. e-linux自带
# android里，估计自带 android下默认没有openGL。

linux-g++:TMAKE_CXXFLAGS += -fno-exceptions
unix:VERSION = 0.2.6

# Input
SOURCES += src/qwt3d_axis.cpp \
           src/qwt3d_color.cpp \
           src/qwt3d_coordsys.cpp \
           src/qwt3d_drawable.cpp \
           src/qwt3d_mousekeyboard.cpp \
           src/qwt3d_movements.cpp \
           src/qwt3d_lighting.cpp \
           src/qwt3d_colorlegend.cpp \
           src/qwt3d_plot.cpp \
           src/qwt3d_label.cpp \
           src/qwt3d_types.cpp \
           src/qwt3d_enrichment_std.cpp \
           src/qwt3d_autoscaler.cpp \
           src/qwt3d_io_reader.cpp \
           src/qwt3d_io.cpp \
           src/qwt3d_scale.cpp

SOURCES += src/qwt3d_gridmapping.cpp \
           src/qwt3d_parametricsurface.cpp \
           src/qwt3d_function.cpp

SOURCES += src/qwt3d_surfaceplot.cpp \
           src/qwt3d_gridplot.cpp \
           src/qwt3d_meshplot.cpp
          

HEADERS += include/qwt3d_color.h \
           include/qwt3d_global.h \
           include/qwt3d_types.h \
           include/qwt3d_axis.h \
           include/qwt3d_coordsys.h \
           include/qwt3d_drawable.h \
           include/qwt3d_helper.h \
           include/qwt3d_label.h \
           include/qwt3d_openglhelper.h \
           include/qwt3d_colorlegend.h \
           include/qwt3d_plot.h \
           include/qwt3d_enrichment.h \
           include/qwt3d_enrichment_std.h \
           include/qwt3d_autoscaler.h \
           include/qwt3d_autoptr.h \
           include/qwt3d_io.h \
           include/qwt3d_io_reader.h \
           include/qwt3d_scale.h \
           include/qwt3d_portability.h
						
HEADERS += include/qwt3d_mapping.h \
           include/qwt3d_gridmapping.h \
           include/qwt3d_parametricsurface.h \
           include/qwt3d_function.h

HEADERS += include/qwt3d_surfaceplot.h \
           include/qwt3d_volumeplot.h \
           include/qwt3d_graphplot.h \
           include/qwt3d_multiplot.h

# gl2ps support
HEADERS+=3rdparty/gl2ps/gl2ps.h \
         include/qwt3d_io_gl2ps.h
         
SOURCES+=src/qwt3d_io_gl2ps.cpp \
         3rdparty/gl2ps/gl2ps.c

# zlib support for gl2ps
zlib {
  DEFINES += GL2PS_HAVE_ZLIB
  win32:LIBS += zlib.lib
	unix:LIBS  += -lz
}


include(../../multi-link/add_base_manager.pri)
system(touch src/qwt3d_surfaceplot.cpp)
add_source_dir($$PWD/include)
add_sdk(QwtPlot3d)

#qwtplot3d e-linux 存在一些编译问题 在此记录
#armhf32, android 都存在
#首先拷贝桌面系统/usr/include/GL/glu.h到arm sdk的usr/include/GL下
#然后,修改arm sdk里的 usr/include/GL/glext.h
##ifndef GLsizeiptr
##define GLsizeiptr ptrdiff_t
##endif
##ifndef GLintptr
##define GLintptr ptrdiff_t
##endif
#//typedef ptrdiff_t GLsizeiptr;
#//typedef ptrdiff_t GLintptr;
#按照如上修改,编译即可通过.

#我在源代码include文件夹里放了一份GL。不用拷贝就能找到头文件。

#可是，还是编译不成功，链接的时候找不到gl函数群，
#这说明缺少so。

#我通过将Multi-link应用于系统C库，将glu编译出来。可以经过Multi-link方便增加对glu的依赖。
