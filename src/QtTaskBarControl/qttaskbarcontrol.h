#ifndef QTTASKBARCONTROL_H
#define QTTASKBARCONTROL_H

#include <QWidget>

#include "qtaskbarcontrol.h"

#include "qttaskbarcontrol_global.h"

/**
 * @brief The QtTaskBarControl class
 * 安装给Widget，必须设置parent为QWidget。
 */
class QTTASKBARCONTROLSHARED_EXPORT QtTaskBarControl : public QTaskbarControl
{
    Q_OBJECT

public:
    explicit QtTaskBarControl ( QWidget* parent = 0 )
        : QTaskbarControl ( parent ) {}
    virtual ~QtTaskBarControl() {}
};

#endif // QTTASKBARCONTROL_H
