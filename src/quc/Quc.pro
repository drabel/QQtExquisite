TARGET              = Quc
TEMPLATE            = lib
RESOURCES           = main.qrc

CONFIG              += qt plugin warn_off
DEFINES             += quc
CONFIG              += build_all

msvc:PRECOMPILED_HEADER  = head.h

include(src/src.pri)

!android:!ios{
    greaterThan(QT_MAJOR_VERSION, 4) {
        QT += designer
    } else {
        QT += xml
        CONFIG += designer
    }
    include(plugin/plugin.pri)
}

include(../../multi-link/add_base_manager.pri)
system(touch src/flatui.cpp)
#message($$CONFIG)
#CONFIG -= lib_bundle
#CONFIG -= create_prl
add_version(1,0,0,0)
add_sdk(Quc)
target.path         = $$get_add_include(Quc)/designer
INSTALLS            += target
