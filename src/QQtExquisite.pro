TEMPLATE = subdirs
CONFIG += ordered

#这个Exquisite是对QQt Exquisite模块的扩展，
#有一些模块是继承在源代码里。
#QQtExquisite
SUBDIRS += QQtExquisite/QQtExquisite.pro


#有一些模块是独立的Library。
#由于Exquisite使用Multi-link技术，我会把独立Library发布到他们的SDK目录。

#Log4Qt
#没有静态库
SUBDIRS += Log4Qt/log4qt.pro

#Quc
#没有静态库
SUBDIRS += quc/Quc.pro

#Qt Xlsx
SUBDIRS += QtXlsxWriter/qtxlsx.pro

#QtPdfium
#macOS, MinGW32, Linux
#iOS, iOSSimulator, Android
#e-linux gcc编译器必须支持c++11才能编译过。 方法：QMAKE_CXX_FLAGS += -std=c++0x
SUBDIRS += qtpdfium

#Qwt
#没有静态库
SUBDIRS += qwt

#Qwt Plot3D
#没有静态库
#android无法编译通过。qwtplot3d依赖openGL，android默认没有openGL
SUBDIRS += qwtplot3d

