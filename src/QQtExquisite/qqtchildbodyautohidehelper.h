#ifndef QQTCHILDBODYAUTOHIDEHELPER_H
#define QQTCHILDBODYAUTOHIDEHELPER_H

#include <QObject>

#include <QWidget>
#include <QEvent>
#include <QPoint>
#include <QTimer>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyAutoHideHelper class
 * 子窗口自动隐藏帮助工具
 *
 * 经常和QQtChildBodyStayOnSideHelper合用。
 */
class QQTEXQUISITESHARED_EXPORT QQtChildBodyAutoHideHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyAutoHideHelper ( QObject* parent = 0 );
    virtual ~QQtChildBodyAutoHideHelper();

    //用户拖着窗口移动的方向
    enum MoveDirection
    {
        None = 0,
        Left,
        Right,
        LeftTop,
        RightTop,
        LeftBottom,
        RightBottom,
        Top,
        Bottom
    };

    enum CornerStyle
    {
        CornerStyle_Default = 0,
        CornerStyle_A,
        CornerStyle_B,
        CornerStyle_Max
    };

    /**
     * 对角上的设置Style，A边，B边，角里，只有四个角有效。
     * LeftTop:
     *  A: TOP
     *  B: LEFT
     * RightTop:
     *  A: TOP
     *  B: RIGHT
     * LeftBottom:
     *  A: LEFT
     *  B: BOTTOM
     * RightBottom:
     *  A: RIGHT
     *  B: BOTTOM
     */
    void setCornerStyle ( MoveDirection direction, CornerStyle style = CornerStyle_Default );
    CornerStyle cornerStyle();

    void setTimerInterval ( int millsecond );
    //default:1s
    int timerInterval();

    void setShowWidth ( int width );
    //default:10
    int showWidth();

    void setEffectWidth ( int width );
    //default:80
    int effectWidth();

protected:
    virtual void enterEvent ( QEvent* event, QWidget* target = 0 );
    virtual void leaveEvent ( QEvent* event, QWidget* target = 0 );
    virtual void checkDirection ( QWidget* target = 0 );
    virtual bool checkIfMousePositionLegal ( QWidget* target = 0 );
    virtual bool checkHideStatus ( QWidget* target = 0 );

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

protected slots:
    //依赖checkDirection，肯定会隐藏起来。
    void slotHide( );
    //依赖checkDirection，如果隐藏肯定会显示出来。
    void slotShow ( );

private:
    MoveDirection direction;
    QWidget* localTarget;

    QTimer* timer;
    int mTimerInterval;

    //显示timer，间隔为尽快
    QTimer* timer2;

    int mShowWidth;
    int mEffectWidth;

    //0 lefttop, 1 leftbottom, 2 righttop, 3 rightbottom
    CornerStyle csArray[4];
};

#endif // QQTCHILDBODYAUTOHIDEHELPER_H

