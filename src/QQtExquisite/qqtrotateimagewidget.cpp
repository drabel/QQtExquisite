﻿#include <qqtrotateimagewidget.h>
#include <QStylePainter>

QQtRotateImageWidget::QQtRotateImageWidget ( QWidget* parent ) : QQtWidget ( parent )
{
    mRotate = 0;

    dx = 0;
    dy = 0;

    dxR = 0;
    dyR = 0;
}

QQtRotateImageWidget::~QQtRotateImageWidget() {}

void QQtRotateImageWidget::setRotate ( qreal rotate )
{
    mRotate  = rotate;
    update();
}

qreal QQtRotateImageWidget::rotate()
{
    return mRotate;
}

void QQtRotateImageWidget::setRotatePos ( qreal dx, qreal dy )
{
    this->dx = dx;
    this->dy = dy;
    update();
}

QPointF QQtRotateImageWidget::rotatePos()
{
    return QPointF ( dx, dy );
}

void QQtRotateImageWidget::setRotatePosRatio ( qreal dxR, qreal dyR )
{
    this->dxR = dxR;
    this->dyR = dyR;
    update();
}

QPointF QQtRotateImageWidget::rotatePosRatio()
{
    return QPointF ( dxR, dyR );
}

void QQtRotateImageWidget::paintEvent ( QPaintEvent* event )
{
    if ( image().isNull() )
        return QQtWidget::paintEvent ( event );

    QStylePainter painter ( this );
    painter.setRenderHint ( QPainter::SmoothPixmapTransform );

    QRect r0 = rect();

    int dx1, dy1;
    dx1 = 0;
    dy1 = 0;

    if ( dx != 0 )
        dx1 = dx;
    if ( dy != 0 )
        dy1 = dy;

    if ( dxR != 0 )
        dx1 = r0.width() * dxR;
    if ( dyR != 0 )
        dy1 = r0.height() * dyR;

    painter.translate ( dx1, dy1 );
    painter.rotate ( mRotate );
    painter.translate ( -dx1, -dy1 );

    //使用这个是不行的。新的painter和旧的painter是不一样的。
    //QQtWidget::paintEvent ( event );

    painter.drawItemPixmap ( rect(), Qt::AlignCenter, QIcon ( QPixmap::fromImage ( image() ) ).pixmap ( rect().size(),
                             QIcon::Normal, QIcon::On ) );

}
