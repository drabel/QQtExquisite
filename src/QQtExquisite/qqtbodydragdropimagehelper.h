﻿#ifndef QQTBODYDRAGDROPIMAGEHELPER
#define QQTBODYDRAGDROPIMAGEHELPER

#include "qqtexquisite_global.h"

#include <QObject>
#include <QMouseEvent>
#include <QWidget>

#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QDropEvent>

#include <QQtWidget>
#include <qqtcore.h>


/**
 * @brief The QQtBodyDragDropImageHelper class
 * 支持在widget与widget之间拷贝图片
 * 支持在QtApp之间拷贝图片，也支持在不同类型App之间，拷贝图片，必须是图片类型
 *
 * 和QQtChildBodyMover冲突
 * 仅仅支持QQtWidget及其子类。
 *
 * 原理：
 * 拖来：            DragEnter、DragMove、DragLeave、 DropEvent
 * 拖去：MousePress、 DragEnter、DragMove、DragLeave、 ChildRemoved
 *
 * 内部有一部分代码注释了，放开那部分代码，就可以加CTRL控制。
 */
class QQTEXQUISITESHARED_EXPORT QQtBodyDragDropImageHelper: public QObject
{
    Q_OBJECT
public:
    explicit QQtBodyDragDropImageHelper ( QObject* parent = 0 );
    virtual ~QQtBodyDragDropImageHelper();

protected:
    virtual void mousePressEvent ( QMouseEvent* event, QWidget* target = 0 );
    virtual void mouseReleaseEvent ( QMouseEvent* event, QWidget* target = 0 );
    virtual void mouseMoveEvent ( QMouseEvent* event, QWidget* target = 0 );

    virtual void dragEnterEvent ( QDragEnterEvent* event, QWidget* target = 0 );
    virtual void dragLeaveEvent ( QDragLeaveEvent* event, QWidget* target = 0 );
    virtual void dragMoveEvent ( QDragMoveEvent* event, QWidget* target = 0 );
    virtual void dropEvent ( QDropEvent* event, QWidget* target = 0 );

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

private:
    bool bMousePressed;
    QPoint pressedPoint;
};

#endif // QQTBODYDRAGDROPIMAGEHELPER
