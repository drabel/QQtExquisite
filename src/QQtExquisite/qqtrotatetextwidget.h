#ifndef QQTROTATETEXTWIDGET_H
#define QQTROTATETEXTWIDGET_H

/*
 * T.D.R. MOD 2019-10-06 19:18:35
 * 更改为LibQQt系列类比较统一的名字QQtRotateTextWidget。
 */

#include <QObject>
#include <QWidget>
#include <QPixmap>
#include <QPoint>
#include <QString>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtRotateTextWidget class
 * 旋转的字控件
 *
 * 效果比较混乱。
 * 起始角度，旋转角度，文字角度 ---功能比较混乱。
 * 旋转半径，旋转圆心，旋转角 ---功能比较稳定。
 */
class QQTEXQUISITESHARED_EXPORT QQtRotateTextWidget : public QWidget
{
    Q_OBJECT
public:
    explicit QQtRotateTextWidget ( QWidget* parent = 0 );

    enum RotateType
    {
        RotateIsSame,
        RotateIsDifferent
    };
    void setRotateType ( int value );
    RotateType rotateType();

    void setText ( const QString& value );

    void setCenterPoint ( const QPoint& value );
    void setRadius ( qreal value );
    void setRotate ( qreal value );

    void setStartAngle ( qreal value );
    qreal startAngle();

    inline qreal rotateAngle() {return mRotateAngle; }
    void setRotateAngle ( qreal value );

protected:
    void paintEvent ( QPaintEvent* event );
    bool event ( QEvent* event );

private:
    void updateImage();

private:
    RotateType     mType;
    QPixmap  mTextPixmap;
    QPoint   mCenterPoint;
    QString  mText;
    qreal    mRotate;
    qreal    mStartAngle;
    qreal    mRotateAngle;
    qreal    mRadius;

};


#endif // QQTROTATETEXTWIDGET_H

