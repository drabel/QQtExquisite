#include <qqtcustompushbutton.h>

#include <QStylePainter>
#include <QStyleOptionButton>
#include <QMouseEvent>

QQtCustomPushButton::QQtCustomPushButton ( QWidget* parent )
    : QPushButton ( parent )
{
    mWorkState = BTN_NORMAL;
}

QQtCustomPushButton::~QQtCustomPushButton() {}

const TBtnImageTable& QQtCustomPushButton::imageTable() const
{
    return mImageTable;
}

TBtnImageTable& QQtCustomPushButton::imageTable()
{
    return mImageTable;
}

void QQtCustomPushButton::translateImage()
{
    int state = mWorkState;

    //qDebug() << isEnabled();

    if ( !isEnabled() )
        state = BTN_DISABLE;

    setImage ( mImageTable[state] );
}

void QQtCustomPushButton::setImage ( const QImage& image )
{
    mImage = image;
    update();
}

int QQtCustomPushButton::workState() const
{
    return mWorkState;
}

void QQtCustomPushButton::setWorkState ( int index )
{
    mWorkState = ( EBtnStatus ) index;
    translateImage();
}


QImage QQtCustomPushButton::stateImage ( int index )
{
    if ( index < BTN_NORMAL || index > BTN_MAX - 1 )
        return mImageTable[BTN_NORMAL];
    return mImageTable[index];
}

void QQtCustomPushButton::setStateImage ( int index, const QImage& image )
{
    if ( index < BTN_NORMAL || index > BTN_MAX - 1 )
        return;
    mImageTable[index] = image;
    translateImage();
}

void QQtCustomPushButton::setNormalImage ( const QImage& normal, const QImage& press )
{
    mImageTable[BTN_NORMAL] = normal;
    mImageTable[BTN_PRESS] = press;
    translateImage();
}

void QQtCustomPushButton::setHoverImage ( const QImage& hover )
{
    mImageTable[BTN_HOVER] = hover;
    translateImage();
}

void QQtCustomPushButton::setDisableImage ( const QImage& disable )
{
    mImageTable[BTN_DISABLE] = disable;
    translateImage();
}

void QQtCustomPushButton::setEnabled ( bool stat )
{
    //qDebug() << stat;
    QPushButton::setEnabled ( stat );
    if ( stat )
        setWorkState ( BTN_NORMAL );
    else
        setWorkState ( BTN_DISABLE );
}

void QQtCustomPushButton::setDisabled ( bool stat )
{
    QPushButton::setDisabled ( stat );
    if ( !stat )
        setWorkState ( BTN_NORMAL );
    else
        setWorkState ( BTN_DISABLE );
}

void QQtCustomPushButton::mousePressEvent ( QMouseEvent* event )
{
    if ( event->button() == Qt::LeftButton )
    {
        mWorkState = BTN_PRESS;
    }
    QPushButton::mousePressEvent ( event );
    translateImage();
}

void QQtCustomPushButton::mouseReleaseEvent ( QMouseEvent* event )
{
    if ( event->button() == Qt::LeftButton )
    {
#ifdef __EMBEDDED_LINUX__
        mWorkState = BTN_NORMAL;
#else
        if ( rect().contains ( event->pos() ) )
            mWorkState = BTN_HOVER;
        else
            mWorkState = BTN_NORMAL;
#endif
    }
    QPushButton::mouseReleaseEvent ( event );
    translateImage();
}

void QQtCustomPushButton::enterEvent ( QEvent* event )
{
    mWorkState = BTN_HOVER;
    QPushButton::enterEvent ( event );
    translateImage();
}

void QQtCustomPushButton::leaveEvent ( QEvent* event )
{
    mWorkState = BTN_NORMAL;
    QPushButton::leaveEvent ( event );
    translateImage();
}


void QQtCustomPushButton::paintEvent ( QPaintEvent* event )
{
    if ( mImage.isNull() )
        return QWidget::paintEvent ( event );

    //qDebug() << isEnabled() << mWorkState;

    QStylePainter p ( this );
    p.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                       /*不.copy() 切出图片的中间部分使用*/
                       QPixmap::fromImage ( mImage
                                            .scaled ( rect().width(), rect().height(), Qt::IgnoreAspectRatio )
                                          ) );

    QStyleOptionButton opt;
    initStyleOption ( &opt );
    p.drawItemText ( rect(), Qt::AlignCenter, opt.palette, isEnabled(), text() );
}
