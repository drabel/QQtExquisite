﻿#ifndef QQTCAROUSELIMAGEWINDOW_H
#define QQTCAROUSELIMAGEWINDOW_H

#include <QWidget>
#include <QScrollArea>
#include <QTimer>
#include <QPropertyAnimation>
#include <QPushButton>

#include <qqtexquisite_global.h>

/**
 * @brief The QQtCarouselImageWindow class
 * 图片轮播控件
 *
 * 只能开启一次，内部的内存管理，只允许开启一次。
 */
class QQTEXQUISITESHARED_EXPORT QQtCarouselImageWindow : public QWidget
{
    Q_OBJECT

public:
    QQtCarouselImageWindow ( QWidget* parent = NULL );
    ~QQtCarouselImageWindow();

    // 设置图片列表;
    void setImageList ( QStringList imageFileNameList );
    // 添加图片;
    void addImage ( QString imageFileName );
    // 开始播放;
    void startPlay();
    //没有图片的时候，显示这张
    void setDefaultPicture ( const QString& picture = ":/Resources/CarouselImageBack.png" );
    //页面按钮大小
    void setButtonSize ( const QSize& size = QSize ( 16, 16 ) );
    //页面按钮常规和选中状态图片
    void setButtonPicture ( const QString& normal = ":/Resources/select1.png",
                            const QString& selected = ":/Resources/select2.png" );
    //图片轮询间隔。动画的间隔是1500ms固定，必须大于1500。
    void setCarouselTime ( int msec = 2000 );
private:
    // 初始化图片切换按钮;
    void initChangeImageButton();
    // 绘图事件;
    void paintEvent ( QPaintEvent* event );

    // 鼠标点击事件;
    void mousePressEvent ( QMouseEvent* event );

public slots:
    // 图片切换时钟;
    void onImageChangeTimeout();

    // 图片切换按钮点击;
    void onImageSwitchButtonClicked ( int buttonId );

private:
    // 用来做图片切换滑动效果，目前以透明度作为切换效果;
    QScrollArea* m_imagePlayWidget;
    // 图片列表;
    QList<QString> m_imageFileNameList;

    // 图片切换时钟;
    QTimer m_imageChangeTimer;

    // 当前显示图片index;
    int m_currentDrawImageIndx;

    // 切换图片;
    QImage m_currentImage;
    QImage m_nextImage;
    // 图片切换动画类;
    QPropertyAnimation* m_opacityAnimation;
    // 按钮列表;
    QList<QPushButton*> m_pButtonChangeImageList;

    QString m_defaultPicture;
    QString m_buttonPic1, m_buttonPic2;
    QSize m_buttonSize;
    int m_carouselTime;
};


#endif //QQTCAROUSELIMAGEWINDOW_H
