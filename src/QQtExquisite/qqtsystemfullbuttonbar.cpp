#include "qqtsystemfullbuttonbar.h"
#include "ui_qqtsystemfullbuttonbar.h"

#ifdef Q_OS_WIN
//#pragma comment(lib, "user32.lib")
#include <qt_windows.h>
#endif
#include <QStyle>
#include <QDebug>

QQtSystemFullButtonBar::QQtSystemFullButtonBar ( QWidget* parent ) :
    QWidget ( parent ),
    ui ( new Ui::QQtSystemFullButtonBar )
{
    ui->setupUi ( this );

    ui->toolButton->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarCloseButton ) );
    ui->toolButton_2->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarMaxButton ) );
    ui->toolButton_3->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarMinButton ) );
    //bug?
    ui->toolButton_4->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarShadeButton ) );
    //bug?
    ui->toolButton_5->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarUnshadeButton ) );

    connect ( ui->toolButton,   SIGNAL ( clicked ( bool ) ), this, SLOT ( onButtonClicked() ) );
    connect ( ui->toolButton_2, SIGNAL ( clicked ( bool ) ), this, SLOT ( onButtonClicked() ) );
    connect ( ui->toolButton_3, SIGNAL ( clicked ( bool ) ), this, SLOT ( onButtonClicked() ) );
    connect ( ui->toolButton_4, SIGNAL ( clicked ( bool ) ), this, SLOT ( onButtonClicked() ) );
    connect ( ui->toolButton_5, SIGNAL ( toggled ( bool ) ), this, SLOT ( onButtonClicked() ) );

    ui->toolButton_5->setCheckable ( true );
    //ui->toolButton_5->setChecked ( false );

    //监视root windows。
    this->window()->installEventFilter ( this );
    //this->window()->setWindowFlags ( Qt::FramelessWindowHint | this->window()->windowFlags() );
}

QQtSystemFullButtonBar::~QQtSystemFullButtonBar()
{
    delete ui;
}

void QQtSystemFullButtonBar::setMenuButtonVisable ( bool visable )
{
    ui->toolButton_5->setVisible ( visable );
}

void QQtSystemFullButtonBar::setFullButtonVisable ( bool visable )
{
    ui->toolButton_4->setVisible ( visable );
}

void QQtSystemFullButtonBar::setMinButtonVisable ( bool visable )
{
    ui->toolButton_3->setVisible ( visable );
}

void QQtSystemFullButtonBar::setMaxButtonVisable ( bool visable )
{
    ui->toolButton_2->setVisible ( visable );
}

void QQtSystemFullButtonBar::setCloseButtonVisable ( bool visable )
{
    ui->toolButton->setVisible ( visable );
}

QRect QQtSystemFullButtonBar::menuButtonRect()
{
    return ui->toolButton_5->rect();
}

void QQtSystemFullButtonBar::resizeEvent ( QResizeEvent* event )
{
    QSize s0 = this->size();
    ui->toolButton->setFixedSize ( s0.height(), s0.height() );
    ui->toolButton_2->setFixedSize ( s0.height(), s0.height() );
    ui->toolButton_3->setFixedSize ( s0.height(), s0.height() );
    ui->toolButton_4->setFixedSize ( s0.height(), s0.height() );
    ui->toolButton_5->setFixedSize ( s0.height(), s0.height() );
    //qDebug() << "aaaa";
}


bool QQtSystemFullButtonBar::eventFilter ( QObject* watched, QEvent* event )
{
    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //qDebug() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        //this->window(), empty? no event...
        case QEvent::WindowStateChange:
        case QEvent::Resize:
        {
            QEvent* e = ( QEvent* ) event;
            windowStateChangeEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        default:
            break;
    }

    return QWidget::eventFilter ( watched, event );
}

void QQtSystemFullButtonBar::windowStateChangeEvent ( QEvent* event, QWidget* target )
{
    QWidget* pWindow = this->window();
    //qDebug() << pWindow->isTopLevel();
    //qDebug() << pWindow->isMaximized();
    if ( pWindow->isTopLevel() )
    {
        bool bMaximize = pWindow->isMaximized();
        if ( bMaximize )
        {
            //ui->toolButton_2->setProperty ( "maximizeProperty", "restore" );
            ui->toolButton_2->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarNormalButton ) );
        }
        else
        {
            //ui->toolButton_2->setProperty ( "maximizeProperty", "maximize" );
            ui->toolButton_2->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarMaxButton ) );
        }
        //ui->toolButton_2->update();
        //ui->toolButton_2->setStyle ( QApplication::style() );
    }
}

void QQtSystemFullButtonBar::onButtonClicked()
{
    QToolButton* pButton = qobject_cast<QToolButton*> ( sender() );
    QWidget* pWindow = this->window();
    if ( pWindow->isTopLevel() )
    {
        if ( pButton == ui->toolButton )
        {
            pWindow->close();
        }
        else if ( pButton == ui->toolButton_2 )
        {
            pWindow->isMaximized() ? pWindow->showNormal() : pWindow->showMaximized();
        }
        else if ( pButton == ui->toolButton_3 )
        {
            pWindow->showMinimized();
        }
        else if ( pButton == ui->toolButton_4 )
        {
            //Qt bug？ 在最大化的时候全屏，恢复的时候，mac下会先showMaxsize状态，然后再变为showNormal状态。在前一状态，isMaximized报告为false，此处有错，应当报告为true。
            //NO！
            //此处注意，fullScreen和maxmized无关。在全屏状态下，获取到的maxmized一直为false。
            //static bool bMax = pWindow->isMaximized();
            //pWindow->isFullScreen() ? ( bMax ? pWindow->showMaximized() : pWindow->showNormal() )
            //: ( bMax = pWindow->isMaximized(), pWindow->showFullScreen() );
            //没用的，windows下还是一样的错误。所以，我觉得还是Qt得bug Qt5.9.2
            pWindow->isFullScreen() ? pWindow->showNormal() : pWindow->showFullScreen();
        }
        else if ( pButton == ui->toolButton_5 )
        {
            emit menuButtonToggled ( ui->toolButton_5->isChecked() );
        }
    }
}
