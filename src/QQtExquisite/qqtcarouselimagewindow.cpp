#include <qqtcarouselimagewindow.h>

#include <QHBoxLayout>
#include <QPainter>
#include <QStylePainter>
#include <QImage>
#include <QPixmap>
#include <QDebug>
#include <QButtonGroup>

QQtCarouselImageWindow::QQtCarouselImageWindow ( QWidget* parent )
    : QWidget ( parent )
    , m_currentDrawImageIndx ( 0 )
{
    m_defaultPicture = ":/Resources/CarouselImageBack.png";
    m_buttonSize = QSize ( 16, 16 );
    m_buttonPic1 = ":/Resources/select1.png";
    m_buttonPic2 = ":/Resources/select2.png";
    m_carouselTime = 2000;

    // 添加ImageOpacity属性;
    this->setProperty ( "ImageOpacity", 1.0 );

    // 动画切换类;
    m_opacityAnimation = new QPropertyAnimation ( this, "ImageOpacity" );
    // 这里要设置的动画时间小于图片切换时间;
    m_opacityAnimation->setDuration ( 1500 );

    // 设置ImageOpacity属性值的变化范围;
    m_opacityAnimation->setStartValue ( 1.0 );
    m_opacityAnimation->setEndValue ( 0.0 );
    // 透明度变化及时更新绘图;
    connect ( m_opacityAnimation, SIGNAL ( valueChanged ( const QVariant& ) ), this, SLOT ( update() ) );
    // 设置图片切换时钟槽函数;
    connect ( &m_imageChangeTimer, SIGNAL ( timeout() ), this, SLOT ( onImageChangeTimeout() ) );

    //this->setFixedSize ( QSize ( 400, 250 ) );
    //this->setWindowFlags ( Qt::FramelessWindowHint );
}

QQtCarouselImageWindow::~QQtCarouselImageWindow()
{

}

void QQtCarouselImageWindow::initChangeImageButton()
{
    // 注意图片过多按钮可能放置不下;
    QButtonGroup* changeButtonGroup = new QButtonGroup;
    QHBoxLayout* hLayout = new QHBoxLayout();
    hLayout->addStretch();
    for ( int i = 0; i < m_imageFileNameList.count(); i++ )
    {
        QPushButton* pButton = new QPushButton;
        pButton->setFixedSize ( m_buttonSize );
        pButton->setCheckable ( true );
        pButton->setStyleSheet ( QString ( "QPushButton{border-image:url(%1);}\
                        QPushButton:checked{border-image:url(%2);}" ).arg ( m_buttonPic1 ).arg ( m_buttonPic2 ) );
        changeButtonGroup->addButton ( pButton, i );
        m_pButtonChangeImageList.append ( pButton );
        hLayout->addWidget ( pButton );
    }
    hLayout->addStretch();
    hLayout->setSpacing ( 10 );
    hLayout->setMargin ( 0 );

    connect ( changeButtonGroup, SIGNAL ( buttonClicked ( int ) ), this, SLOT ( onImageSwitchButtonClicked ( int ) ) );

    QVBoxLayout* mainLayout = new QVBoxLayout ( this );
    mainLayout->addStretch();
    mainLayout->addLayout ( hLayout );
    mainLayout->setContentsMargins ( 0, 0, 0, 20 );
}

void QQtCarouselImageWindow::setImageList ( QStringList imageFileNameList )
{
    m_imageFileNameList = imageFileNameList;
}

void QQtCarouselImageWindow::addImage ( QString imageFileName )
{
    m_imageFileNameList.append ( imageFileName );
    //qDebug() << m_imageFileNameList;
}

void QQtCarouselImageWindow::startPlay()
{
    // 添加完图片之后，根据图片多少设置图片切换按钮;
    initChangeImageButton();
    if ( m_imageFileNameList.count() == 1 )
    {
        m_pButtonChangeImageList[m_currentDrawImageIndx]->setChecked ( true );
    }
    else if ( m_imageFileNameList.count() > 1 )
    {
        m_pButtonChangeImageList[m_currentDrawImageIndx]->setChecked ( true );
        m_currentImage = QImage ( m_imageFileNameList.at ( m_currentDrawImageIndx ) );
        m_imageChangeTimer.start ( m_carouselTime );
        update();
    }
}

void QQtCarouselImageWindow::setDefaultPicture ( const QString& picture )
{
    m_defaultPicture = picture;
}

void QQtCarouselImageWindow::setButtonSize ( const QSize& size )
{
    m_buttonSize = size;
}

void QQtCarouselImageWindow::setButtonPicture ( const QString& normal, const QString& selected )
{
    m_buttonPic1 = normal;
    m_buttonPic2 = selected;
}

void QQtCarouselImageWindow::setCarouselTime ( int msec )
{
    m_carouselTime = msec;
}

void QQtCarouselImageWindow::onImageChangeTimeout()
{
    // 设置前后的图片;
    m_currentImage = QImage ( m_imageFileNameList.at ( m_currentDrawImageIndx ) );
    //qDebug() << "current pic:" << m_imageFileNameList.at ( m_currentDrawImageIndx );
    m_currentDrawImageIndx++;
    if ( m_currentDrawImageIndx >= m_imageFileNameList.count() )
    {
        m_currentDrawImageIndx = 0;
    }
    //qDebug() << "next pic:" << m_imageFileNameList.at ( m_currentDrawImageIndx );
    m_nextImage = QImage ( m_imageFileNameList.at ( m_currentDrawImageIndx ) );

    m_pButtonChangeImageList[m_currentDrawImageIndx]->setChecked ( true );

    // 动画类重新开始;
    m_opacityAnimation->start();
}

void QQtCarouselImageWindow::paintEvent ( QPaintEvent* event )
{
    QStylePainter painter ( this );

    // 如果图片列表为空，显示默认图片;
    if ( m_imageFileNameList.isEmpty() )
    {
        QImage backImage = QImage ( m_defaultPicture );
        painter.drawItemPixmap ( rect(), Qt::AlignCenter, QIcon ( QPixmap::fromImage ( backImage ) ).pixmap ( rect().size(),
                                 QIcon::Normal, QIcon::On ) );
    }
    // 如果只有一张图片;
    else if ( m_imageFileNameList.count() == 1 )
    {
        QImage backImage = QImage ( m_imageFileNameList.first() );
        painter.drawItemPixmap ( rect(), Qt::AlignCenter, QIcon ( QPixmap::fromImage ( backImage ) ).pixmap ( rect().size(),
                                 QIcon::Normal, QIcon::On ) );
    }
    // 多张图片;
    else if ( m_imageFileNameList.count() > 1 )
    {
        float imageOpacity = this->property ( "ImageOpacity" ).toFloat();
        painter.setOpacity ( 1 );
        painter.drawItemPixmap ( rect(), Qt::AlignCenter, QIcon ( QPixmap::fromImage ( m_nextImage ) ).pixmap ( rect().size(),
                                 QIcon::Normal, QIcon::On ) );
        painter.setOpacity ( imageOpacity );
        painter.drawItemPixmap ( rect(), Qt::AlignCenter, QIcon ( QPixmap::fromImage ( m_currentImage ) ).pixmap ( rect().size(),
                                 QIcon::Normal, QIcon::On ) );
    }
}

void QQtCarouselImageWindow::onImageSwitchButtonClicked ( int buttonId )
{
    m_currentDrawImageIndx = buttonId - 1;
    if ( m_currentDrawImageIndx == -1 )
    {
        m_currentDrawImageIndx = m_imageFileNameList.count() - 1;
    }

    onImageChangeTimeout();
    m_imageChangeTimer.start ( m_carouselTime );
    update();
}

void QQtCarouselImageWindow::mousePressEvent ( QMouseEvent* event )
{
    // 这里可以对当前图片进行点击然后触发每个图片对应的效果;
    // 比如web上好多类似的弹出对应的广告页面等功能;
    //qDebug() << m_currentDrawImageIndx;
    return QWidget::mousePressEvent ( event );
}


