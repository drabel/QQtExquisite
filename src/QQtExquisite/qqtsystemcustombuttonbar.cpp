#include "qqtsystemcustombuttonbar.h"
#include "ui_qqtsystemcustombuttonbar.h"

QQtSystemCustomButtonBar::QQtSystemCustomButtonBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QQtSystemCustomButtonBar)
{
    ui->setupUi(this);
}

QQtSystemCustomButtonBar::~QQtSystemCustomButtonBar()
{
    delete ui;
}
