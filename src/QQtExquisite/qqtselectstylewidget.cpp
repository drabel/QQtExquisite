#include <qqtselectstylewidget.h>

#include <QWidget>
#include <QQtWidget>
#include <QPainter>

QQtSelectStyleWidget::QQtSelectStyleWidget ( QWidget* parent ) : QQtWidget ( parent )
{
    hasSelected = false;
    hasStyle = SelectedStyle_QtDesigner;
}

QQtSelectStyleWidget::~QQtSelectStyleWidget() {}


void QQtSelectStyleWidget::setSelectedStyle ( QQtSelectStyleWidget::SelectedStyle style )
{
    hasStyle = style;
}

QQtSelectStyleWidget::SelectedStyle QQtSelectStyleWidget::selectedStyle()
{
    return hasStyle;
}

void QQtSelectStyleWidget::setSelected ( bool bSelected )
{
    hasSelected = bSelected;
}

bool QQtSelectStyleWidget::selectedStatus()
{
    return hasSelected;
}

void QQtSelectStyleWidget::paintEvent ( QPaintEvent* event )
{
    QQtWidget::paintEvent ( event );

    //添加选中效果。
    if ( hasSelected )
    {
        QWidget* target = this;
        QPainter painter ( target );
        int w = 10;
        //如果显示有问题，可以注释掉此处的缩放。
        //只在macOS里发现了右侧和下侧裁切掉了的现象。
        //painter.scale ( ( target->rect().width() - painter.pen().width() ) / target->rect().width(),
        //              ( target->rect().height() - painter.pen().width() ) / target->rect().height() );
        //painter.scale ( 0.999, 0.999 );
        switch ( hasStyle )
        {
            case SelectedStyle_QtDesigner:
            {
                painter.setPen ( Qt::black );
                QRect srcRect = target->rect();
                QRect tarRect = QRect ( srcRect.left() + w / 2, srcRect.top() + w / 2, srcRect.width() - w, srcRect.height() - w );
                painter.drawRect ( tarRect );
                //left top
                painter.drawRect ( 0, 0, w, w );
                //right top
                painter.drawRect ( target->size().width() - w, 0, w, w );
                //left bottom
                painter.drawRect ( 0, target->size().height() - w, w, w );
                //right bottom
                painter.drawRect ( target->size().width() - w, target->size().height() - w, w, w );
                //left
                painter.drawRect ( 0, target->size().height() / 2 - w / 2, w, w );
                //right
                painter.drawRect ( target->size().width() - w, target->size().height() / 2 - w / 2, w, w );
                //top
                painter.drawRect ( target->size().width() / 2 - w / 2, 0, w, w );
                //bottom
                painter.drawRect ( target->size().width() / 2 - w / 2, target->size().height() - w, w, w );
            }
            break;
            case SelectedStyle_QRCodeScaner:
            {
                painter.setPen ( Qt::black );
                painter.drawLine ( 0, 0, w, 0 );
                painter.drawLine ( 0, 0, 0, w );

                painter.drawLine ( target->size().width() - w, 0, target->size().width(), 0 );
                painter.drawLine ( target->size().width(), 0, target->size().width(), w );

                painter.drawLine ( 0, target->size().height() - w, 0, target->size().height() );
                painter.drawLine ( 0, target->size().height(), w, target->size().height() );

                painter.drawLine ( target->size().width(), target->size().height() - w,
                                   target->size().width(), target->size().height() );
                painter.drawLine ( target->size().width() - w, target->size().height(),
                                   target->size().width(), target->size().height() );
            }
            break;
            case SelectedStyle_DottedLine:
            {
                painter.setPen ( Qt::black );
                painter.setPen ( Qt::DotLine );
                painter.drawRect ( target->rect() );
            }
            break;
            case SelectedStyle_FourCheck:
            {
                painter.setPen ( Qt::black );
                painter.drawRect ( target->rect() );
                painter.drawRect ( 0, 0, w, w );
                painter.drawRect ( target->size().width() - w, 0, w, w );
                painter.drawRect ( 0, target->size().height() - w, w, w );
                painter.drawRect ( target->size().width() - w, target->size().height() - w, w, w );
            }
            break;
            default:
                break;
        }
    }
}


void QQtSelectStyleWidget::focusInEvent ( QFocusEvent* event )
{
    hasSelected = true;
    //不需要update
    return QQtWidget::focusInEvent ( event );
}

void QQtSelectStyleWidget::focusOutEvent ( QFocusEvent* event )
{
    hasSelected = false;
    //不需要update
    return QQtWidget::focusOutEvent ( event );
}
