#include <qqtpushbuttonstylehelper.h>

#include <QStylePainter>

QQtPushButtonStyleHelper::QQtPushButtonStyleHelper ( QObject* parent )
    : QObject ( parent )
{
    mWorkState = BTN_NORMAL;
    mTarget = 0;
}

QQtPushButtonStyleHelper::~QQtPushButtonStyleHelper() {}

bool QQtPushButtonStyleHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
    {
        QPaintEvent* e = ( QPaintEvent* ) event;
        paintEvent ( e, qobject_cast<QWidget*> ( watched ) );
        return false;
    }

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::MouseButtonPress:
        {
            QMouseEvent* e = ( QMouseEvent* ) event;
            mousePressEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::MouseButtonRelease:
        {
            QMouseEvent* e = ( QMouseEvent* ) event;
            mouseReleaseEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::Enter:
        {
            QEvent* e = ( QEvent* ) event;
            enterEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::Leave:
        {
            QEvent* e = ( QEvent* ) event;
            leaveEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );

}

const TBtnImageTable& QQtPushButtonStyleHelper::imageTable() const
{
    return mImageTable;
}

TBtnImageTable& QQtPushButtonStyleHelper::imageTable()
{
    return mImageTable;
}

void QQtPushButtonStyleHelper::setTargetWidget ( QWidget* target )
{
    mTarget = target;
}

void QQtPushButtonStyleHelper::translateImage ( QWidget* target )
{
    int state = mWorkState;

    //qDebug() << isEnabled();

    if ( !target->isEnabled() )
        state = BTN_DISABLE;

    setImage ( mImageTable[state] );
}

void QQtPushButtonStyleHelper::setImage ( const QImage& image )
{
    mImage = image;
    //update();
}

void QQtPushButtonStyleHelper::update()
{
    if ( mTarget )
        mTarget->update();
}

int QQtPushButtonStyleHelper::workState() const
{
    return mWorkState;
}

void QQtPushButtonStyleHelper::setWorkState ( int index )
{
    mWorkState = ( EBtnStatus ) index;
    //translateImage();
    //update();
}


QImage QQtPushButtonStyleHelper::stateImage ( int index )
{
    if ( index < BTN_NORMAL || index > BTN_MAX - 1 )
        return mImageTable[BTN_NORMAL];
    return mImageTable[index];
}

void QQtPushButtonStyleHelper::setStateImage ( int index, const QImage& image )
{
    if ( index < BTN_NORMAL || index > BTN_MAX - 1 )
        return;
    mImageTable[index] = image;
    //translateImage();
    update();
}

void QQtPushButtonStyleHelper::setNormalImage ( const QImage& normal, const QImage& press )
{
    mImageTable[BTN_NORMAL] = normal;
    mImageTable[BTN_PRESS] = press;
    //translateImage();
    update();
}

void QQtPushButtonStyleHelper::setHoverImage ( const QImage& hover )
{
    mImageTable[BTN_HOVER] = hover;
    //translateImage();
    update();
}

void QQtPushButtonStyleHelper::setDisableImage ( const QImage& disable )
{
    mImageTable[BTN_DISABLE] = disable;
    //translateImage();
    update();
}

void QQtPushButtonStyleHelper::setEnabled ( bool stat )
{
    //qDebug() << stat;
    //QPushButton::setEnabled ( stat );

    if ( stat )
        setWorkState ( BTN_NORMAL );
    else
        setWorkState ( BTN_DISABLE );
    update();
}

void QQtPushButtonStyleHelper::setDisabled ( bool stat )
{
    //    QPushButton::setDisabled ( stat );
    if ( !stat )
        setWorkState ( BTN_NORMAL );
    else
        setWorkState ( BTN_DISABLE );
    update();
}

void QQtPushButtonStyleHelper::mousePressEvent ( QMouseEvent* event, QWidget* target )
{
    if ( event->button() == Qt::LeftButton )
    {
        mWorkState = BTN_PRESS;
    }
    //translateImage();
    //target->update();
}

void QQtPushButtonStyleHelper::mouseReleaseEvent ( QMouseEvent* event, QWidget* target )
{
    if ( event->button() == Qt::LeftButton )
    {
#ifdef __EMBEDDED_LINUX__
        mWorkState = BTN_NORMAL;
#else
        if ( target->rect().contains ( event->pos() ) )
            mWorkState = BTN_HOVER;
        else
            mWorkState = BTN_NORMAL;
#endif
    }
    //QPushButton::mouseReleaseEvent ( event );
    //translateImage();
    //target->update();
}

void QQtPushButtonStyleHelper::enterEvent ( QEvent* event, QWidget* target )
{
    mWorkState = BTN_HOVER;
    //QPushButton::enterEvent ( event );
    //translateImage();
    target->update();
}

void QQtPushButtonStyleHelper::leaveEvent ( QEvent* event, QWidget* target )
{
    mWorkState = BTN_NORMAL;
    //QPushButton::leaveEvent ( event );
    //translateImage();
    target->update();
}


void QQtPushButtonStyleHelper::paintEvent ( QPaintEvent* event, QWidget* target )
{
    //把imageTable()[stat]设置到mImage。
    translateImage ( target );

    if ( mImage.isNull() )
        return;

    //qDebug() << isEnabled() << mWorkState;

    QStylePainter p ( target );
    p.drawItemPixmap ( target->rect(), Qt::AlignLeft | Qt::AlignTop,
                       /*不.copy() 切出图片的中间部分使用*/
                       QPixmap::fromImage ( mImage
                                            .scaled ( target->rect().width(), target->rect().height(), Qt::IgnoreAspectRatio )
                                          ) );
}
