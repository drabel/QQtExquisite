#include <qqtbodydragdropimagehelper.h>

#include <QDrag>
#include <qqtcore.h>
#include <qqtframe.h>
#include <QQtWidget>

//#define LOCAL_DEBUG
#ifdef LOCAL_DEBUG
#define p3line() p2line()
#else
#define p3line() QNoDebug()
#endif

//#define NEED_CTRL

QQtBodyDragDropImageHelper::QQtBodyDragDropImageHelper ( QObject* parent ) : QObject ( parent )
{
    bMousePressed = false;
}

QQtBodyDragDropImageHelper::~QQtBodyDragDropImageHelper() {}


void QQtBodyDragDropImageHelper::mousePressEvent ( QMouseEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QMargins m_margins = target->contentsMargins();

    //以下代码用来过滤边缘的margin。
#if 1
    //maptoGlobal(rect()) 与 globalPos 对比
    QRect rectMustIn = QRect ( target->mapToGlobal ( target->rect().topLeft() ),
                               target->mapToGlobal ( target->rect().bottomRight() ) );
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = event->globalPos();
    //经过测试，这种方法，在子窗口、root窗口，得到的数据完全正常。
    //pline() << target->geometry() << rectMustIn
    //        << rectMustIn.contains ( event->globalPos() ) << rectMustNotIn.contains ( event->globalPos() );
#endif

    if ( target->isMaximized() ||
         !target->isActiveWindow() ||
         !rectMustIn.contains ( cursorPos ) ||
         !rectMustNotIn.contains ( cursorPos ) )
    {
        event->ignore();
        return;
    }

    if ( event->button() == Qt::LeftButton )
    {
        bMousePressed = true;
        pressedPoint = event->globalPos();
    }
    event->accept();
}

void QQtBodyDragDropImageHelper::mouseReleaseEvent ( QMouseEvent* event, QWidget* target )
{
    bMousePressed = false;
    event->accept();
}

void QQtBodyDragDropImageHelper::mouseMoveEvent ( QMouseEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QQtWidget& w = * ( QQtWidget* ) target;

    //QWidget* win = target->window();
    //pline() << bMousePressed;
    if ( bMousePressed && !target->isMaximized() )
    {
        if ( ( event->globalPos() - pressedPoint ).manhattanLength() >= QApplication::startDragDistance() )
        {
            //我决定把内发的这个放在mouseMove里面，也可以在mousePress里面。使用CTRL控制起来。
            if ( !event->modifiers().testFlag ( Qt::ControlModifier ) )
            {
                //a. ctrl + png out
#ifdef NEED_CTRL
                p3line() << "S. use CTRL + cc this opt.";
                event->ignore();
                return;
#endif
            }

            //这个地方启动了，才会出现下边那三个消息，这里不开始，没有那三个消息。
            //拖来：不用这个，Qt会自动给一个，自动走到了下边的DragEnter里面。
            //拖走：用这个
            //无论如何MouseMove、MouseRelease没有了。
            //看来，Qt内部对Drag进行了测定，如果是外来的，自动生成QDrag，如果是内发的，使用内发的这个。
            QDrag* dg = new QDrag ( target );

            //我把url、image存进了mimedata。
            QMimeData* md = new QMimeData();
            //source make data
#if 0
            QImage image ( w.image() );
            QUuid uuid = QUuid::createUuid();
            QString uuidName = uuid.toString().replace ( "{", "" ).replace ( "}", "" );
            p3line() << uuid.toString() << uuidName;
            QDir ( qApp->applicationDirPath() ).mkdir ( CACHE_PATH );
            QFile file ( conf_cache ( "WidgetScreenShot-" + uuidName + ".png" ) );
            image.save ( file.fileName() );
            p3line() << file.fileName();
            QList<QUrl> urls;
            //urls << QUrl ( "/Users/abel/aaa.png" ) << QUrl ( "/Users/abel/bbb.png" );
            urls << file.fileName();
            md->setUrls ( urls );
#endif
            md->setImageData ( w.image() );

            dg->setMimeData ( md );
            //好像exec和start没有区别
            if ( dg->exec ( Qt::CopyAction | Qt::MoveAction | Qt::LinkAction ) == Qt::CopyAction )
            {
                p3line() << "AAA"; //本示例没有可删除的原始数据，因此只简单输出字符串用于测试。
            }
            else
            {
                p3line() << "BBB"; //本示例没有可删除的原始数据，因此只简单输出字符串用于测试。
            }

            bMousePressed = false; //why?release消息哪里去了？
            p3line() << dg->defaultAction();
            p3line() << dg->source() << dg->target();
            p3line() << dg;
            //此处导致一个问题：还没有从widget拖出去保存到桌面上保存成文件，内部临时文件就被删除了。导致拷贝失败，报错
            //file.remove() 上述问题
            //file.deleteLater() 居然没删除，为什么。
            //不加以删除，程序目录下会有很多图片文件。
            //file.remove();
            //file.deleteLater();
            //提供一个接口到外边去，用户启动程序时自行删除。NO
            //在从文件拖来的时候，不会有childRemoved
            //在拖走到文件的时候，会有childRemoved。
            //在控件之间拖动的时候，src widget总是会收到childRemoved，参数就是QDrag。
            //所以，也就是说，这里，不是删除资源的正确位置
            //结贴

            //是这里，但是有个条件。这个条件代表，肯定是从窗体拖出到文件。源文件会自动被删除。原因在于内部不知道什么原因把DropAction设置为moveAction。强制的。不知道什么原因。
            //在窗体之间却一直都是CopyAction
            if ( dg->target() == 0 )
            {
                //这个时候，我在mac电脑里，系统自动把源图片给删除了，原因未知啊。
            }
            if ( dg->target() != 0 )
            {
                //这个时候，肯定是窗体之间
                //删除文件！
                //file.remove();
                //我决定把bodydragdropwithpngfile，和bodydragdrophelper分开
            }

            md->deleteLater();
            dg->deleteLater();
        }
    }
    event->accept();
}

void QQtBodyDragDropImageHelper::dragEnterEvent ( QDragEnterEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QWidget& w = *target;

    QRect rectMustIn = QRect ( w.mapToGlobal ( w.rect().topLeft() ), w.mapToGlobal ( w.rect().bottomRight() ) );//srcR0
    QMargins m_margins = w.contentsMargins();
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = QCursor::pos();//globalPos

    //在content里面才能响应 [比较的时候不放大坐标，调试出来的]
    if ( rectMustIn.contains ( cursorPos ) && !rectMustNotIn.contains ( cursorPos ) )
    {
        //event->ignore();
        //return;
    }

    if ( !event->keyboardModifiers().testFlag ( Qt::ControlModifier ) )
    {
#ifdef NEED_CTRL
        p3line() << "1. use CTRL+ exec this opt.";
        //1. control + png in
        event->ignore();
        return;
#endif
    }

    QDragEnterEvent* e = ( QDragEnterEvent* ) event;
    p3line() << e->answerRect();
    //---------p2line() << e->dropAction();
    p3line() << e->spontaneous();
    p3line() << e->source();
    if ( e->source() )
        p3line() << e->source()->objectName();
    p3line() << e->mimeData()->formats() << e->mimeData()->urls();
    p3line() << e->possibleActions() << e->proposedAction();

    //无论来、去，什么类型都接受。
#if 0
    if ( !e->mimeData()->hasUrls() )
    {
        e->ignore();
        return;
    }
    p3line() << e->mimeData()->urls();
#endif

#if 0
    if ( !e->mimeData()->hasImage() )
    {
        e->ignore();
        return;
    }
    //p3line() << e->mimeData()->imageData();
#endif

    //必要的
    event->accept();
    return;
}

void QQtBodyDragDropImageHelper::dragLeaveEvent ( QDragLeaveEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QWidget& w = *target;

    if ( !target->isActiveWindow() )
    {
        //event->ignore();
        //return;
    }

    QRect rectMustIn = QRect ( w.mapToGlobal ( w.rect().topLeft() ), w.mapToGlobal ( w.rect().bottomRight() ) );//srcR0
    QMargins m_margins = w.contentsMargins();
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = QCursor::pos();//globalPos

    //在content里面才能响应 [比较的时候不放大坐标，调试出来的]
    if ( rectMustIn.contains ( cursorPos ) && rectMustNotIn.contains ( cursorPos ) )
    {
    }

    if ( !event->isAccepted() )
    {
        //如果有需要，用户自行添加代码
#ifdef NEED_CTRL
        p3line() << "A. use CTRL+ exec this opt.";
        event->ignore();
        return;
#endif
    }

    //别崩溃就行
    QDragLeaveEvent* e = ( QDragLeaveEvent* ) event;
    p3line() << e->spontaneous();

    //event->accept();
    return;
}

void QQtBodyDragDropImageHelper::dragMoveEvent ( QDragMoveEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QWidget& w = *target;

    QRect rectMustIn = QRect ( w.mapToGlobal ( w.rect().topLeft() ), w.mapToGlobal ( w.rect().bottomRight() ) );//srcR0
    QMargins m_margins = w.contentsMargins();
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = QCursor::pos();//globalPos

    //在content里面才能响应 [比较的时候不放大坐标，调试出来的]
    if ( rectMustIn.contains ( cursorPos ) && !rectMustNotIn.contains ( cursorPos ) )
    {
        //event->ignore();
        //return;
    }

    if ( !event->keyboardModifiers().testFlag ( Qt::ControlModifier ) )
    {
        //2. ctrl +- 不同的形态
#ifdef NEED_CTRL
        p3line() << "2. use CTRL+ exec this opt.";
        event->ignore();
        return;
#endif
    }

    //别崩溃就行
    QDragMoveEvent* e = ( QDragMoveEvent* ) event;
    if ( e->keyboardModifiers() == Qt::CTRL )
        e->setDropAction ( Qt::CopyAction );
    else if ( e->keyboardModifiers() == Qt::SHIFT )
        e->setDropAction ( Qt::MoveAction );
    else if ( e->keyboardModifiers() == Qt::ALT )
        e->setDropAction ( Qt::LinkAction );
    else
        //默认拷贝
        e->setDropAction ( Qt::CopyAction );


    event->accept();
    return;
}

void QQtBodyDragDropImageHelper::dropEvent ( QDropEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QQtWidget& w = * ( QQtWidget* ) target;

    QRect rectMustIn = QRect ( w.mapToGlobal ( w.rect().topLeft() ), w.mapToGlobal ( w.rect().bottomRight() ) );//srcR0
    QMargins m_margins = w.contentsMargins();
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = QCursor::pos();//globalPos

    //在content里面才能响应 [比较的时候不放大坐标，调试出来的]
    if ( rectMustIn.contains ( cursorPos ) && !rectMustNotIn.contains ( cursorPos ) )
    {
        //p2line();
        //event->ignore();
        //return;
    }

    QDropEvent* e = ( QDropEvent* ) event;
    p3line();
    //p2line() << e->dropAction();
    p3line() << e->spontaneous();
    p3line() << "source:" << e->source();
    if ( e->source() )
        p3line() << "source:" << e->source()->objectName();
    p3line() << e->mimeData()->formats() << e->mimeData()->urls();
    p3line() << e->possibleActions() << e->proposedAction();
    p3line() << "target:" << target;
    if ( target )
        p3line() << "target:" << target->objectName();

    //来的，如果从自己来的忽略掉
    if ( e->source() == target )
    {
        //p2line();
        e->ignore();
        return;
    }

    //来的，如果有url就用url，如果有image就用image。这样跨Qt App也没问题。
    //url必须是个文件系统上能找到的，绝对路径最能行。
    //target use data
#if 0
    if ( e->mimeData()->hasUrls() )
    {
        if ( e->mimeData()->urls().size() > 0 )
        {
            p3line() << e->mimeData()->urls();
            QUrl url = e->mimeData()->urls() [0];
            QString filename = url.url ( QUrl::FormattingOptions ( QUrl::PreferLocalFile | QUrl::PrettyDecoded ) );
            p3line() << filename;

            QMimeDatabase db;
            QMimeType subFileMimeType = db.mimeTypeForFile ( filename ); //根据前面定义的文件名（含后缀）
            QString fileMimeType = subFileMimeType.name();  //使用name()将MimeType类型转为字符串类型
            p3line() << fileMimeType;

            //if ( filename.toLower().endsWith ( ".png" ) )
            w.setPixmap ( filename );
        }
    }
#endif

#if 1
    if ( e->mimeData()->hasImage() )
        w.setImage ( e->mimeData()->imageData().value<QImage>() );
#endif

    //这里决定了QDrag exec的返回值
    //e->setDropAction ( Qt::MoveAction );
    //e->setDropAction ( Qt::CopyAction );

    e->accept();
    return;
}

bool QQtBodyDragDropImageHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QQtWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    //if ( event->type() == QEvent::MouseMove )
    //    return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::MouseButtonPress:
        {
            QMouseEvent* e = ( QMouseEvent* ) event;
            mousePressEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::MouseButtonRelease:
        {
            QMouseEvent* e = ( QMouseEvent* ) event;
            mouseReleaseEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::MouseMove:
        {
            QMouseEvent* e = ( QMouseEvent* ) event;
            mouseMoveEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::DragEnter:
        {
            QDragEnterEvent* e = ( QDragEnterEvent* ) event;
            dragEnterEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        break;
        case QEvent::DragLeave:
        {
            QDragLeaveEvent* e = ( QDragLeaveEvent* ) event;
            dragLeaveEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        break;
        case QEvent::DragMove:
        {
            QDragMoveEvent* e = ( QDragMoveEvent* ) event;
            dragMoveEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        break;
        case QEvent::Drop:
        {
            QDropEvent* e = ( QDropEvent* ) event;
            dropEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        break;
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}

