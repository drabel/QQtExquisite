﻿#ifndef QQTDRAGFROMFILEHELPER
#define QQTDRAGFROMFILEHELPER

#include "qqtexquisite_global.h"

#include <QObject>
#include <QMouseEvent>
#include <QWidget>

#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QDropEvent>

#include <QQtWidget>
#include <qqtcore.h>


/**
 * @brief The QQtDragFromFileHelper class
 * 支持把文件拖入窗体，发射信号给用户处理
 *
 * 和QQtChildBodyMover冲突
 * 支持QWidget以及其子类
 *
 * 原理：
 * 拖来：            DragEnter、DragMove、DragLeave、 DropEvent
 * 拖去：MousePress、 DragEnter、DragMove、DragLeave、 ChildRemoved
 *
 * 内部有一部分代码注释了，放开那部分代码，就可以加CTRL控制。
 */
class QQTEXQUISITESHARED_EXPORT QQtDragFromFileHelper: public QObject
{
    Q_OBJECT
public:
    explicit QQtDragFromFileHelper ( QObject* parent = 0 );
    virtual ~QQtDragFromFileHelper();

signals:
    void dragFileName ( QString fileName, Qt::DropActions action = Qt::CopyAction );

protected:
    virtual void dragEnterEvent ( QDragEnterEvent* event, QWidget* target = 0 );
    virtual void dragLeaveEvent ( QDragLeaveEvent* event, QWidget* target = 0 );
    virtual void dragMoveEvent ( QDragMoveEvent* event, QWidget* target = 0 );
    virtual void dropEvent ( QDropEvent* event, QWidget* target = 0 );

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

private:
    bool bMousePressed;
    QPoint pressedPoint;
};

#endif // QQTDRAGFROMFILEHELPER
