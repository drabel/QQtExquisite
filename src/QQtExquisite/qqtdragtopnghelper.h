﻿#ifndef QQTDRAGTOPNGHELPER
#define QQTDRAGTOPNGHELPER

#include "qqtexquisite_global.h"

#include <QObject>
#include <QMouseEvent>
#include <QWidget>

#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QDropEvent>

#include <QQtWidget>
#include <qqtcore.h>


/**
 * @brief The QQtDragToPngHelper class
 * 支持把图片文件拖入窗体
 * 支持把控件拖到桌面上保存为图片文件
 * 支持窗体内部widget与widget之间拷贝图片
 * 支持在Qt App之间拷贝图片，也支持在不同类型App之间，必须是图片类型，或者可以找到的图片文件类型
 *
 * 和QQtChildBodyMover冲突
 * 仅仅支持QQtWidget及其子类。
 * 大图需谨慎，比较卡，但是能成功显示。
 * 这个类存在一个问题：在config_root()目录下会出现很多WidgetScreenShot-<uuid>.png的图片文件，用户需要在启动APP的时候手动清理。
 *
 * 原理：
 * 拖来：            DragEnter、DragMove、DragLeave、 DropEvent
 * 拖去：MousePress、 DragEnter、DragMove、DragLeave、 ChildRemoved
 *
 * 内部有一部分代码注释了，放开那部分代码，就可以加CTRL控制。
 * 这个类是附近几个类的母体。---QQtDragFromFileHelper,QQtBodyDragDropImageHelper
 * 这个类另一个名字：QQtDragDropImageFileHelper
 * 也支持和file交互，附加支持窗体之间交互。
 */
class QQTEXQUISITESHARED_EXPORT QQtDragToPngHelper: public QObject
{
    Q_OBJECT
public:
    explicit QQtDragToPngHelper ( QObject* parent = 0 );
    virtual ~QQtDragToPngHelper();

protected:
    virtual void mousePressEvent ( QMouseEvent* event, QWidget* target = 0 );
    virtual void mouseReleaseEvent ( QMouseEvent* event, QWidget* target = 0 );
    virtual void mouseMoveEvent ( QMouseEvent* event, QWidget* target = 0 );

    virtual void dragEnterEvent ( QDragEnterEvent* event, QWidget* target = 0 );
    virtual void dragLeaveEvent ( QDragLeaveEvent* event, QWidget* target = 0 );
    virtual void dragMoveEvent ( QDragMoveEvent* event, QWidget* target = 0 );
    virtual void dropEvent ( QDropEvent* event, QWidget* target = 0 );

    virtual void childRemovedEvent ( QChildEvent* event, QWidget* target = 0 );

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

private:
    bool bMousePressed;
    QPoint pressedPoint;
};

#endif // QQTDRAGTOPNGHELPER
