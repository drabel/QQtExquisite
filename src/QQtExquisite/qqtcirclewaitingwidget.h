#ifndef QQTCIRCLEWAITINGWIDGET_H
#define QQTCIRCLEWAITINGWIDGET_H

#include <QWidget>
#include <qqtcirclewaitinghelper.h>

#include <qqtexquisite_global.h>
class QQTEXQUISITESHARED_EXPORT QQtCircleWaitingWidget : public QWidget
{
    Q_OBJECT

public:
    explicit QQtCircleWaitingWidget ( QWidget* parent = 0 )
        : QWidget ( parent ) {
        installEventFilter ( new QQtCircleWaitingHelper ( this ) );
    }
    virtual ~QQtCircleWaitingWidget() {}

protected:

private:

};

#endif // QQTCIRCLEWAITINGWIDGET_H

