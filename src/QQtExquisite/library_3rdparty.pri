#此文件用于指示版权

SOURCES += \
    $$PWD/qqtcustompianokeyboard.cpp \
    $$PWD/qqtcustomspeedmeter.cpp \
    $$PWD/qqtrulerslider.cpp \
    $$PWD/qqtarrowwidget.cpp \
    $$PWD/qqtnavigationwidget.cpp

HEADERS += \
    $$PWD/qqtcustompianokeyboard.h \
    $$PWD/qqtcustomspeedmeter.h \
    $$PWD/qqtrulerslider.h \
    $$PWD/qqtarrowwidget.h \
    $$PWD/qqtnavigationwidget.h

add_file(qqtcarouselimagewindow.cpp)
add_file(qqtcarouselimagewindow.h)
SOURCES += \
    $$PWD/qqtcarouselimagewindow.cpp
HEADERS += \
    $$PWD/qqtcarouselimagewindow.h

add_file(qqttoolmagazinewidget.cpp)
add_file(qqttoolmagazinewidget.h)
SOURCES += \
    $$PWD/qqttoolmagazinewidget.cpp
HEADERS += \
    $$PWD/qqttoolmagazinewidget.h

add_object_class(QQtRotateTextWidget)
SOURCES += \
    $$PWD/qqtrotatetextwidget.cpp
HEADERS += \
    $$PWD/qqtrotatetextwidget.h

add_widget_class(QQtCharacterWidget)
SOURCES += \
    $$PWD/qqtcharacterwidget.cpp
HEADERS += \
    $$PWD/qqtcharacterwidget.h

add_widget_class(QQtGifCharacterWidget)
SOURCES += \
    $$PWD/qqtgifcharacterwidget.cpp
HEADERS += \
    $$PWD/qqtgifcharacterwidget.h
