#ifndef QQTCIRCLEWAITINGHELPER_H
#define QQTCIRCLEWAITINGHELPER_H

#include <QObject>
#include <QEvent>
#include <QWidget>
#include <qqtexquisite_global.h>



class QQTEXQUISITESHARED_EXPORT QQtCircleWaitingHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtCircleWaitingHelper ( QObject* parent = 0 );
    virtual ~QQtCircleWaitingHelper();

protected:
    int round ( double value );
    double cos ( double degree );
    double sin ( double degree );
    //转化为弧
    double CovertRadian ( double degree );

#if 0
private:
    const double Indicators_Offet = 11.25; //每个指示器中间的间隔
    const int Control_Height = 100;        //默认的控件高度
    const int Control_Offset = 20;
    const int Indicator_Size = 6;           //制丝器的数量
    const double Start_At = 120.0;          //开始的角度
    //三角函数数组
    double[] sine = new double[1440];
    double[] cosine = new double[1440];
    Indicator[] indicators = new Indicator[Indicator_Size]
    QTimer* mTimer;
    int indicator_diameter = 0; //指示器的直径
    int indicator_radius = 0; //指示器的半径
    int outer_radius = 0;     //外半径
    int inner_radius = 0;     //内半径
    int indicator_center_radius = 0; //两个圆心的距离


#endif
    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTCIRCLEWAITINGHELPER_H

