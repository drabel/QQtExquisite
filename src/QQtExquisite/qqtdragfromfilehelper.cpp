#include <qqtdragfromfilehelper.h>

#include <QDrag>
#include <qqtcore.h>
#include <qqtframe.h>
#include <QFile>

//#define LOCAL_DEBUG
#ifdef LOCAL_DEBUG
//#define p3line() p2line()
#else
#define p3line() QNoDebug()
#endif

//#define NEED_CTRL

QQtDragFromFileHelper::QQtDragFromFileHelper ( QObject* parent ) : QObject ( parent )
{
    bMousePressed = false;
}

QQtDragFromFileHelper::~QQtDragFromFileHelper() {}


void QQtDragFromFileHelper::dragEnterEvent ( QDragEnterEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QWidget& w = *target;

    QRect rectMustIn = QRect ( w.mapToGlobal ( w.rect().topLeft() ), w.mapToGlobal ( w.rect().bottomRight() ) );//srcR0
    QMargins m_margins = w.contentsMargins();
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = QCursor::pos();//globalPos

    //在content里面才能响应 [比较的时候不放大坐标，调试出来的]
    if ( rectMustIn.contains ( cursorPos ) && !rectMustNotIn.contains ( cursorPos ) )
    {
        //event->ignore();
        //return;
    }

    if ( !event->keyboardModifiers().testFlag ( Qt::ControlModifier ) )
    {
#ifdef NEED_CTRL
        p3line() << "1. use CTRL+ exec this opt.";
        //1. control + png in
        event->ignore();
        return;
#endif
    }

    QDragEnterEvent* e = ( QDragEnterEvent* ) event;
    p3line() << e->answerRect();
    //---------p2line() << e->dropAction();
    p3line() << e->spontaneous();
    p3line() << e->source();
    if ( e->source() )
        p3line() << e->source()->objectName();
    p3line() << e->mimeData()->formats() << e->mimeData()->urls();
    p3line() << e->possibleActions() << e->proposedAction();

    //无论来、去，什么类型都接受。
#if 0
    if ( !e->mimeData()->hasUrls() )
    {
        e->ignore();
        return;
    }
    p3line() << e->mimeData()->urls();
#endif

#if 0
    if ( !e->mimeData()->hasImage() )
    {
        e->ignore();
        return;
    }
    //p3line() << e->mimeData()->imageData();
#endif

    //必要的
    event->accept();
    return;
}

void QQtDragFromFileHelper::dragLeaveEvent ( QDragLeaveEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QWidget& w = *target;

    if ( !target->isActiveWindow() )
    {
        //event->ignore();
        //return;
    }

    QRect rectMustIn = QRect ( w.mapToGlobal ( w.rect().topLeft() ), w.mapToGlobal ( w.rect().bottomRight() ) );//srcR0
    QMargins m_margins = w.contentsMargins();
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = QCursor::pos();//globalPos

    //在content里面才能响应 [比较的时候不放大坐标，调试出来的]
    if ( rectMustIn.contains ( cursorPos ) && rectMustNotIn.contains ( cursorPos ) )
    {
    }

    if ( !event->isAccepted() )
    {
        //如果有需要，用户自行添加代码
#ifdef NEED_CTRL
        p3line() << "A. use CTRL+ exec this opt.";
        event->ignore();
        return;
#endif
    }

    //别崩溃就行
    QDragLeaveEvent* e = ( QDragLeaveEvent* ) event;
    p3line() << e->spontaneous();

    //event->accept();
    return;
}

void QQtDragFromFileHelper::dragMoveEvent ( QDragMoveEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QWidget& w = *target;

    QRect rectMustIn = QRect ( w.mapToGlobal ( w.rect().topLeft() ), w.mapToGlobal ( w.rect().bottomRight() ) );//srcR0
    QMargins m_margins = w.contentsMargins();
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = QCursor::pos();//globalPos

    //在content里面才能响应 [比较的时候不放大坐标，调试出来的]
    if ( rectMustIn.contains ( cursorPos ) && !rectMustNotIn.contains ( cursorPos ) )
    {
        //event->ignore();
        //return;
    }

    if ( !event->keyboardModifiers().testFlag ( Qt::ControlModifier ) )
    {
        //2. ctrl +- 不同的形态
#ifdef NEED_CTRL
        p3line() << "2. use CTRL+ exec this opt.";
        event->ignore();
        return;
#endif
    }

    //别崩溃就行
    QDragMoveEvent* e = ( QDragMoveEvent* ) event;
    if ( e->keyboardModifiers() == Qt::CTRL )
        e->setDropAction ( Qt::CopyAction );
    else if ( e->keyboardModifiers() == Qt::SHIFT )
        e->setDropAction ( Qt::MoveAction );
    else if ( e->keyboardModifiers() == Qt::ALT )
        e->setDropAction ( Qt::LinkAction );
    else
        //默认拷贝
        e->setDropAction ( Qt::CopyAction );


    event->accept();
    return;
}

void QQtDragFromFileHelper::dropEvent ( QDropEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QWidget& w = * ( QWidget* ) target;

    QRect rectMustIn = QRect ( w.mapToGlobal ( w.rect().topLeft() ), w.mapToGlobal ( w.rect().bottomRight() ) );//srcR0
    QMargins m_margins = w.contentsMargins();
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = QCursor::pos();//globalPos

    //在content里面才能响应 [比较的时候不放大坐标，调试出来的]
    if ( rectMustIn.contains ( cursorPos ) && !rectMustNotIn.contains ( cursorPos ) )
    {
        //p2line();
        //event->ignore();
        //return;
    }

    QDropEvent* e = ( QDropEvent* ) event;
    p3line();
    //p2line() << e->dropAction();
    p3line() << e->spontaneous();
    p3line() << "source:" << e->source();
    if ( e->source() )
        p3line() << "source:" << e->source()->objectName();
    p3line() << e->mimeData()->formats() << e->mimeData()->urls();
    p3line() << e->possibleActions() << e->proposedAction() << e->dropAction();
    p3line() << "target:" << target;
    if ( target )
        p3line() << "target:" << target->objectName();

    //来的，如果从自己来的忽略掉
    if ( e->source() == target )
    {
        //p2line();
        e->ignore();
        return;
    }

    //来的，如果有url就用url，如果有image就用image。这样跨Qt App也没问题。
    //url必须是个文件系统上能找到的，绝对路径最能行。
    //target use data
#if 1
    if ( e->mimeData()->hasUrls() )
    {
        if ( e->mimeData()->urls().size() > 0 )
        {
            p3line() << e->mimeData()->urls();
            QUrl url = e->mimeData()->urls() [0];
            QString filename = url.url ( QUrl::FormattingOptions ( QUrl::PreferLocalFile | QUrl::PrettyDecoded ) );
            p3line() << filename;

            QMimeDatabase db;
            QMimeType subFileMimeType = db.mimeTypeForFile ( filename ); //根据前面定义的文件名（含后缀）
            QString fileMimeType = subFileMimeType.name();  //使用name()将MimeType类型转为字符串类型
            p3line() << fileMimeType;

            //if ( filename.toLower().endsWith ( ".png" ) )
            //w.setPixmap ( filename );
            emit dragFileName ( filename, e->dropAction() );
        }
    }
#endif

#if 0
    if ( e->mimeData()->hasImage() )
        w.setImage ( e->mimeData()->imageData().value<QImage>() );
#endif


    //这里决定了QDrag exec的返回值
    //e->setDropAction ( Qt::MoveAction );
    //e->setDropAction ( Qt::CopyAction );

    e->accept();
    return;
}

bool QQtDragFromFileHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    //if ( event->type() == QEvent::MouseMove )
    //    return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p2line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::DragEnter:
        {
            QDragEnterEvent* e = ( QDragEnterEvent* ) event;
            dragEnterEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        break;
        case QEvent::DragLeave:
        {
            QDragLeaveEvent* e = ( QDragLeaveEvent* ) event;
            dragLeaveEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        break;
        case QEvent::DragMove:
        {
            QDragMoveEvent* e = ( QDragMoveEvent* ) event;
            dragMoveEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        break;
        case QEvent::Drop:
        {
            QDropEvent* e = ( QDropEvent* ) event;
            dropEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        break;
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}

