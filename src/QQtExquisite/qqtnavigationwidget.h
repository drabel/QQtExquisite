﻿#ifndef QNAVIGATIONWIDGET_H
#define QNAVIGATIONWIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <qqtexquisite_global.h>

/**
 * @brief The QQtNavigationWidget class
 * 简单的导航栏
 *
 * 可以用作导航，
 * 可以用做右键菜单
 */
class QQTEXQUISITESHARED_EXPORT QQtNavigationWidget : public QWidget
{
    Q_OBJECT

public:
    QQtNavigationWidget ( QWidget* parent = 0 );
    ~QQtNavigationWidget();

    void addItem ( const QString& title );
    void setWidth ( const int& width );
    void setBackgroundColor ( const QString& color );
    void setSelectColor ( const QString& color );
    void setRowHeight ( const int& height );

protected:
    void paintEvent ( QPaintEvent* );
    void mouseMoveEvent ( QMouseEvent* );
    void mousePressEvent ( QMouseEvent* );
    void mouseReleaseEvent ( QMouseEvent* );

private:
    QList<QString> listItems;
    QList<QString> selectItems;
    QString backgroundColor;
    QString selectedColor;
    int rowHeight;

signals:
    void currentItemChanged ( const int& index );
};

#endif
