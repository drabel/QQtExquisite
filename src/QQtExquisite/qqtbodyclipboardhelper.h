﻿#ifndef QQTBODYCLIPBOARDHELPER_H
#define QQTBODYCLIPBOARDHELPER_H

#include "qqtexquisite_global.h"

#include <QObject>
#include <QKeyEvent>
#include <QQtWidget>
#include <QClipboard>


/**
 * @brief The QQtBodyClipBoardHelper class
 * 对QQtWidget及其子类支持CTRL-C CTRL-V，复制粘贴剪贴板里的图片
 */
class QQTEXQUISITESHARED_EXPORT QQtBodyClipBoardHelper: public QObject
{
    Q_OBJECT
public:
    explicit QQtBodyClipBoardHelper ( QObject* parent = 0 );
    virtual ~QQtBodyClipBoardHelper();

protected:
    virtual void keyPressEvent ( QKeyEvent* event, QWidget* target = 0 );

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

private:
    QClipboard* m_pClipBoard;
};

#endif // QQTBODYCLIPBOARDHELPER_H
