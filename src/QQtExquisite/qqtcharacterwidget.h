#ifndef QQTCHARACTERWIDGET_H
#define QQTCHARACTERWIDGET_H

#include <QQtWidget>

#include <qqtexquisite_global.h>

/**
 * @brief The QQtCharacterWidget class
 * 把图片转换为字符图片，然后显示。
 */
class QQTEXQUISITESHARED_EXPORT QQtCharacterWidget : public QQtWidget
{
    Q_OBJECT

public:
    explicit QQtCharacterWidget ( QWidget* parent = 0 );
    virtual ~QQtCharacterWidget();

    void setCharactorImage ( const QImage& image );

protected:
    virtual char makeChar ( int g );
    virtual int rgbtoGray ( int r, int g, int b );

private:

};

#endif // QQTCHARACTERWIDGET_H

