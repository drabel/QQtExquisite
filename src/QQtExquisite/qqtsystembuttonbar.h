#ifndef QQTSYSTEMBUTTONBAR_H
#define QQTSYSTEMBUTTONBAR_H

#include <QWidget>
#include <qqtexquisite_global.h>

namespace Ui {
class QQtSystemButtonBar;
}

/**
 * @brief The QQtSystemButtonBar class
 * 系统按钮栏。右侧用。最小、最大、关闭。
 *
 * 允许修改layoutSpacing，调整按钮间距；
 * 不允许修改按钮大小，按钮大小会随着控件高度自动变化，正方形；size.width * 3 <= widget.size.width
 *
 * 如果修改基类，可以方便的为整个控件设置背景图片
 * 如果允许设置自定义图片，可以支持设置置灰。min/max/restore/close Disable Off?
 * 如果能增加菜单、全屏按钮
 */
class QQTEXQUISITESHARED_EXPORT QQtSystemButtonBar : public QWidget
{
    Q_OBJECT

public:
    explicit QQtSystemButtonBar ( QWidget* parent = 0 );
    virtual ~QQtSystemButtonBar();

    //是否可见
    void setMinButtonVisable ( bool visable );
    void setMaxButtonVisable ( bool visable );
    void setCloseButtonVisable ( bool visable );

private:
    Ui::QQtSystemButtonBar* ui;

    // QWidget interface
protected:
    virtual void resizeEvent ( QResizeEvent* event ) override;

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

protected:
    virtual void windowStateChangeEvent ( QEvent* event, QWidget* target );

protected slots:
    virtual void onButtonClicked();
};

#endif // QQTSYSTEMBUTTONBAR_H
