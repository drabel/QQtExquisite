﻿#ifndef QQT3DWIDGET_H
#define QQT3DWIDGET_H

#include <qqtexquisite_global.h>
#include <qqtwidget.h>

/**
 * @brief The QQt3DWidget class
 * 可以在xyz三个平面扭曲图片的3d widget。
 *
 * 可以设置向三个平面的扭曲角度；
 * 可以设置旋转中心 --原点;
 * 可以按照画面中的比例位置设置；
 */
class QQTEXQUISITESHARED_EXPORT QQt3DWidget : public QQtWidget
{
    Q_OBJECT
public:
    explicit QQt3DWidget ( QWidget* parent = 0 );
    virtual ~QQt3DWidget();

    //在x、y、z平面的偏移角度。[0,0,0]
    void set_degree_of_axis_x ( qreal degree );
    void set_degree_of_axis_y ( qreal degree );
    void set_degree_of_axis_z ( qreal degree );

    //设置原点位置 [0,0] 画面中的比例位置也可以。
    void set_original_point ( qreal x, qreal y );
    void set_original_point_x ( qreal x );
    void set_original_point_y ( qreal y );
    void set_original_point_ratio ( qreal xRatio, qreal yRatio );
    void set_original_point_x_ratio ( qreal xRatio );
    void set_original_point_y_ratio ( qreal yRatio );

    // QWidget interface
protected:
    virtual void paintEvent ( QPaintEvent* event ) override;

private:
    qreal degreeX;
    qreal degreeY;
    qreal degreeZ;

    qreal basePointX, basePointY;
    qreal basePointXRatio, basePointYRatio;
};

#endif // QQT3DWIDGET_H
