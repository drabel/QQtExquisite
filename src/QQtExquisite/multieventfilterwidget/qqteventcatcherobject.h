#ifndef QQTEVENTCATCHEROBJECT_H
#define QQTEVENTCATCHEROBJECT_H

#include <QObject>
#include <QList>
#include <QEvent>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtEventCatcherObject class
 * 保存eventFilter
 * 保存eventCatcher
 */
class QQTEXQUISITESHARED_EXPORT QQtEventCatcherObject
{

public:
    QQtEventCatcherObject();
    virtual ~QQtEventCatcherObject();

    //安装和卸载 eventfilter
    void installEventFilter ( QObject* filterObj );
    void removeEventFilter ( QObject* obj );

    //filter list
    QList<QObject*>& eventFilterList();
    const QList<QObject*>& eventFilterList() const;

    //安装和卸载 eventcatcher
    void installEventCatcher ( QObject* catcherObj );
    void removeEventCatcher ( QObject* obj );

    //catcher list
    QList<QObject*>& eventCatcherList();
    const QList<QObject*>& eventCatcherList() const;

public:
    //一般返回 false 一般在event之前。
    virtual bool filter ( QObject* watched, QEvent* event );

    //一般返回 false 一般在event之后。
    virtual bool catcher ( QObject* watched, QEvent* event );

private:
    QList<QObject*> mFilterObjList;
    QList<QObject*> mCatcherObjList;
};

#endif // QQTEVENTCATCHEROBJECT_H

