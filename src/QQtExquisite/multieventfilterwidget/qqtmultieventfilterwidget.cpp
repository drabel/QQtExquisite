#include <qqtmultieventfilterwidget.h>
#include <QList>

QQtMultiEventFilterWidget::QQtMultiEventFilterWidget ( QWidget* parent ) : QQtWidget ( parent )
{

}

QQtMultiEventFilterWidget::~QQtMultiEventFilterWidget()
{

}

void QQtMultiEventFilterWidget::installEventFilter ( QObject* filterObj )
{
    QQtWidget::installEventFilter ( filterObj );
}

void QQtMultiEventFilterWidget::removeEventFilter ( QObject* obj )
{
    QQtWidget::removeEventFilter ( obj );
}

void QQtMultiEventFilterWidget::installEventCatcher ( QObject* catcherObj )
{
    catcherList.installEventCatcher ( catcherObj );
}

void QQtMultiEventFilterWidget::removeEventCatcher ( QObject* obj )
{
    catcherList.removeEventCatcher ( obj );
}

bool QQtMultiEventFilterWidget::event ( QEvent* event )
{
    bool ret = false;

    //eventFilter ... + paintEvent ...
    ret = QQtWidget::event ( event );

    //这里基本上都返回了 true ， 很少返回 false ， FocusAboutToChange 这个返回了false。
    //pline() << event->type() << ret;
    //if ( ret )
    //    return ret;

    //event被销毁了吗？没有。
    //paint event 现在还有效吗？还能用吗？能。

    //eventCatcher ... 先安装谁，就先执行谁。
    QListIterator<QObject*> itor ( catcherList.eventCatcherList() );
    while ( itor.hasNext() )
    {
        QObject* catcher = itor.next();
        bool ret = catcher->eventFilter ( this, event );
        //pline() << event->type() << ret;
        //如果catcher返回true，不继续执行。
        if ( ret )
            return ret;
    }

    //这里我全都返回false，好不好？不好？我决定返回event()的返回值。
    return ret;
}
