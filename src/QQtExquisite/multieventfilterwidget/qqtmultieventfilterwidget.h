#ifndef QQTMULTIEVENTFILTERWIDGET_H
#define QQTMULTIEVENTFILTERWIDGET_H

#include <QQtWidget>
#include <QEvent>
#include <qqteventcatcherobject.h>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtMultiEventFilterWidget class
 * 多eventFilterWidget，支持在event()之前filter、之后catcher。
 *
 * 有的时候有的效果需要显示在图像的上边，可以依靠这里的catcher完成。
 * 在有多种效果添加到图片上的时候，建议使用。
 *
 * 工作原理：
 * [installEventFilter]  [event] [installEventCatcher]
 * eventFilter - event() - eventFilter
 * 效果器 - paintEvent - 效果器
 * 后边 - 中间（主要） - 前边
 * background - middle - foreground
 * back - overlay - fore
 */
class QQTEXQUISITESHARED_EXPORT QQtMultiEventFilterWidget : public QQtWidget
{
    Q_OBJECT

public:
    explicit QQtMultiEventFilterWidget ( QWidget* parent = 0 );
    virtual ~QQtMultiEventFilterWidget();

    //QObject interface
    void installEventFilter ( QObject* filterObj );
    void removeEventFilter ( QObject* obj );

    //在 paintEvent ... 之前
    //paintEvent() ...
    //在 paintEvent ... 之后

    //QQtMultiEventFilterObject interface
    void installEventCatcher ( QObject* catcherObj );
    void removeEventCatcher ( QObject* obj );

protected:

private:
    QQtEventCatcherObject catcherList;

    // QObject interface
public:
    virtual bool event ( QEvent* event ) override;
};

#endif // QQTMULTIEVENTFILTERWIDGET_H

