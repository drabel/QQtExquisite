#include <qqteventcatcherobject.h>

#include <QList>

QQtEventCatcherObject::QQtEventCatcherObject()
{

}

QQtEventCatcherObject::~QQtEventCatcherObject()
{

}

void QQtEventCatcherObject::installEventFilter ( QObject* filterObj )
{
    if ( !filterObj )
        return;
    if ( mFilterObjList.contains ( filterObj ) )
        return;
    mFilterObjList.push_back ( filterObj );
}

void QQtEventCatcherObject::removeEventFilter ( QObject* obj )
{
    if ( !obj )
        return;
    if ( !mFilterObjList.contains ( obj ) )
        return;
    mFilterObjList.removeOne ( obj );
}

QList<QObject*>& QQtEventCatcherObject::eventFilterList()
{
    return mFilterObjList;
}

const QList<QObject*>& QQtEventCatcherObject::eventFilterList() const
{
    return mFilterObjList;
}

void QQtEventCatcherObject::installEventCatcher ( QObject* catcherObj )
{
    if ( !catcherObj )
        return;
    if ( mCatcherObjList.contains ( catcherObj ) )
        return;
    mCatcherObjList.push_back ( catcherObj );
}

void QQtEventCatcherObject::removeEventCatcher ( QObject* obj )
{
    if ( !obj )
        return;
    if ( !mCatcherObjList.contains ( obj ) )
        return;
    mCatcherObjList.removeOne ( obj );
}

QList<QObject*>& QQtEventCatcherObject::eventCatcherList()
{
    return mCatcherObjList;
}

const QList<QObject*>& QQtEventCatcherObject::eventCatcherList() const
{
    return mCatcherObjList;
}

bool QQtEventCatcherObject::filter ( QObject* watched, QEvent* event )
{
    bool ret = false;

    QListIterator<QObject*> itor ( mFilterObjList );
    while ( itor.hasNext() )
    {
        QObject* filter = itor.next();
        bool ret = filter->eventFilter ( watched, event );
        //pline() << event->type() << ret;
        //如果filter返回true，不继续执行。
        if ( ret )
            return ret;
    }

    return ret;
}

bool QQtEventCatcherObject::catcher ( QObject* watched, QEvent* event )
{
    bool ret = false;

    QListIterator<QObject*> itor ( mCatcherObjList );
    while ( itor.hasNext() )
    {
        QObject* catcher = itor.next();
        bool ret = catcher->eventFilter ( watched, event );
        //pline() << event->type() << ret;
        //如果catcher返回true，不继续执行。
        if ( ret )
            return ret;
    }

    return ret;
}
