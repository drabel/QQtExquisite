#ifndef QQT3DMULTIEVENTFILTERWIDGET_H
#define QQT3DMULTIEVENTFILTERWIDGET_H

#include <qqt3dwidget.h>
#include <qqteventcatcherobject.h>

#include <qqtexquisite_global.h>
/**
 * @brief The QQt3DMultiEventFilterWidget class
 */
class QQTEXQUISITESHARED_EXPORT QQt3DMultiEventFilterWidget : public QQt3DWidget
{
    Q_OBJECT

public:
    explicit QQt3DMultiEventFilterWidget ( QWidget* parent = 0 );
    virtual ~QQt3DMultiEventFilterWidget();

    //QObject interface
    void installEventFilter ( QObject* filterObj );
    void removeEventFilter ( QObject* obj );

    //在 paintEvent ... 之前
    //paintEvent() ...
    //在 paintEvent ... 之后

    //QQtMultiEventFilterObject interface
    void installEventCatcher ( QObject* catcherObj );
    void removeEventCatcher ( QObject* obj );

protected:

private:
    QQtEventCatcherObject catcherList;


    // QObject interface
public:
    virtual bool event ( QEvent* event ) override;
};

#endif // QQT3DMULTIEVENTFILTERWIDGET_H

