﻿#include <qqtbodyclipboardhelper.h>

//#define LOCAL_DEBUG
#ifdef LOCAL_DEBUG
#define p3line() p2line()
#else
#define p3line() QNoDebug()
#endif

#include <QGuiApplication>
#include <QImage>

QQtBodyClipBoardHelper::QQtBodyClipBoardHelper ( QObject* parent )
{
    m_pClipBoard = QGuiApplication::clipboard();
}

QQtBodyClipBoardHelper::~QQtBodyClipBoardHelper()
{

}

void QQtBodyClipBoardHelper::keyPressEvent ( QKeyEvent* event, QWidget* target )
{
    QKeyEvent* e = event;
    QQtWidget& w = * ( qobject_cast<QQtWidget*> ( target ) );

    if ( e->modifiers().testFlag ( Qt::ControlModifier ) )
    {
        if ( e->key() == Qt::Key_C )
        {
            m_pClipBoard->setImage ( w.image() );
        }
        else if ( e->key() == Qt::Key_V )
        {
            p3line() << m_pClipBoard->mimeData()->urls();
            p3line() << m_pClipBoard->mimeData()->text();
            if ( m_pClipBoard->mimeData()->hasUrls()
                 && m_pClipBoard->mimeData()->urls().size() > 0 )
            {
                QUrl url = m_pClipBoard->mimeData()->urls() [0];
                QString filename = url.toLocalFile();
                p3line() << filename;
                w.setPixmap ( filename );
            }
            else if ( m_pClipBoard->mimeData()->hasImage() )
            {
                w.setPixmap ( m_pClipBoard->mimeData()->imageData().value<QImage>() );
            }
        }
    }

    e->accept();
}

bool QQtBodyClipBoardHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QQtWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::KeyPress:
        {
            QKeyEvent* e = ( QKeyEvent* ) event;
            keyPressEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
