#ifndef QQTCHILDBODYKEEPSAMEPOSHELPER_H
#define QQTCHILDBODYKEEPSAMEPOSHELPER_H

#include <QObject>
#include <QEvent>
#include <QWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyKeepSamePosHelper class
 * 使target保持和parent相同的位置。
 */
class QQTEXQUISITESHARED_EXPORT QQtChildBodyKeepSamePosHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyKeepSamePosHelper ( QObject* parent = 0 )
        : QObject ( parent ) {
        targetParentWidget = 0;
        targetWidget = 0;
    }
    virtual ~QQtChildBodyKeepSamePosHelper() {}

protected:


protected:

private:
    QWidget* targetParentWidget;
    QWidget* targetWidget;


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTCHILDBODYKEEPSAMEPOSHELPER_H

