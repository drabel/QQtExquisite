#ifndef QQTBODYKEYPRESSHELPER_H
#define QQTBODYKEYPRESSHELPER_H

#include <QObject>
#include <QWidget>
#include <QKeyEvent>

#include <qqtexquisite_global.h>

class QQTEXQUISITESHARED_EXPORT QQtBodyKeyPressHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtBodyKeyPressHelper ( QObject* parent = 0 )
        : QObject ( parent ) {}
    virtual ~QQtBodyKeyPressHelper() {}

signals:
    void keyPress ( Qt::Key, Qt::KeyboardModifiers );
    void keyRelease ( Qt::Key, Qt::KeyboardModifiers );

protected:
    virtual void keyPressEvent ( QKeyEvent* event, QWidget* target );
    virtual void keyReleaseEvent ( QKeyEvent* event, QWidget* target );
private:


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTBODYKEYPRESSHELPER_H

