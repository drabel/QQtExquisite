system(touch $${PWD}/qqtexquisite.cpp)

SOURCES +=
HEADERS +=
FORMS +=

add_object_class(QQtChildBodyKeepSameRightHelper)
SOURCES += \
    $$PWD/qqtchildbodykeepsamerighthelper.cpp
HEADERS += \
    $$PWD/qqtchildbodykeepsamerighthelper.h

add_object_class(QQtChildBodyKeepSameBottomHelper)
SOURCES += \
    $$PWD/qqtchildbodykeepsamebottomhelper.cpp
HEADERS += \
    $$PWD/qqtchildbodykeepsamebottomhelper.h

add_object_class(QQtChildBodyKeepSameTopHelper)
SOURCES += \
    $$PWD/qqtchildbodykeepsametophelper.cpp
HEADERS += \
    $$PWD/qqtchildbodykeepsametophelper.h

add_object_class(QQtChildBodyKeepSameLeftHelper)
SOURCES += \
    $$PWD/qqtchildbodykeepsamelefthelper.cpp
HEADERS += \
    $$PWD/qqtchildbodykeepsamelefthelper.h

add_object_class(QQtChildBodyKeepSamePosHelper)
SOURCES += \
    $$PWD/qqtchildbodykeepsameposhelper.cpp
HEADERS += \
    $$PWD/qqtchildbodykeepsameposhelper.h

add_object_class(QQtChildBodyKeepSameHeightHelper)
SOURCES += \
    $$PWD/qqtchildbodykeepsameheighthelper.cpp
HEADERS += \
    $$PWD/qqtchildbodykeepsameheighthelper.h

add_object_class(QQtChildBodyKeepSameWidthHelper)
SOURCES += \
    $$PWD/qqtchildbodykeepsamewidthhelper.cpp
HEADERS += \
    $$PWD/qqtchildbodykeepsamewidthhelper.h

add_object_class(QQtChildBodyKeepSameSizeHelper)
SOURCES += \
    $$PWD/qqtchildbodykeepsamesizehelper.cpp
HEADERS += \
    $$PWD/qqtchildbodykeepsamesizehelper.h

add_object_class(QQtChildBodyKeepCenterHelper)
SOURCES += \
    $$PWD/qqtchildbodykeepcenterhelper.cpp
HEADERS += \
    $$PWD/qqtchildbodykeepcenterhelper.h

add_object_class(QQtBodyBackgroundMaskEffect)
SOURCES += \
    $$PWD/qqtbodybackgroundmaskeffect.cpp
HEADERS += \
    $$PWD/qqtbodybackgroundmaskeffect.h

add_object_class(QQtShowMaskHelper)
SOURCES += \
    $$PWD/qqtshowmaskhelper.cpp
HEADERS += \
    $$PWD/qqtshowmaskhelper.h

add_object_class(QQtSelectStyleWidget)
SOURCES += \
    $$PWD/qqtselectstylewidget.cpp
HEADERS += \
    $$PWD/qqtselectstylewidget.h

add_object_class(QQtBodyFullscreenHelper)
SOURCES += \
    $$PWD/qqtbodyfullscreenhelper.cpp
HEADERS += \
    $$PWD/qqtbodyfullscreenhelper.h

add_object_class(QQtBodyAutoHideHelper)
SOURCES += \
    $$PWD/qqtbodyautohidehelper.cpp
HEADERS += \
    $$PWD/qqtbodyautohidehelper.h

add_object_class(QQtChildBodyAutoHideHelper)
SOURCES += \
    $$PWD/qqtchildbodyautohidehelper.cpp
HEADERS += \
    $$PWD/qqtchildbodyautohidehelper.h

add_object_class(QQtChildBodyStayOnSideHelper)
SOURCES += \
    $$PWD/qqtchildbodystayonsidehelper.cpp
HEADERS += \
    $$PWD/qqtchildbodystayonsidehelper.h

add_object_class(QQtBodyStayOnSideHelper)
SOURCES += \
    $$PWD/qqtbodystayonsidehelper.cpp
HEADERS += \
    $$PWD/qqtbodystayonsidehelper.h

add_file(qqt3dwidget.cpp)
add_file(qqt3dwidget.h)
SOURCES += \
    $$PWD/qqt3dwidget.cpp
HEADERS += \
    $$PWD/qqt3dwidget.h

add_object_class(QQt3DBodyHelper)
SOURCES += \
    $$PWD/qqt3dbodyhelper.cpp
HEADERS += \
    $$PWD/qqt3dbodyhelper.h

add_object_class(QQt3DBodySelectedStyle)
SOURCES += \
    $$PWD/qqt3dbodyselectedstyle.cpp
HEADERS += \
    $$PWD/qqt3dbodyselectedstyle.h

add_file(qqtbodyclipboardhelper.cpp)
add_file(qqtbodyclipboardhelper.h)
SOURCES += \
    $$PWD/qqtbodyclipboardhelper.cpp
HEADERS += \
    $$PWD/qqtbodyclipboardhelper.h

add_file(qqtbodydragdropimagehelper.cpp)
add_file(qqtbodydragdropimagehelper.h)
SOURCES += \
    $$PWD/qqtbodydragdropimagehelper.cpp
HEADERS += \
    $$PWD/qqtbodydragdropimagehelper.h

add_file(qqtdragtopnghelper.cpp)
add_file(qqtdragtopnghelper.h)
SOURCES += \
    $$PWD/qqtdragtopnghelper.cpp
HEADERS += \
    $$PWD/qqtdragtopnghelper.h

add_file(qqtdragfromfilehelper.cpp)
add_file(qqtdragfromfilehelper.h)
SOURCES += \
    $$PWD/qqtdragfromfilehelper.cpp
HEADERS += \
    $$PWD/qqtdragfromfilehelper.h

add_file(qqtmacstyletitlebar.cpp)
add_file(qqtmacstyletitlebar.h)
SOURCES += \
    $$PWD/qqtmacstyletitlebar.cpp
HEADERS += \
    $$PWD/qqtmacstyletitlebar.h

add_file(qqtrotateimagewidget.cpp)
add_file(qqtrotateimagewidget.h)
SOURCES += \
    $$PWD/qqtrotateimagewidget.cpp
HEADERS += \
    $$PWD/qqtrotateimagewidget.h

add_file(qqtshakehelper.cpp)
add_file(qqtshakehelper.h)
SOURCES += \
    $$PWD/qqtshakehelper.cpp
HEADERS += \
    $$PWD/qqtshakehelper.h

DEFINES += __QQTMULTIPAGEWIDGET__
contains (DEFINES, __QQTMULTIPAGEWIDGET__) {
    #multipage widget
    SOURCES += \
        $$PWD/multipagewidget/qqt3dmultipagewidget.cpp
    HEADERS += \
        $$PWD/multipagewidget/qqt3dmultipagewidget.h

    SOURCES += \
        $$PWD/multipagewidget/qqt3dselectedstyle.cpp
    HEADERS += \
        $$PWD/multipagewidget/qqt3dselectedstyle.h

    SOURCES += \
        $$PWD/multipagewidget/qqtselectedstyle.cpp
    HEADERS += \
        $$PWD/multipagewidget/qqtselectedstyle.h

    SOURCES += \
        $$PWD/multipagewidget/qqtmultipagewidget.cpp
    HEADERS += \
        $$PWD/multipagewidget/qqtmultipagewidget.h

    add_widget_class(QQt3DMultiEventFilterMultiPageWidget, $$PWD/multipagewidget)
    SOURCES += \
        $$PWD/multipagewidget/qqt3dmultieventfiltermultipagewidget.cpp
    HEADERS += \
        $$PWD/multipagewidget/qqt3dmultieventfiltermultipagewidget.h

    add_widget_class(QQtMultiEventFilterMultiPageWidget, $$PWD/multipagewidget)
    SOURCES += \
        $$PWD/multipagewidget/qqtmultieventfiltermultipagewidget.cpp
    HEADERS += \
        $$PWD/multipagewidget/qqtmultieventfiltermultipagewidget.h
}

DEFINES += __QQTMULTIEVENTFILTERWIDGET__
contains (DEFINES, __QQTMULTIEVENTFILTERWIDGET__) {
    add_class(QQtEventCatcherObject, $$PWD/multieventfilterwidget)
    SOURCES += \
        $$PWD/multieventfilterwidget/qqteventcatcherobject.cpp
    HEADERS += \
        $$PWD/multieventfilterwidget/qqteventcatcherobject.h

    add_widget_class(QQtMultiEventFilterWidget, $$PWD/multieventfilterwidget)
    SOURCES += \
        $$PWD/multieventfilterwidget/qqtmultieventfilterwidget.cpp
    HEADERS += \
        $$PWD/multieventfilterwidget/qqtmultieventfilterwidget.h

    add_widget_class(QQt3DMultiEventFilterWidget, $$PWD/multieventfilterwidget)
    SOURCES += \
        $$PWD/multieventfilterwidget/qqt3dmultieventfilterwidget.cpp
    HEADERS += \
        $$PWD/multieventfilterwidget/qqt3dmultieventfilterwidget.h
}

SOURCES += \
    $$PWD/qqtsystembuttonbar.cpp
HEADERS += \
    $$PWD/qqtsystembuttonbar.h
FORMS += \
    $$PWD/qqtsystembuttonbar.ui

add_widget_class(QQtSystemFullButtonBar)
SOURCES += \
    $$PWD/qqtsystemfullbuttonbar.cpp
HEADERS += \
    $$PWD/qqtsystemfullbuttonbar.h
FORMS += \
    $$PWD/qqtsystemfullbuttonbar.ui

add_widget_class(QQtSystemCustomButtonBar)
SOURCES += \
    $$PWD/qqtsystemcustombuttonbar.cpp
HEADERS += \
    $$PWD/qqtsystemcustombuttonbar.h
FORMS += \
    $$PWD/qqtsystemcustombuttonbar.ui

add_object_class(QQtSplashScreenHelper)
SOURCES += \
    $$PWD/qqtsplashscreenhelper.cpp
HEADERS += \
    $$PWD/qqtsplashscreenhelper.h

add_widget_class(QQtLightWidget)
SOURCES += \
    $$PWD/qqtlightwidget.cpp
HEADERS += \
    $$PWD/qqtlightwidget.h

add_object_class(QQtLightWidgetHelper)
SOURCES += \
    $$PWD/qqtlightwidgethelper.cpp
HEADERS += \
    $$PWD/qqtlightwidgethelper.h

add_widget_class(QQtPushButtonStyleWidget)
SOURCES += \
    $$PWD/qqtpushbuttonstylewidget.cpp
HEADERS += \
    $$PWD/qqtpushbuttonstylewidget.h

add_widget_class(QQtCustomPushButton)
SOURCES += \
    $$PWD/qqtcustompushbutton.cpp
HEADERS += \
    $$PWD/qqtcustompushbutton.h

add_object_class(QQtPushButtonStyleHelper)
SOURCES += \
    $$PWD/qqtpushbuttonstylehelper.cpp
HEADERS += \
    $$PWD/qqtpushbuttonstylehelper.h

add_object_class(QQtBodyKeyPressHelper)
SOURCES += \
    $$PWD/qqtbodykeypresshelper.cpp
HEADERS += \
    $$PWD/qqtbodykeypresshelper.h

add_object_class(QQtBodyMouseWheelHelper)
SOURCES += \
    $$PWD/qqtbodymousewheelhelper.cpp
HEADERS += \
    $$PWD/qqtbodymousewheelhelper.h

add_object_class(QQtCircleWaitingHelper)
SOURCES += \
    $$PWD/qqtcirclewaitinghelper.cpp
HEADERS += \
    $$PWD/qqtcirclewaitinghelper.h

add_widget_class(QQtCircleWaitingWidget)
SOURCES += \
    $$PWD/qqtcirclewaitingwidget.cpp
HEADERS += \
    $$PWD/qqtcirclewaitingwidget.h

SOURCES += \
        $${PWD}/qqtexquisite.cpp

HEADERS += \
        $${PWD}/qqtexquisite.h \
        $${PWD}/qqtexquisite_global.h

include($${PWD}/library_3rdparty.pri)

