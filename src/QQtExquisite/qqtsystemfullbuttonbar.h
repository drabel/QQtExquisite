#ifndef QQTSYSTEMFULLBUTTONBAR_H
#define QQTSYSTEMFULLBUTTONBAR_H

#include <QWidget>
#include <qqtexquisite_global.h>

namespace Ui {
class QQtSystemFullButtonBar;
}

/**
 * @brief The QQtSystemFullButtonBar class
 * 系统按钮栏。右侧用。菜单、全屏、最小、最大、关闭。
 *
 * 如果设置menu，就可以为menuButton显示菜单。
 */
class QQTEXQUISITESHARED_EXPORT QQtSystemFullButtonBar : public QWidget
{
    Q_OBJECT

public:
    explicit QQtSystemFullButtonBar ( QWidget* parent = 0 );
    virtual ~QQtSystemFullButtonBar();

    //是否可见
    void setMenuButtonVisable ( bool visable );
    void setFullButtonVisable ( bool visable );
    void setMinButtonVisable ( bool visable );
    void setMaxButtonVisable ( bool visable );
    void setCloseButtonVisable ( bool visable );

    //在显示菜单的时候会用到。
    QRect menuButtonRect();

signals:
    void menuButtonToggled ( bool );

    // QWidget interface
protected:
    virtual void resizeEvent ( QResizeEvent* event ) override;

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

protected:
    virtual void windowStateChangeEvent ( QEvent* event, QWidget* target );

protected slots:
    virtual void onButtonClicked();

private:
    Ui::QQtSystemFullButtonBar* ui;
};

#endif // QQTSYSTEMFULLBUTTONBAR_H
