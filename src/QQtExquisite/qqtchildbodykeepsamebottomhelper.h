#ifndef QQTCHILDBODYKEEPSAMEBOTTOMHELPER_H
#define QQTCHILDBODYKEEPSAMEBOTTOMHELPER_H

#include <QObject>
#include <QEvent>
#include <QWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyKeepSameBottomHelper class
 * 使target保持和parent相同的BOTTOM边。
 */
class QQTEXQUISITESHARED_EXPORT QQtChildBodyKeepSameBottomHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyKeepSameBottomHelper ( QObject* parent = 0 )
        : QObject ( parent ) {
        targetParentWidget = 0;
        targetWidget = 0;
    }
    virtual ~QQtChildBodyKeepSameBottomHelper() {}

protected:

private:
    QWidget* targetParentWidget;
    QWidget* targetWidget;


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

};

#endif // QQTCHILDBODYKEEPSAMEBOTTOMHELPER_H

