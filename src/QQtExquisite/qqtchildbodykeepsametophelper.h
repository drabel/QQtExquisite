#ifndef QQTCHILDBODYKEEPSAMETOPHELPER_H
#define QQTCHILDBODYKEEPSAMETOPHELPER_H

#include <QObject>
#include <QEvent>
#include <QWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyKeepSameTopHelper class
 * 使target保持和parent相同的TOP边。
 */
class QQTEXQUISITESHARED_EXPORT QQtChildBodyKeepSameTopHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyKeepSameTopHelper ( QObject* parent = 0 )
        : QObject ( parent ) {
        targetParentWidget = 0;
        targetWidget = 0;
    }
    virtual ~QQtChildBodyKeepSameTopHelper() {}

protected:

private:
    QWidget* targetParentWidget;
    QWidget* targetWidget;


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

};

#endif // QQTCHILDBODYKEEPSAMETOPHELPER_H

