#ifndef QQTCHILDBODYKEEPSAMERIGHTHELPER_H
#define QQTCHILDBODYKEEPSAMERIGHTHELPER_H


#include <QObject>
#include <QEvent>
#include <QWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyKeepSameTopHelper class
 * 使target保持和parent相同的RIGHT边。
 */

class QQTEXQUISITESHARED_EXPORT QQtChildBodyKeepSameRightHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyKeepSameRightHelper ( QObject* parent = 0 )
        : QObject ( parent ) {
        targetParentWidget = 0;
        targetWidget = 0;
    }
    virtual ~QQtChildBodyKeepSameRightHelper() {}

protected:

private:
    QWidget* targetParentWidget;
    QWidget* targetWidget;


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTCHILDBODYKEEPSAMERIGHTHELPER_H

