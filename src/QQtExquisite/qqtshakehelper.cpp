﻿#include <qqtshakehelper.h>

QQtShakeHelper::QQtShakeHelper ( QObject* parent ) : QObject ( parent )
{
    wSkWidget = 0;
    startGeometry = QRect();
    m_animation = new QPropertyAnimation ( this );
}

QQtShakeHelper::~QQtShakeHelper()
{

}

void QQtShakeHelper::setShakeWidget ( QWidget* shakeWidget )
{
    //内部 shake widget 为空
    if ( wSkWidget == 0 )
    {
        wSkWidget = shakeWidget ;
        startGeometry = wSkWidget->frameGeometry();
        m_animation->setTargetObject ( shakeWidget );
        m_animation->setPropertyName ( "pos" );
        return;
    }

    //内部 shake widget 静止
    if ( m_animation->state() == QPropertyAnimation::Stopped )
    {
        wSkWidget = shakeWidget ;
        startGeometry = wSkWidget->frameGeometry();
        m_animation->setTargetObject ( shakeWidget );
        m_animation->setPropertyName ( "pos" );
        return;
    }

    //A.内部 shake widget 在暂停，不能直接更换。暂停恢复后，pos序列无法改变。需要重新shake。
    //  shake pause一下？不，更改shake序列。
    //B.内部 shake widget 可能还在运动，不能直接更换。必须更新shake序列，在新位置shake。

    //无论如何，停止shark？
    //移动到前边来，一个bug，同一个widget连续shake，如果geometry变化，必须调用这个函数，可是此时，连续shark，不关闭，导致窗体老是移动。
    //现在，无论是否是相同的widget，都会顺序的执行。
    endShake();
    wSkWidget->move ( startGeometry.topLeft() );

    //1.先保存 inside shake widget 状态
    QWidget* neibuWidget = wSkWidget;
    QRect pos = startGeometry;

    //2.更新 inside shake widget 到新的。
    wSkWidget = shakeWidget ;
    startGeometry = wSkWidget->frameGeometry();
    m_animation->setTargetObject ( shakeWidget );
    m_animation->setPropertyName ( "pos" );
    setShakeSequeues();;//update shake sequeues。

    //3.修复 prev inside shake widget 的状态
    neibuWidget->setProperty ( "pos", pos.topLeft() );
}

void QQtShakeHelper::shake()
{
    //动画还没有结束就先立马停止，防止用户不停的点击
    endShake();
    setShakeSequeues();
    m_animation->start();
}

void QQtShakeHelper::endShake()
{
    m_animation->stop();
}

void QQtShakeHelper::setShakeSequeues()
{
    m_animation->setDuration ( 200 );
    QPoint pos = startGeometry.topLeft();
    m_animation->setStartValue ( pos );
    m_animation->setKeyValueAt ( 0.1, pos + QPoint ( -5, -5 ) );
    m_animation->setKeyValueAt ( 0.2, pos + QPoint ( +0, -5 ) );
    m_animation->setKeyValueAt ( 0.3, pos + QPoint ( +5, -5 ) );
    m_animation->setKeyValueAt ( 0.4, pos + QPoint ( +5, +0 ) );
    m_animation->setKeyValueAt ( 0.5, pos + QPoint ( +5, +5 ) );
    m_animation->setKeyValueAt ( 0.6, pos + QPoint ( +0, +5 ) );
    m_animation->setKeyValueAt ( 0.7, pos + QPoint ( -5, +5 ) );
    m_animation->setKeyValueAt ( 0.8, pos + QPoint ( -5, +0 ) );
    m_animation->setKeyValueAt ( 0.9, pos + QPoint ( -5, -5 ) );
    m_animation->setEndValue ( pos );
}
