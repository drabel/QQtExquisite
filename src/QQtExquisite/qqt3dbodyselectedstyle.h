#ifndef QQT3DBODYSELECTEDSTYLE_H
#define QQT3DBODYSELECTEDSTYLE_H

#include <QObject>
#include <QEvent>
#include <qqt3dbodyhelper.h>
#include <qqtexquisite_global.h>
/**
 * @brief The QQt3DBodySelectedStyle class
 * 3D控件的选中效果
 */
class QQTEXQUISITESHARED_EXPORT QQt3DBodySelectedStyle : public QQt3DBodyHelper
{
    Q_OBJECT

public:
    explicit QQt3DBodySelectedStyle ( QObject* parent = 0 );
    virtual ~QQt3DBodySelectedStyle();

    enum SelectedStyle
    {
        //default
        SelectedStyle_QtDesigner,
        SelectedStyle_QRCodeScaner,
        SelectedStyle_DottedLine,
        SelectedStyle_FourCheck,

        SelectedStyle_Max
    } ;

    void setSelectedStyle ( SelectedStyle style );
    SelectedStyle selectedStyle();

    void setSelected ( bool bSelected );
    bool selectedStatus();

protected:

private:
    bool hasSelected;
    SelectedStyle hasStyle;


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQT3DBODYSELECTEDSTYLE_H

