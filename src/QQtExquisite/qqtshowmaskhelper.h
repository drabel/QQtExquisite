#ifndef QQTSHOWMASKHELPER_H
#define QQTSHOWMASKHELPER_H

#include <QObject>
#include <QQtWidget>
#include <QColor>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtShowMaskHelper class
 * 为弹窗提供背景遮罩。
 *
 * 支持颜色遮罩，支持图片遮罩。不设置图片就是颜色遮罩 QColor(0,0,0)。
 * 支持修改透明度。默认透明度0.7
 * 支持QWidget及其子类。
 *
 * 依赖目标控件带parent。
 * 如果没有parent，那么就会没有效果。
 * 外部不可以随便删除targetParentWidget。
 *
 * QQtShowXXXEffect系列效果器，目标控件不带parent，显示效果将会失效或者部分失效。
 */
class QQTEXQUISITESHARED_EXPORT QQtShowMaskHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtShowMaskHelper ( QObject* parent = 0 );
    virtual ~QQtShowMaskHelper();

    //允许直接带上alpha。
    void setBackgroundColor ( const QColor& color );
    QColor backgroundColor();

    //0-1
    void setOpacity ( qreal value );
    qreal opacity();

    //这里可以用来设置背景图片。
    QQtWidget* maskWidget();

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

protected:

private:
    QColor mColor;
    qreal mOpacity;
    QQtWidget* mMaskWidget;
};

#endif // QQTSHOWMASKHELPER_H

