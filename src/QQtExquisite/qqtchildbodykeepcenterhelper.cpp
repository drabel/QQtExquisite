#include <qqtchildbodykeepcenterhelper.h>
#include <QEvent>
#include <QShowEvent>
#include <QHideEvent>
#include <QWidget>
#include <qqtcore.h>
#include <qqtwidgets.h>

QQtChildBodyKeepCenterHelper::QQtChildBodyKeepCenterHelper ( QObject* parent ) : QObject ( parent )
{
    targetParentWidget = 0;
    targetWidget = 0;
}

QQtChildBodyKeepCenterHelper::~QQtChildBodyKeepCenterHelper() {}


bool QQtChildBodyKeepCenterHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    QWidget* target = qobject_cast<QWidget*> ( watched );
    if ( !target )
        return QObject::eventFilter ( watched, event );

    if ( target == targetParentWidget )
    {
        switch ( event->type() )
        {
            case QEvent::Move:
            case QEvent::Resize:
            {
                //moveCenter实现有错误不建议使用。已经修正。
                //moveCenter ( targetWidget, targetParentWidget );
                int x1 = 0, y1 = 0, w1 = 0, h1 = 0;
                x1 = targetParentWidget->rect().left() + ( targetParentWidget->rect().width() - targetWidget->width() ) / 2;
                y1 = targetParentWidget->rect().top() + ( targetParentWidget->rect().height() - targetWidget->height() ) / 2;
                w1 = targetWidget->width();
                h1 = targetWidget->height();
                QRect center = QRect ( x1, y1, w1, h1 );
                targetWidget->setGeometry ( center );
                event->accept();
                return false;
            }
            default:
                break;
        }
        return QObject::eventFilter ( watched, event );
    }

    if ( ! target->parent() )
        return QObject::eventFilter ( watched, event );

    if ( !target->parent()->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::Show:
        {
            QShowEvent* e = ( QShowEvent* ) event;
            QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
            targetParentWidget = parentWidget;
            targetWidget = target;
            int x1 = 0, y1 = 0, w1 = 0, h1 = 0;
            x1 = targetParentWidget->rect().left() + ( targetParentWidget->rect().width() - targetWidget->width() ) / 2;
            y1 = targetParentWidget->rect().top() + ( targetParentWidget->rect().height() - targetWidget->height() ) / 2;
            w1 = targetWidget->width();
            h1 = targetWidget->height();
            QRect center = QRect ( x1, y1, w1, h1 );
            //这个函数是否应当保留？
            targetWidget->setGeometry ( center );
            parentWidget->installEventFilter ( this );
            e->accept();
            return false;
        }
        case QEvent::Hide:
        {
            QHideEvent* e = ( QHideEvent* ) event;
            QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
            targetParentWidget = 0;
            targetWidget = 0;
            parentWidget->removeEventFilter ( this );
            e->accept();
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
