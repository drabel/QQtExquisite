#include <qqtlightwidgethelper.h>

#include <QEvent>
#include <QPaintEvent>

#include <QPainter>

QQtLightWidgetHelper::QQtLightWidgetHelper ( QObject* parent )
    : QObject ( parent ) {}

QQtLightWidgetHelper::~QQtLightWidgetHelper() {}

void QQtLightWidgetHelper::setLightStyle ( QQtLightWidgetHelper::LightStyle style )
{
    mStyle = style;
}

QQtLightWidgetHelper::LightStyle QQtLightWidgetHelper::lightStyle()
{
    return mStyle;
}

void QQtLightWidgetHelper::setLightStatus ( bool on )
{
    mON = on;
}

bool QQtLightWidgetHelper::lightStatus()
{
    return mON;
}

void QQtLightWidgetHelper::paintEvent ( QPaintEvent* e, QWidget* target )
{

}

bool QQtLightWidgetHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p2line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::Paint:
        {
            QPaintEvent* e = ( QPaintEvent* ) event;
            paintEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        break;
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
