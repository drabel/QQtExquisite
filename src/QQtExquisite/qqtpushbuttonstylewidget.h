#ifndef QQTPUSHBUTTONSTYLEWIDGET_H
#define QQTPUSHBUTTONSTYLEWIDGET_H

#include <QQtWidget>
#include <qqtwidgets.h>
#include <qqtexquisite_global.h>

/**
 * @brief The QQtPushButtonStyleWidget class
 * 具有5个状态图片的Widget，类似于PushButton，但是没有任何信号，仅仅用于显示。
 *
 * 使用约束：
 * setEnable() 必须使用这里的函数；使用QSS改变enable状态不管用。
 */
class QQTEXQUISITESHARED_EXPORT QQtPushButtonStyleWidget : public QQtWidget
{
    Q_OBJECT

public:
    explicit QQtPushButtonStyleWidget ( QWidget* parent = 0 );
    virtual ~QQtPushButtonStyleWidget();

    QImage stateImage ( int index );
    void setStateImage ( int index, const QImage& image );

    //normal, press; uncheck, check; 0,1;
    void setNormalImage ( const QImage& normal, const QImage& press );
    //hover; 2
    void setHoverImage ( const QImage& hover );
    //disable; 4
    void setDisableImage ( const QImage& disable );

    //更改是否可用状态。
    void setEnabled ( bool );
    void setDisabled ( bool );

protected:
    //用户对工作状态感兴趣的时候，可以使用。
    void setWorkState ( int index );
    int workState() const;

    //状态改变，内部会调用这个函数。可是有的意外、未实现状态未捕获，用户截获那些状态后可以调用这个函数更新图片。
    virtual void translateImage();

private:
    EBtnStatus mWorkState;
    QImage mImageCache[BTN_MAX];

    // QWidget interface
protected:
    virtual void mousePressEvent ( QMouseEvent* event ) override;
    virtual void mouseReleaseEvent ( QMouseEvent* event ) override;
    virtual void enterEvent ( QEvent* event ) override;
    virtual void leaveEvent ( QEvent* event ) override;
};

#endif // QQTPUSHBUTTONSTYLEWIDGET_H

