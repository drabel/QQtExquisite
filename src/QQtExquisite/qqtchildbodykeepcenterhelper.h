#ifndef QQTCHILDBODYKEEPCENTERHELPER_H
#define QQTCHILDBODYKEEPCENTERHELPER_H

#include <QObject>
#include <QEvent>
#include <QWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyKeepCenterHelper class
 * target会在parent窗口中保持居中。
 *
 * 如果没有parent则没有效果。
 */
class QQTEXQUISITESHARED_EXPORT QQtChildBodyKeepCenterHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyKeepCenterHelper ( QObject* parent = 0 );
    virtual ~QQtChildBodyKeepCenterHelper();

protected:

private:
    QWidget* targetParentWidget;
    QWidget* targetWidget;

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTCHILDBODYKEEPCENTERHELPER_H

