#include <qqtbodymousewheelhelper.h>


void QQtBodyMouseWheelHelper::mouseWheelEvent ( QWheelEvent* event, QWidget* target )
{
    QWidget& w = *target;

    if ( !target->isActiveWindow() )
    {
        event->ignore();
        return;
    }

    QWheelEvent* e = ( QWheelEvent* ) event;

    qreal scaling = 1;
    //求取scaling 1.2 0.9
    //up
    if ( e->delta() < 0 )
    {
        //int ratio = 2;
        scaling = 1.2;
    }
    else
    {
        //int ratio = -2;
        scaling = 0.8;
    }

    Qt::Orientation ori = e->orientation();
    emit mouseWheel ( scaling, ori );

    event->accept();
    return;
}

bool QQtBodyMouseWheelHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::Wheel:
        {
            QWheelEvent* e = ( QWheelEvent* ) event;
            mouseWheelEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
