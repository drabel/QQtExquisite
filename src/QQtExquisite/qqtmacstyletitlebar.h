﻿#ifndef QQTMACSTYLETITLEBAR_H
#define QQTMACSTYLETITLEBAR_H

#include "qqtexquisite_global.h"

#include <qqttitlebar.h>

/**
 * @brief The QQtMacStyleTitleBar class
 * 苹果风格的标题栏
 *
 * 有四个按钮。
 * 页面还没有完成。
 */
class QQTEXQUISITESHARED_EXPORT QQtMacStyleTitleBar : public QQtTitleBar
{
    Q_OBJECT
public:
    QQtMacStyleTitleBar ( QWidget* parent = 0 );
    virtual ~QQtMacStyleTitleBar();

    QToolButton* fullscreenButton() { return m_pFullScreenButton; }

private:
    QToolButton* m_pFullScreenButton;

    // QQtTitleBar interface
private slots:
    virtual void onClicked() override;

private:
    virtual void updateMaximize() override;

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

private:
    QImage miniImage, maxImage, fullImage, closeImage;
};

#endif // QQTMACSTYLETITLEBAR_H
