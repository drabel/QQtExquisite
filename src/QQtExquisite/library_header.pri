##############################################################
#编译 QQtExquisite
#为用户编译本库提供头文件包含集、宏控制集方便。
##############################################################
#头文件
defineTest(add_include_QQtExquisite) {
    #header_path = $$1
    header_path=$$PWD

    command =
    command += $${header_path}
    command += $${header_path}/multipagewidget
    command += $${header_path}/multieventfilterwidget


    add_include_path($$command)
    return (1)
}

#本库使用的定义
defineTest(add_defines_QQtExquisite) {

    #预估
    #QQtMessageQueue - libzmq
    #QQtDBus - QtDBus
    #QQtV4L2 Module - libv4l2

    export(QT)
    export(DEFINES)
    export(CONFIG)
    return (1)
}

add_include_QQtExquisite()
add_defines_QQtExquisite()

