#ifndef QQTCHILDBODYSTAYONSIDEHELPER_H
#define QQTCHILDBODYSTAYONSIDEHELPER_H

#include <QObject>

#include <QMouseEvent>
#include <QWidget>
#include <QEvent>
#include <QPoint>


#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyStayOnSideHelper class
 * 子窗口停靠在父窗口的边缘
 *
 * 如果父窗口没有设置，那么没有效果。
 */
class QQTEXQUISITESHARED_EXPORT QQtChildBodyStayOnSideHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyStayOnSideHelper ( QObject* parent = 0 );
    virtual ~QQtChildBodyStayOnSideHelper();

    //用户拖着窗口移动的方向
    enum MoveDirection
    {
        None = 0,
        Left,
        Right,
        LeftTop,
        RightTop,
        LeftBottom,
        RightBottom,
        Top,
        Bottom,
    };

    void setEffectWidth ( int width );
    //default:200
    int effectWidth();

protected:
    virtual void mousePressEvent ( QMouseEvent* event, QWidget* target = 0 );
    virtual void mouseReleaseEvent ( QMouseEvent* event, QWidget* target = 0 );
    virtual void mouseMoveEvent ( QMouseEvent* event, QWidget* target = 0 );

    virtual void checkDirection ( QMouseEvent* event, QWidget* target = 0 );

private:
    bool bMousePressed;
    QPoint pressedPoint;

    MoveDirection direction;
    int mEffectWidth;

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTCHILDBODYSTAYONSIDEHELPER_H

