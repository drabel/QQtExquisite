#ifndef QQTCUSTOMPUSHBUTTON_H
#define QQTCUSTOMPUSHBUTTON_H

#include <QPushButton>
#include <qqtwidgets.h>
#include <qqtexquisite_global.h>

/**
 * @brief The QQtCustomPushButton class
 * QQtPushButton的升级版。
 *
 * 内部保存为QImage，不局限于QString图片。
 */
class QQTEXQUISITESHARED_EXPORT QQtCustomPushButton : public QPushButton
{
    Q_OBJECT

public:
    explicit QQtCustomPushButton ( QWidget* parent = 0 );
    virtual ~QQtCustomPushButton();

    QImage stateImage ( int index );
    void setStateImage ( int index, const QImage& image );

    //normal, press; uncheck, check; [0,1];
    void setNormalImage ( const QImage& normal, const QImage& press );
    //hover; [2];
    void setHoverImage ( const QImage& hover );
    //disable; [4];
    void setDisableImage ( const QImage& disable );

    void setEnabled ( bool );
    void setDisabled ( bool );

protected:
    const TBtnImageTable& imageTable() const;
    TBtnImageTable& imageTable();
    void setWorkState ( int index );
    int workState() const;
    virtual void translateImage();
    virtual void setImage ( const QImage& image );
protected:

private:
    EBtnStatus mWorkState;
    TBtnImageTable mImageTable;
    QImage mImage;

    // QWidget interface
protected:
    virtual void mousePressEvent ( QMouseEvent* event ) override;
    virtual void mouseReleaseEvent ( QMouseEvent* event ) override;
    virtual void enterEvent ( QEvent* event ) override;
    virtual void leaveEvent ( QEvent* event ) override;

    // QWidget interface
protected:
    virtual void paintEvent ( QPaintEvent* event ) override;
};

#endif // QQTCUSTOMPUSHBUTTON_H

