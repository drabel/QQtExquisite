#-------------------------------------------------
#
# Project created by QtCreator 2018-06-07T22:23:34
#
#-------------------------------------------------

QT       += widgets

#TARGET = QQtExquisite
#TEMPLATE = lib
#CONFIG += build_all

#armhf32 gcc4.9不支持c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#用户工程必须包含多链接技术。
#include(../../multi-link/add_base_manager.pri)

#本库编译为动态库
#contains(DEFINES, LIB_LIBRARY):DEFINES += QQTEXQUISITE_LIBRARY
#else:contains(DEFINES, LIB_STATIC_LIBRARY):DEFINES += QQTEXQUISITE_STATIC_LIBRARY

#根据multi-link提供的动态编译 静态编译设定进行编译，添加我自己的QQtExquisite的宏定义。
#如果用户编译动态library。保证头文件导出正常。
contains(TEMPLATE, lib):contains(DEFINES, LIB_LIBRARY) {
    DEFINES += QQTEXQUISITE_LIBRARY
    message(Build $${TARGET} QQTEXQUISITE_LIBRARY is defined. build)
}
#如果用户编译静态library。保证头文件包含正常。
else:contains(TEMPLATE, lib) {
    DEFINES += QQTEXQUISITE_STATIC_LIBRARY
    message(Build $${TARGET} QQTEXQUISITE_STATIC_LIBRARY is defined. build and link)
}
#如果用户编译app。保证头文件包含正常。
else {
    DEFINES += QQTEXQUISITE_STATIC_LIBRARY
    message(Build $${TARGET} QQTEXQUISITE_STATIC_LIBRARY is defined. build and link)
}

#用户必须手动依赖QQt
#本库依赖QQt
#add_dependent_manager(QQt)

#本库导出SDK到LIB_SDK_ROOT
#add_sdk(QQtExquisite, $$add_target_name())
#add_sdk_header_no_postfix(QQtExquisite, $$add_target_name(), QQtExquisite)

include($${PWD}/library_header.pri)
include($${PWD}/library_source.pri)
