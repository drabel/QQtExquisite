#ifndef QQTSPLASHSCREENHELPER_H
#define QQTSPLASHSCREENHELPER_H

#include <QObject>
#include <QTimer>
#include <QWidget>
#include <QMutex>
#include <qqtexquisite_global.h>

/**
 * @brief The QQtSplashScreenHelper class
 * 帮助用户方便的设置程序启动动画
 *
 * 未实现
 */
class QQTEXQUISITESHARED_EXPORT QQtSplashScreenHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtSplashScreenHelper ( QObject* parent = 0 );
    virtual ~QQtSplashScreenHelper();

    void setSplashScreenWidget ( QWidget* targetWidget );

    void finish();

private slots:
    void slotTimeout();

protected:

private:
    QMutex mutex;
    QWidget* target;
    QTimer* timer;
};

#endif // QQTSPLASHSCREENHELPER_H

