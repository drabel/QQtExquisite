#ifndef QQTSELECTSTYLEWIDGET_H
#define QQTSELECTSTYLEWIDGET_H

#include <QQtWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtSelectStyleWidget class
 * 可选中的Widget。
 *
 * 主库提供的selectedstyle在选中的时候，虚线跑到了图片的底下，
 * 这里提供了一个可选中的Widget，虚线会在图片上边。
 *
 * 建议使用效果器和QQtMultiEventFilterWidget。
 */
class QQTEXQUISITESHARED_EXPORT QQtSelectStyleWidget : public QQtWidget
{
    Q_OBJECT

public:
    explicit QQtSelectStyleWidget ( QWidget* parent = 0 );
    virtual ~QQtSelectStyleWidget();

    enum SelectedStyle
    {
        //default
        SelectedStyle_QtDesigner,
        SelectedStyle_QRCodeScaner,
        SelectedStyle_DottedLine,
        SelectedStyle_FourCheck,

        SelectedStyle_Max
    } ;

    void setSelectedStyle ( SelectedStyle style );
    SelectedStyle selectedStyle();

    void setSelected ( bool bSelected );
    bool selectedStatus();

protected:

private:
    bool hasSelected;
    SelectedStyle hasStyle;

    // QWidget interface
protected:
    virtual void paintEvent ( QPaintEvent* event ) override;

    // QWidget interface
protected:
    virtual void focusInEvent ( QFocusEvent* event ) override;
    virtual void focusOutEvent ( QFocusEvent* event ) override;
};

#endif // QQTSELECTSTYLEWIDGET_H

