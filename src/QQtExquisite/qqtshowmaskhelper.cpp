#include <qqtshowmaskhelper.h>
#include <QShowEvent>
#include <QResizeEvent>
#include <QPalette>

#include <qqtchildbodykeepsamesizehelper.h>

QQtShowMaskHelper::QQtShowMaskHelper ( QObject* parent ) : QObject ( parent )
{
    mColor = QColor ( 0, 0, 0, 178 );
    mOpacity = ( qreal ) 178 / 255;

    mMaskWidget = new QQtWidget ( );
    mMaskWidget->hide();
    mMaskWidget->setAutoFillBackground ( true );

    setBackgroundColor ( mColor );

    mMaskWidget->installEventFilter ( new QQtChildBodyKeepSameSizeHelper ( this ) );

    //这些设置没有必要
    //mMaskWidget->setWindowFlag ( Qt::FramelessWindowHint );
    //mMaskWidget->setWindowFlag ( Qt::Tool );
    //mMaskWidget->setWindowFlag ( Qt::WindowStaysOnBottomHint );

    //不管用
    //mMaskWidget->setWindowOpacity ( 0.7 );
}

QQtShowMaskHelper::~QQtShowMaskHelper()
{

}

void QQtShowMaskHelper::setBackgroundColor ( const QColor& color )
{
    mColor = color;
    QPalette plt = mMaskWidget->palette();
    plt.setColor ( QPalette::Background, mColor );
    mMaskWidget->setPalette ( plt );
    mMaskWidget->update();
}

QColor QQtShowMaskHelper::backgroundColor()
{
    return mColor;
}

void QQtShowMaskHelper::setOpacity ( qreal value )
{
    if ( value < 0 || value > 255 )
        return;

    int alpha = qRound ( 255 * value );
    mColor.setAlpha ( alpha );

    //这个函数不管用
    //mColor.setAlphaF ( alpha );

    setBackgroundColor ( mColor );
}

qreal QQtShowMaskHelper::opacity()
{
    return mOpacity;
}

QQtWidget* QQtShowMaskHelper::maskWidget()
{
    return mMaskWidget;
}


bool QQtShowMaskHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    QWidget* target = qobject_cast<QWidget*> ( watched );
    if ( !target )
        return QObject::eventFilter ( watched, event );

    if ( ! target->parent() )
        return QObject::eventFilter ( watched, event );

    if ( !target->parent()->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::Show:
        {
            QShowEvent* e = ( QShowEvent* ) event;

            //此处注意下，如果是设置QObject，或者转换为QObject进行设置，那么，将会没有任何widget内置现象，widget自己飘走了。
            //mMaskWidget->setParent ( target->parent() );
            //qobject_cast<QObject*> ( mMaskWidget )->setParent ( parentWidget );

            QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
            mMaskWidget->setParent ( parentWidget );
            mMaskWidget->show();

            //当target是widget的时候，这里很需要，帮助target保持在最上。
            target->raise();

            e->accept();
            return false;
        }
        case QEvent::Hide:
        {
            QShowEvent* e = ( QShowEvent* ) event;
            mMaskWidget->setParent ( 0 );
            mMaskWidget->hide();
            e->accept();
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
