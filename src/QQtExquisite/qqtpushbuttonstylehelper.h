#ifndef QQTPUSHBUTTONSTYLEHELPER_H
#define QQTPUSHBUTTONSTYLEHELPER_H

#include <QObject>
#include <QMouseEvent>

#include <qqtwidgets.h>
#include <qqtexquisite_global.h>
/**
 * @brief The QQtPushButtonStyleHelper class
 * 具有5个状态图片的WidgetHelper，
 * 类似于PushButton，但是没有任何信号，仅仅用于显示。
 *
 * 使用约束：
 * setEnable() 必须使用这里的函数；使用QSS改变enable状态不管用。
 *
 * bug：有响应缓慢的现象。
 * 这个设计不合理，和event会产生重叠，导致效果重叠，或许失效现象。
 */
class QQTEXQUISITESHARED_EXPORT QQtPushButtonStyleHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtPushButtonStyleHelper ( QObject* parent = 0 );
    virtual ~QQtPushButtonStyleHelper();

public:
    QImage stateImage ( int index );
    void setStateImage ( int index, const QImage& image );

    //normal, press; uncheck, check; [0,1];
    void setNormalImage ( const QImage& normal, const QImage& press );
    //hover; [2];
    void setHoverImage ( const QImage& hover );
    //disable; [4];
    void setDisableImage ( const QImage& disable );

    void setEnabled ( bool );
    void setDisabled ( bool );

    const TBtnImageTable& imageTable() const;
    TBtnImageTable& imageTable();
    int workState() const;

    void setTargetWidget ( QWidget* target );

protected:
    void setWorkState ( int index );
    virtual void translateImage ( QWidget* target );
    virtual void setImage ( const QImage& image );
    virtual void update();
protected:

private:
    EBtnStatus mWorkState;
    TBtnImageTable mImageTable;
    QImage mImage;

    QWidget* mTarget;

    // QWidget interface
protected:
    virtual void mousePressEvent ( QMouseEvent* event, QWidget* target );
    virtual void mouseReleaseEvent ( QMouseEvent* event, QWidget* target );
    virtual void enterEvent ( QEvent* event, QWidget* target );
    virtual void leaveEvent ( QEvent* event, QWidget* target );

    // QWidget interface
protected:
    virtual void paintEvent ( QPaintEvent* event, QWidget* target );


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTPUSHBUTTONSTYLEHELPER_H

