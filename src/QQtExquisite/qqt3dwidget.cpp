﻿#include <qqt3dwidget.h>

QQt3DWidget::QQt3DWidget ( QWidget* parent ) : QQtWidget ( parent )
{
    degreeX = degreeY = degreeZ = 0;
    basePointX = basePointY = 0;
    basePointXRatio = basePointYRatio = 0;
}

QQt3DWidget::~QQt3DWidget()
{

}

void QQt3DWidget::set_degree_of_axis_x ( qreal degree )
{
    degreeX = degree;
    update();
}

void QQt3DWidget::set_degree_of_axis_y ( qreal degree )
{
    degreeY = degree;
    update();
}

void QQt3DWidget::set_degree_of_axis_z ( qreal degree )
{
    degreeZ = degree;
    update();
}

void QQt3DWidget::set_original_point ( qreal x, qreal y )
{
    basePointX = x;
    basePointY = y;
    update();
}

void QQt3DWidget::set_original_point_x ( qreal x )
{
    basePointX = x;
    update();
}

void QQt3DWidget::set_original_point_y ( qreal y )
{
    basePointY = y;
    update();
}

void QQt3DWidget::set_original_point_ratio ( qreal xRatio, qreal yRatio )
{
    basePointXRatio = xRatio;
    basePointYRatio = yRatio;
    update();
}

void QQt3DWidget::set_original_point_x_ratio ( qreal xRatio )
{
    basePointXRatio = xRatio;
    update();
}

void QQt3DWidget::set_original_point_y_ratio ( qreal yRatio )
{
    basePointYRatio = yRatio;
    update();
}

void QQt3DWidget::paintEvent ( QPaintEvent* event )
{
    Q_UNUSED ( event );

    if ( image().isNull() )
        return QQtWidget::paintEvent ( event );

    QStylePainter painter ( this );

    // 反走样
    painter.setRenderHint ( QPainter::Antialiasing );
    painter.setRenderHint ( QPainter::TextAntialiasing );
    painter.setRenderHint ( QPainter::SmoothPixmapTransform );
    painter.setRenderHint ( QPainter::HighQualityAntialiasing );

    QRect r0 = rect();

    qreal basePointXResult, basePointYResult;
    basePointXResult = 0;
    basePointYResult = 0;
    if ( basePointX != 0 )
        basePointXResult = basePointX;
    if ( basePointY != 0 )
        basePointYResult = basePointY;

    if ( basePointXRatio != 0 )
        basePointXResult = r0.width() * basePointXRatio;
    if ( basePointYRatio != 0 )
        basePointYResult = r0.height() * basePointYRatio;

    QTransform transform;
    // 平移
    transform.translate ( basePointXResult, basePointYResult );
    // 旋转
    transform.rotate ( degreeX, Qt::XAxis );
    transform.rotate ( degreeY, Qt::YAxis );
    transform.rotate ( degreeZ, Qt::ZAxis );
    transform.translate ( -basePointXResult, -basePointYResult );
    painter.setTransform ( transform );

    switch ( imageStyle() )
    {
        case QQTCENTER:
        {
            /**
             * 要达到居中的目标，QImage需要做的size判断很繁复，这里使用QIcon做一些中间转换的后续转换，可以很容易的达到绘制合理大小的pixmap的目的。
             * source: pixmap file image
             * QImage 输入、输出两侧是pixmap
             * 借助(+QIcon) QIcon 输入、输出两侧也是pixmap
             * dest: 所需要的、合理大小的pixmap
             */
            //这个也不行.???不能缩放...
#if 0
            QPixmap pix;
            pix.convertFromImage ( image() );
            painter.drawPixmap ( rect(), pix );
#endif

            //计算繁琐
#if 0
            QSize rSize = rect().size();
            QSize iSize = image().rect().size();
            int lX = iSize.width() <= rSize.width() ? iSize.width() : rSize.width();
            int lY = iSize.height() <= rSize.height() ? iSize.height() : rSize.height();
            QSize mSize ( lX, lY );
            painter.drawItemPixmap ( rect(), Qt::AlignCenter, QPixmap::fromImage ( image().scaled ( mSize,
                                                                                   Qt::KeepAspectRatio ) ) );
#endif

            //缩小图片的时候,失败了.???不是的,外部使用的时候请注意,你的widget最小大小是不是0,0.如果不是,那么你的fixedSize是不是太小了,能设置成功吗?
            //写法复杂
#if 0
            QIcon icon;
            icon.addPixmap ( QPixmap::fromImage ( image() ), QIcon::Normal, QIcon::On );
            painter.drawItemPixmap ( rect(), Qt::AlignCenter, icon.pixmap ( rect().size(), QIcon::Normal, QIcon::On ) );
#endif

            //经过验证,
            //QIcon的构造函数参数为pixmap的有bug,不能正常获得完整pixmap.
            //QIcon的构造函数参数为QString的是正常的.
            //以上结论错误,都是好的,没有bug,受到了外部widget,setMinimumSize的限定蛊惑,setFixedSize无效导致的.
            //不居中.
#if 0
            painter.drawPixmap ( rect(), QIcon ( QPixmap::fromImage ( image() ) ).pixmap ( rect().size(), QIcon::Normal,
                                                                                           QIcon::On ) );
#endif

#if 0
            painter.drawItemPixmap ( rect(), Qt::AlignCenter, QIcon ( QPixmap::fromImage ( image() ) ).pixmap ( rect().size(),
                                     QIcon::Normal, QIcon::On ) );
#endif

#if 1
            //获取合理大小的pixmap 很可能会缩小一下 代替QIcon
            QPixmap pixmap;

            int w0, h0;
            QRect resultImage = this->rect();
            w0 = resultImage.width() - image().width();
            h0 = resultImage.height() - image().height();

            if ( w0 < 0 && h0 < 0 )
                pixmap = QPixmap::fromImage ( image()
                                              .scaled ( resultImage.width(), resultImage.height(), Qt::KeepAspectRatio )
                                            );
            else if ( w0 < 0 )
                pixmap = QPixmap::fromImage ( image()
                                              .scaled ( resultImage.width(), image().height(), Qt::KeepAspectRatio )
                                            );
            else if ( h0 < 0 )
                pixmap = QPixmap::fromImage ( image()
                                              .scaled ( image().width(), resultImage.height(), Qt::KeepAspectRatio )
                                            );
            else
                pixmap = QPixmap::fromImage ( image() );

            //代替drawItemPixmap
            //QQtWidget里面，StylePainter自动居中绘制，这里，手动居中绘制。
            //从合理位置开始绘制 很可能从中间开始绘制
            int x0, y0;
            x0 = ( resultImage.width() - pixmap.width() ) / 2;
            y0 = ( resultImage.height() - pixmap.height() ) / 2;
            x0 = x0 < 0 ? 0 : x0;
            y0 = y0 < 0 ? 0 : y0;
            painter.drawPixmap ( x0, y0, pixmap );
#endif

        }
        break;

        case QQTTILEDWIDTH:
        {
            painter.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                                     /*.copy() 切出图片的左上部分使用*/
                                     QPixmap::fromImage ( image().copy ( rect() )
                                                          .scaledToWidth ( rect().width() )
                                                        ) );
        }
        break;

        case QQTZOOMWIDTH:
        {
            painter.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                                     /*不.copy() 切出图片的中间部分使用*/
                                     QPixmap::fromImage ( image()
                                                          .scaled ( rect().width(), image().height(), Qt::IgnoreAspectRatio )
                                                        ) );
        }
        break;

        case QQTTILEDHEIGHT:
        {
            painter.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                                     /*.copy() 切出图片的左上部分使用*/
                                     QPixmap::fromImage ( image().copy ( rect() )
                                                          .scaledToHeight ( rect().height() )
                                                        ) );
        }
        break;

        case QQTTILED:
        {
            painter.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                                     /*.copy() 切出图片的左上部分使用*/
                                     QPixmap::fromImage ( image().copy ( rect() )
                                                          .scaled ( rect().width(), rect().height(), Qt::KeepAspectRatio )
                                                        ) );
        }
        break;

        case QQTZOOMHEIGHT:
        {
            painter.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                                     /*不.copy() 切出图片的中间部分使用*/
                                     QPixmap::fromImage ( image()
                                                          .scaled ( image().width(), rect().height(), Qt::IgnoreAspectRatio )
                                                        ) );
        }
        break;

        case QQTZOOM:
        {
            painter.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                                     /*不.copy() 切出图片的中间部分使用*/
                                     QPixmap::fromImage ( image()
                                                          .scaled ( rect().width(), rect().height(), Qt::IgnoreAspectRatio )
                                                        ) );
        }
        break;

        case QQTZOOMWIDTH_KEEPASPECTRATIO:
        {
            painter.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                                     /*不.copy() 切出图片的中间部分使用*/
                                     QPixmap::fromImage ( image()
                                                          .scaled ( rect().width(), image().height(), Qt::KeepAspectRatio )
                                                        ) );
        }
        break;

        case QQTZOOMHEIGHT_KEEPASPECTRATIO:
        {
            painter.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                                     /*不.copy() 切出图片的中间部分使用*/
                                     QPixmap::fromImage ( image()
                                                          .scaled ( image().width(), rect().height(), Qt::KeepAspectRatio )
                                                        ) );
        }
        break;

        case QQTZOOM_KEEPASPECTRATIO:
        {
            painter.drawItemPixmap ( rect(), Qt::AlignLeft | Qt::AlignTop,
                                     /*不.copy() 切出图片的中间部分使用*/
                                     QPixmap::fromImage ( image()
                                                          .scaled ( rect().width(), rect().height(), Qt::KeepAspectRatio )
                                                        ) );
        }
        break;

        default:
        {

        }
        break;
    }

    //For QSS
    return QWidget::paintEvent ( event );
}
