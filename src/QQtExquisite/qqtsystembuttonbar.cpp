#include "qqtsystembuttonbar.h"
#include "ui_qqtsystembuttonbar.h"

#ifdef Q_OS_WIN
//#pragma comment(lib, "user32.lib")
#include <qt_windows.h>
#endif
#include <QStyle>

QQtSystemButtonBar::QQtSystemButtonBar ( QWidget* parent ) :
    QWidget ( parent ),
    ui ( new Ui::QQtSystemButtonBar )
{
    ui->setupUi ( this );

    ui->toolButton_3->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarMinButton ) );
    ui->toolButton_2->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarMaxButton ) );
    ui->toolButton->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarCloseButton ) );

    connect ( ui->toolButton,   SIGNAL ( clicked ( bool ) ), this, SLOT ( onButtonClicked() ) );
    connect ( ui->toolButton_2, SIGNAL ( clicked ( bool ) ), this, SLOT ( onButtonClicked() ) );
    connect ( ui->toolButton_3, SIGNAL ( clicked ( bool ) ), this, SLOT ( onButtonClicked() ) );

    //监视root windows。
    this->window()->installEventFilter ( this );
}

QQtSystemButtonBar::~QQtSystemButtonBar()
{
    delete ui;
}

void QQtSystemButtonBar::setMinButtonVisable ( bool visable )
{
    ui->toolButton_3->setVisible ( visable );
}

void QQtSystemButtonBar::setMaxButtonVisable ( bool visable )
{
    ui->toolButton_2->setVisible ( visable );
}

void QQtSystemButtonBar::setCloseButtonVisable ( bool visable )
{
    ui->toolButton->setVisible ( visable );
}

void QQtSystemButtonBar::resizeEvent ( QResizeEvent* event )
{
    QSize s0 = this->size();
    ui->toolButton->setFixedSize ( s0.height(), s0.height() );
    ui->toolButton_2->setFixedSize ( s0.height(), s0.height() );
    ui->toolButton_3->setFixedSize ( s0.height(), s0.height() );
}


bool QQtSystemButtonBar::eventFilter ( QObject* watched, QEvent* event )
{
    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        //this->window(), empty? no event...
        case QEvent::WindowStateChange:
        case QEvent::Resize:
        {
            QEvent* e = ( QEvent* ) event;
            windowStateChangeEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        default:
            break;
    }

    return QWidget::eventFilter ( watched, event );
}

void QQtSystemButtonBar::windowStateChangeEvent ( QEvent* event, QWidget* target )
{
    QWidget* pWindow = this->window();
    if ( pWindow->isTopLevel() )
    {
        bool bMaximize = pWindow->isMaximized();
        if ( bMaximize )
        {
            ui->toolButton_2->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarNormalButton ) );
        }
        else
        {
            ui->toolButton_2->setIcon ( style()->standardIcon ( QStyle::SP_TitleBarMaxButton ) );
        }
    }
}

void QQtSystemButtonBar::onButtonClicked()
{
    QToolButton* pButton = qobject_cast<QToolButton*> ( sender() );
    QWidget* pWindow = this->window();
    if ( pWindow->isTopLevel() )
    {
        if ( pButton == ui->toolButton_3 )
        {
            pWindow->showMinimized();
        }
        else if ( pButton == ui->toolButton_2 )
        {
            pWindow->isMaximized() ? pWindow->showNormal() : pWindow->showMaximized();
        }
        else if ( pButton == ui->toolButton )
        {
            pWindow->close();
        }
    }
}
