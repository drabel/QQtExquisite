#ifndef QQTLIGHTWIDGET_H
#define QQTLIGHTWIDGET_H

#include <QWidget>

#include <qqtlightwidgethelper.h>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtLightWidget class
 * 灯控件
 *
 * 设置灯样式
 * 开启、关闭灯
 */
class QQTEXQUISITESHARED_EXPORT QQtLightWidget : public QWidget
{
    Q_OBJECT

public:
    explicit QQtLightWidget ( QWidget* parent = 0 );
    virtual ~QQtLightWidget();

    //操作灯的样式、开关
    QQtLightWidgetHelper* helper();

protected:

private:
    QQtLightWidgetHelper* mHelper;
};

#endif // QQTLIGHTWIDGET_H

