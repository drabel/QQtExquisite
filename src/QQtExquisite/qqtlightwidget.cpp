#include <qqtlightwidget.h>


QQtLightWidget::QQtLightWidget ( QWidget* parent )
    : QWidget ( parent )
{
    mHelper = new QQtLightWidgetHelper ( this );
    installEventFilter ( mHelper );
}

QQtLightWidget::~QQtLightWidget() {}

QQtLightWidgetHelper* QQtLightWidget::helper()
{
    return mHelper;
}
