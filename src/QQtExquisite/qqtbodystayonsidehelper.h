#ifndef QQTBODYSTAYONSIDEHELPER_H
#define QQTBODYSTAYONSIDEHELPER_H

#include <QObject>

#include <QMouseEvent>
#include <QWidget>
#include <QEvent>
#include <QPoint>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtBodyStayOnSideHelper class
 * 帮助顶级窗口停靠在桌面边缘；
 */
class QQTEXQUISITESHARED_EXPORT QQtBodyStayOnSideHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtBodyStayOnSideHelper ( QObject* parent = 0 );
    virtual ~QQtBodyStayOnSideHelper();

    //用户拖着窗口移动的方向
    enum MoveDirection
    {
        None = 0,
        Left,
        Right,
        LeftTop,
        RightTop,
        LeftBottom,
        RightBottom,
        Top,
        Bottom,
    };


    void setEffectWidth ( int width );
    //default:200
    int effectWidth();

protected:
    virtual void mousePressEvent ( QMouseEvent* event, QWidget* target = 0 );
    virtual void mouseReleaseEvent ( QMouseEvent* event, QWidget* target = 0 );
    virtual void mouseMoveEvent ( QMouseEvent* event, QWidget* target = 0 );

    virtual void checkDirection ( QMouseEvent* event, QWidget* target = 0 );

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

private:
    bool bMousePressed;
    QPoint pressedPoint;

    MoveDirection direction;
    int mEffectWidth;
};

#endif // QQTBODYSTAYONSIDEHELPER_H

