#ifndef QQTBODYBACKGROUNDMASKEFFECT_H
#define QQTBODYBACKGROUNDMASKEFFECT_H

#include <qqtshowmaskhelper.h>
#include <qqtexquisite_global.h>
/**
 * @brief The QQtBodyBackgroundMaskEffect class
 * 给弹窗添加背景Mask
 *
 * QQtShowMaskHelper的包装类。
 */
class QQTEXQUISITESHARED_EXPORT QQtBodyBackgroundMaskEffect : public QQtShowMaskHelper
{
    Q_OBJECT

public:
    explicit QQtBodyBackgroundMaskEffect ( QObject* parent = 0 )
        : QQtShowMaskHelper ( parent ) {}
    virtual ~QQtBodyBackgroundMaskEffect() {}

protected:

private:

};

#endif // QQTBODYBACKGROUNDMASKEFFECT_H

