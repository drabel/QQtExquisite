﻿#include <qqtmacstyletitlebar.h>
#ifdef Q_OS_WIN
//#pragma comment(lib, "user32.lib")
#include <qt_windows.h>
#endif
#include <QStyle>

QQtMacStyleTitleBar::QQtMacStyleTitleBar ( QWidget* parent ) : QQtTitleBar ( parent )
{
    m_pFullScreenButton = new QToolButton ( this );
    m_pFullScreenButton->setIcon ( style()-> standardIcon ( QStyle::SP_TitleBarMenuButton ) );
    m_pFullScreenButton->setSizePolicy ( QSizePolicy::Minimum, QSizePolicy::Expanding );
    m_pFullScreenButton->setObjectName ( "fullscreenButton" );
    m_pFullScreenButton->setToolTip ( "Fullscreen" );
    connect ( m_pFullScreenButton, SIGNAL ( clicked ( bool ) ), this, SLOT ( onClicked() ) );
    ( ( QHBoxLayout* ) layout() )->insertWidget ( 4, m_pFullScreenButton );
}

QQtMacStyleTitleBar::~QQtMacStyleTitleBar()
{

}


void QQtMacStyleTitleBar::onClicked()
{
    QToolButton* pButton = qobject_cast<QToolButton*> ( sender() );
    QWidget* pWindow = this->window();
    if ( pWindow->isTopLevel() )
    {
        if ( pButton == minimizeButton() )
        {
            pWindow->showMinimized();
        }
        else if ( pButton == maximizeButton() )
        {
            pWindow->isMaximized() ? pWindow->showNormal() : pWindow->showMaximized();
        }
        else if ( pButton == closeButton() )
        {
            pWindow->close();
        }
        else if ( pButton == fullscreenButton() )
        {
            static QRect g0 = pWindow->geometry();
            if ( pWindow->isFullScreen() )
            {
                pWindow->showNormal();
                pWindow->setGeometry ( g0 );
            }
            else
            {
                g0 = pWindow->geometry();
                pWindow->showFullScreen();
            }
        }
    }
}

void QQtMacStyleTitleBar::updateMaximize()
{
    QWidget* pWindow = this->window();
    if ( pWindow->isTopLevel() )
    {
        bool bMaximize = pWindow->isMaximized();
        if ( bMaximize )
        {
            maximizeButton()->setToolTip ( tr ( "Restore" ) );
            maximizeButton()->setProperty ( "maximizeProperty", "restore" );
            maximizeButton()->setIcon ( style()-> standardIcon ( QStyle::SP_TitleBarNormalButton ) );
        }
        else
        {
            maximizeButton()->setProperty ( "maximizeProperty", "maximize" );
            maximizeButton()->setToolTip ( tr ( "Maximize" ) );
            maximizeButton()->setIcon ( style()-> standardIcon ( QStyle::SP_TitleBarMaxButton ) );
        }

        maximizeButton()->setStyle ( QApplication::style() );
    }
}


bool QQtMacStyleTitleBar::eventFilter ( QObject* watched, QEvent* event )
{
    switch ( event->type() )
    {
        case QEvent::WindowStateChange:
        case QEvent::Resize:
        {
            int extent = titleLabel()->size().height();
            fullscreenButton()->setMinimumSize ( extent, extent );
            return true;
        }
        default:
            break;
    }
    return QQtTitleBar::eventFilter ( watched, event );
}
