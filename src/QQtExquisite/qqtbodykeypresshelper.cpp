#include <qqtbodykeypresshelper.h>

void QQtBodyKeyPressHelper::keyPressEvent ( QKeyEvent* event, QWidget* target )
{
    Qt::KeyboardModifiers modifiers =  event->modifiers();
    Qt::Key key = ( Qt::Key ) event->key();
    emit keyPress ( key, modifiers );
}

void QQtBodyKeyPressHelper::keyReleaseEvent ( QKeyEvent* event, QWidget* target )
{
    Qt::KeyboardModifiers modifiers =  event->modifiers();
    Qt::Key key = ( Qt::Key ) event->key();
    emit keyRelease ( key, modifiers );
}

bool QQtBodyKeyPressHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::KeyPress:
        {
            QKeyEvent* e = ( QKeyEvent* ) event;
            keyPressEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::KeyRelease:
        {
            QKeyEvent* e = ( QKeyEvent* ) event;
            keyReleaseEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
