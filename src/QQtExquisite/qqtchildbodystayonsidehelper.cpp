#include <qqtchildbodystayonsidehelper.h>

#include <QEvent>
#include <QWidget>

#include <qqtcore.h>


QQtChildBodyStayOnSideHelper::QQtChildBodyStayOnSideHelper ( QObject* parent ) : QObject ( parent )
{
    bMousePressed = false;
    mEffectWidth = 80;
}

QQtChildBodyStayOnSideHelper::~QQtChildBodyStayOnSideHelper() {}


void QQtChildBodyStayOnSideHelper::setEffectWidth ( int width )
{
    mEffectWidth = width;
}

int QQtChildBodyStayOnSideHelper::effectWidth()
{
    return mEffectWidth;
}
void QQtChildBodyStayOnSideHelper::mousePressEvent ( QMouseEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QMargins m_margins = target->contentsMargins();
    //以下代码用来过滤边缘的margin。
#if 1
    //maptoGlobal(rect()) 与 globalPos 对比
    QRect rectMustIn = QRect ( target->mapToGlobal ( target->rect().topLeft() ),
                               target->mapToGlobal ( target->rect().bottomRight() ) );
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = event->globalPos();
    //经过测试，这种方法，在子窗口、root窗口，得到的数据完全正常。
    //pline() << target->geometry() << rectMustIn
    //        << rectMustIn.contains ( event->globalPos() ) << rectMustNotIn.contains ( event->globalPos() );
#endif

    //这里必须用target，这代表当前窗口。
    QWidget* win = target;

    if ( win->isMaximized() ||
         !win->isActiveWindow() ||
         !rectMustIn.contains ( cursorPos ) ||
         !rectMustNotIn.contains ( cursorPos ) )
    {
        event->ignore();
        return;
    }

    if ( event->button() == Qt::LeftButton )
    {
        bMousePressed = true;
        pressedPoint = event->globalPos();
    }

    checkDirection ( event, win );

    event->accept();
}

void QQtChildBodyStayOnSideHelper::mouseReleaseEvent ( QMouseEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    if ( !target->parent() )
    {
        bMousePressed = false;
        direction = None;
        event->ignore();
        return;
    }

    //但凡应用于子窗口，一律使用target。
    QWidget* win = target;
    QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );

    //根据位置，设置geometry
    //如果鼠标pressed，那么移动。
    if ( bMousePressed && !win->isMaximized() )
    {
        QRect deskGeometry = parentWidget->rect();

        QRect oldGeometry = win->geometry();
        QPoint widgetPos = oldGeometry.topLeft();

        int rx, ry;
        int x, y, width = 0, height = 0;

        QRect newGeometry = oldGeometry;

        switch ( direction )
        {
            case Left: // left
                //cursor
                rx = event->globalPos().x() - pressedPoint.x();
                ry = 0;

                //widget
                x = deskGeometry.left();
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                //pline() << event->globalPos().x() << pressedPoint.x() << rx << ry;
                //pline() << oldGeometry << newGeometry;

                break;

            case Right: //right
                //cursor
                rx = event->globalPos().x() - pressedPoint.x();
                ry = 0;

                //widget
                x = deskGeometry.right() - oldGeometry.width();
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            case LeftTop: //left top
                //cursor
                rx = event->globalPos().x() - pressedPoint.x();
                ry = event->globalPos().y() - pressedPoint.y();

                //widget
                x = deskGeometry.left();
                y = deskGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            case RightTop: //right top
                //cursor
                rx = event->globalPos().x() - pressedPoint.x();
                ry = event->globalPos().y() - pressedPoint.y();

                //widget
                x = deskGeometry.right() - oldGeometry.width();
                y = deskGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case LeftBottom: //left bottom
                //cursor
                rx = event->globalPos().x() - pressedPoint.x();
                ry = event->globalPos().y() - pressedPoint.y();

                //widget
                x = deskGeometry.left();
                y = deskGeometry.bottom() - oldGeometry.height();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case RightBottom: //right bottom
                //cursor
                rx = event->globalPos().x() - pressedPoint.x();
                ry = event->globalPos().y() - pressedPoint.y();

                //widget
                x = deskGeometry.right() - oldGeometry.width();
                y = deskGeometry.bottom() - oldGeometry.height();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Top: //top
                //cursor
                rx = 0;
                ry = event->globalPos().y() - pressedPoint.y();

                //widget
                x = oldGeometry.left();
                y = deskGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Bottom: //bottom
                //cursor
                rx = 0;
                ry = event->globalPos().y() - pressedPoint.y();

                //widget
                x = oldGeometry.left();
                y = deskGeometry.bottom() - oldGeometry.height();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            default:
                break;
        }

        pressedPoint = event->globalPos();

        QMargins m_margins = win->contentsMargins();

        if ( width > ( m_margins.left() + m_margins.right() ) &&
             height > ( m_margins.top() + m_margins.bottom() ) )
        {
            win->setGeometry ( newGeometry );
        }

    }

    //用geometry也是对的。
    //顶层窗口对target、对win设置geometry都是对的?错误。必须对win设置！！！它才是真正的root窗口。target不一定！
    //target是当前窗口！
#if 0
    pline() << target->objectName() << win->objectName();
    pline() << target->parent();
    if ( target->parent() )
        pline() << target->parent()->objectName();
    pline() << direction << target->geometry()
            << win->geometry() << win->frameGeometry()
            << QApplication::desktop()->availableGeometry();
#elif 1
    p2line() << target << target->objectName();
    p2line() << target->parent();
    if ( target->parent() )
    {
        p2line() << target->parent()->objectName();
        p2line() << direction << target->geometry() << parentWidget->rect();
    }
#endif

    bMousePressed = false;
    direction = None;
    event->accept();
}

void QQtChildBodyStayOnSideHelper::mouseMoveEvent ( QMouseEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    QWidget* win = target;

    //如果鼠标没有press，遭遇到窗体边缘，变换鼠标样式。
    if ( bMousePressed && !win->isMaximized() )
    {
        checkDirection ( event, win );
    }
    event->accept();
}

void QQtChildBodyStayOnSideHelper::checkDirection ( QMouseEvent* event, QWidget* target )
{
    //判断位置
    //子窗体，根据？，判断位置 parent 如果parent=0，那么direction=None
    //顶级窗体，根据和DesktopWidget的位置关系，判断位置
    Q_ASSERT ( target );

    QMargins m_margins = target->contentsMargins();

#if 1
    //maptoGlobal(rect()) 与 globalPos 对比
    QRect rectMustIn = QRect ( target->mapToGlobal ( target->rect().topLeft() ),
                               target->mapToGlobal ( target->rect().bottomRight() ) );
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = event->globalPos();
    //经过测试，这种方法，在子窗口、root窗口，得到的数据完全正常。
    //pline() << target->geometry() << rectMustIn
    //        << rectMustIn.contains ( event->globalPos() ) << rectMustNotIn.contains ( event->globalPos() );
#endif

    if ( target->isMaximized() ||
         !target->isActiveWindow() ||
         !rectMustIn.contains ( cursorPos ) ||
         !rectMustNotIn.contains ( cursorPos ) )
    {
        direction = None;
        //target->unsetCursor();
        event->ignore();
        return;
    }

    if ( !target->parent() )
    {
        direction = None;
        //target->unsetCursor();
        event->ignore();
        return;
    }

    int baseLeftCoord, baseTopCoord, baseRightCoord, baseBottomCoord;
    rectMustNotIn.getCoords ( &baseLeftCoord, &baseTopCoord, &baseRightCoord, &baseBottomCoord );

    int x, y;
    x = cursorPos.x();
    y = cursorPos.y();

    QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
    QRect deskGeometry = QRect ( parentWidget->mapToGlobal ( parentWidget->rect().topLeft() ),
                                 parentWidget->mapToGlobal ( parentWidget->rect().bottomRight() ) );

    int effectWidth = mEffectWidth;

    direction = None;
    //left
    if ( x - deskGeometry.left()  - effectWidth < x - rectMustIn.left() )
    {
        if ( y - deskGeometry.top() - effectWidth < y - rectMustIn.top() )
        {
            direction = LeftTop;
        }
        else if ( deskGeometry.bottom() - y - effectWidth < rectMustIn.bottom() - y )
        {
            direction = LeftBottom;
        }
        else
        {
            direction = Left;
        }
    }
    //right
    else if ( deskGeometry.right() - x - effectWidth < rectMustIn.right() - x )
    {
        if ( y - deskGeometry.top() - effectWidth < y - rectMustIn.top() )
        {
            direction = RightTop;
        }
        else if ( deskGeometry.bottom() - y - effectWidth < rectMustIn.bottom() - y )
        {
            direction = RightBottom;
        }
        else
        {
            direction = Right;
        }
    }
    //middle
    else
    {
        if ( y - deskGeometry.top() - effectWidth < y - rectMustIn.top() )
        {
            direction = Top;
        }
        else if ( deskGeometry.bottom() - y - effectWidth < rectMustIn.bottom() - y )
        {
            direction = Bottom;
        }
    }

#if 0
    switch ( direction )
    {
        case Left:
        case Right:
            target->setCursor ( Qt::SizeHorCursor );
            break;

        case Top:
        case Bottom:
            target->setCursor ( Qt::SizeVerCursor );
            break;

        case LeftTop:
        case RightBottom:
            target->setCursor ( Qt::SizeFDiagCursor );
            break;

        case LeftBottom:
        case RightTop:
            target->setCursor ( Qt::SizeBDiagCursor );
            break;

        default:
            break;
    }
#endif

}

bool QQtChildBodyStayOnSideHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    //if ( event->type() == QEvent::MouseMove )
    //    return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::MouseButtonPress:
        {
            QMouseEvent* e = ( QMouseEvent* ) event;
            mousePressEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::MouseButtonRelease:
        {
            QMouseEvent* e = ( QMouseEvent* ) event;
            mouseReleaseEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::MouseMove:
        {
            QMouseEvent* e = ( QMouseEvent* ) event;
            mouseMoveEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
