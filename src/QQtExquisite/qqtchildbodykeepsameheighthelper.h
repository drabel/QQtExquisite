#ifndef QQTCHILDBODYKEEPSAMEHEIGHTHELPER_H
#define QQTCHILDBODYKEEPSAMEHEIGHTHELPER_H

#include <QObject>
#include <QEvent>
#include <QWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyKeepSameHeightHelper class
 * 使target保持和parent相同高度。
 */
class QQTEXQUISITESHARED_EXPORT QQtChildBodyKeepSameHeightHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyKeepSameHeightHelper ( QObject* parent = 0 );
    virtual ~QQtChildBodyKeepSameHeightHelper();

protected:

private:
    QWidget* targetParentWidget;
    QWidget* targetWidget;


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTCHILDBODYKEEPSAMEHEIGHTHELPER_H

