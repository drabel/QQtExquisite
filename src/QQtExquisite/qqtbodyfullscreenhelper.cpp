#include <qqtbodyfullscreenhelper.h>


QQtBodyFullscreenHelper::QQtBodyFullscreenHelper ( QObject* parent )
    : QObject ( parent ) {}

QQtBodyFullscreenHelper::~QQtBodyFullscreenHelper() {}

void QQtBodyFullscreenHelper::showFullscreenHelper ( QWidget* target )
{
    if ( !target->inherits ( "QWidget" ) )
        return;

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = target->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return;

    mouseDoubleClickEvent ( 0, target );
}

void QQtBodyFullscreenHelper::mouseDoubleClickEvent ( QMouseEvent* event, QWidget* target )
{
    if ( target->isFullScreen() )
    {
        //ui->frame->setParent ( this );
        target->setWindowFlags ( target->windowFlags() ^ Qt::Window );
        target->setWindowFlags ( target->windowFlags() | Qt::Widget );
        target->showNormal();
    }
    else
    {
        //ui->frame->setParent ( 0 );
        target->setWindowFlags ( target->windowFlags() | Qt::Window );
        target->showFullScreen();
    }
    return;
}

bool QQtBodyFullscreenHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    //if ( event->type() == QEvent::MouseMove )
    //    return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::MouseButtonDblClick:
        {
            QMouseEvent* e = ( QMouseEvent* ) event;
            mouseDoubleClickEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
