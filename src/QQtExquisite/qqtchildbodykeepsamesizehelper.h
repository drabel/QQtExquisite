#ifndef QQTCHILDBODYKEEPSAMESIZEHELPER_H
#define QQTCHILDBODYKEEPSAMESIZEHELPER_H

#include <QObject>
#include <QEvent>
#include <QWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyKeepSameSizeHelper class
 * 使target保持和parent相同大小。
 */
class QQTEXQUISITESHARED_EXPORT QQtChildBodyKeepSameSizeHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyKeepSameSizeHelper ( QObject* parent = 0 );
    virtual ~QQtChildBodyKeepSameSizeHelper();

protected:

private:
    QWidget* targetParentWidget;
    QWidget* targetWidget;


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTCHILDBODYKEEPSAMESIZEHELPER_H

