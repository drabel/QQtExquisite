#ifndef QQT3DBODYHELPER_H
#define QQT3DBODYHELPER_H

#include <QObject>
#include <QEvent>

#include <qqtexquisite_global.h>
/**
 * @brief The QQt3DBodyHelper class
 * 3d效果器的基础类，主要提供关于3d状态的设定和读取
 *
 * 可以设置向三个平面的扭曲角度；
 * 可以设置旋转中心 --原点;
 * 可以按照画面中的比例位置设置；
 */
class QQTEXQUISITESHARED_EXPORT QQt3DBodyHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQt3DBodyHelper ( QObject* parent = 0 );
    virtual ~QQt3DBodyHelper();

    //在x、y、z平面的偏移角度。[0,0,0]
    void set_degree_of_axis_x ( qreal degree );
    void set_degree_of_axis_y ( qreal degree );
    void set_degree_of_axis_z ( qreal degree );

    qreal degree_of_axis_x();
    qreal degree_of_axis_y();
    qreal degree_of_axis_z();

    //设置原点位置 [0,0] 画面中的比例位置也可以。
    void set_original_point ( qreal x, qreal y );
    void set_original_point_x ( qreal x );
    void set_original_point_y ( qreal y );

    void original_point ( qreal& x, qreal& y );
    qreal original_point_x();
    qreal original_point_y();

    void set_original_point_ratio ( qreal xRatio, qreal yRatio );
    void set_original_point_x_ratio ( qreal xRatio );
    void set_original_point_y_ratio ( qreal yRatio );

    void original_point_ratio ( qreal& xRatio, qreal& yRatio );
    qreal original_point_x_ratio();
    qreal original_point_y_ratio();

protected:

private:
    qreal degreeX;
    qreal degreeY;
    qreal degreeZ;

    qreal basePointX, basePointY;
    qreal basePointXRatio, basePointYRatio;

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQT3DBODYHELPER_H

