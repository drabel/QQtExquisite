#-------------------------------------------------
#
# Project created by QtCreator 2018-06-07T22:23:34
#
#-------------------------------------------------

QT       += widgets

TARGET = QQtExquisite
TEMPLATE = lib

CONFIG += debug_and_release #这个必须有
CONFIG += build_all

#armhf32 gcc4.9不支持c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include(../../multi-link/add_base_manager.pri)

#默认编译动态库
#add_dynamic_library_project()
#以下更改为静态库。
#add_static_library_project()

#以下动、静态编写形式，也是可以的。链接库只有CONFIG、自有宏，必须经过以上或者以下代码添加。
#本库编译为动态库
contains(DEFINES, LIB_LIBRARY):DEFINES += QQTEXQUISITE_LIBRARY
else:contains(DEFINES, LIB_STATIC_LIBRARY):DEFINES += QQTEXQUISITE_STATIC_LIBRARY

#本库依赖QQt
add_dependent_manager(QQt)

#本库导出SDK到LIB_SDK_ROOT
add_sdk(QQtExquisite, $$add_target_name())
#add_sdk_header_no_postfix(QQtExquisite, $$add_target_name(), QQtExquisite)

include(library_header.pri)
include(library_source.pri)
