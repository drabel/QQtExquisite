#ifndef QQTCHILDBODYKEEPSAMELEFTHELPER_H
#define QQTCHILDBODYKEEPSAMELEFTHELPER_H


#include <QObject>
#include <QEvent>
#include <QWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyKeepSameLeftHelper class
 * 使target保持和parent相同的LEFT边。
 */

class QQTEXQUISITESHARED_EXPORT QQtChildBodyKeepSameLeftHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyKeepSameLeftHelper ( QObject* parent = 0 )
        : QObject ( parent ) {
        targetParentWidget = 0;
        targetWidget = 0;
    }
    virtual ~QQtChildBodyKeepSameLeftHelper() {}

protected:

private:

private:
    QWidget* targetParentWidget;
    QWidget* targetWidget;


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTCHILDBODYKEEPSAMELEFTHELPER_H

