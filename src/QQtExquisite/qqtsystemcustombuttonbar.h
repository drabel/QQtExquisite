#ifndef QQTSYSTEMCUSTOMBUTTONBAR_H
#define QQTSYSTEMCUSTOMBUTTONBAR_H

#include <QWidget>
#include <qqtwidgets.h>
#include <qqtexquisite_global.h>

namespace Ui {
class QQtSystemCustomButtonBar;
}

/**
 * @brief The QQtSystemCustomButtonBar class
 * 系统按钮栏。右侧用。菜单、全屏、最小、最大、关闭。
 */
class QQTEXQUISITESHARED_EXPORT QQtSystemCustomButtonBar : public QWidget
{
    Q_OBJECT

public:
    explicit QQtSystemCustomButtonBar ( QWidget* parent = 0 );
    virtual ~QQtSystemCustomButtonBar();

#if 0
    void setMenuButtonImageTable ( const QImage& normal, const QImage& press, const QImage& hover, const QImage& disable );
    void setFullButtonImageTable ( const QImage& normal, const QImage& press, const QImage& hover, const QImage& disable );
    void setMinButtonImageTable ( const QImage& normal, const QImage& press, const QImage& hover, const QImage& disable );
    void setMaxButtonImageTable ( const QImage& normal, const QImage& press, const QImage& hover, const QImage& disable );
    void setRestoreButtonImageTable ( const QImage& normal, const QImage& press, const QImage& hover,
                                      const QImage& disable );
    void setCloseButtonImageTable ( const QImage& normal, const QImage& press, const QImage& hover, const QImage& disable );

    void setMenuButtonVisable ( bool visable );
    void setFullButtonVisable ( bool visable );
    void setMinButtonVisable ( bool visable );
    void setMaxButtonVisable ( bool visable );
    void setCloseButtonVisable ( bool visable );

    void setMenuButtonDisable ( bool visable );
    void setFullButtonDisable ( bool visable );
    void setMinButtonDisable ( bool visable );
    void setMaxButtonDisable ( bool visable );
    void setCloseButtonDisable ( bool visable );
#endif

private:
    Ui::QQtSystemCustomButtonBar* ui;

    QImage menuImage[BTN_MAX], fullScreenImage[BTN_MAX],
           minImage[BTN_MAX], maxImage[BTN_MAX], restoreImage[BTN_MAX],
           closeImage[BTN_MAX];
};

#endif // QQTSYSTEMCUSTOMBUTTONBAR_H
