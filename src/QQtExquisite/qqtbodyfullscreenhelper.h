#ifndef QQTBODYFULLSCREENHELPER_H
#define QQTBODYFULLSCREENHELPER_H

#include <QObject>

#include <QMouseEvent>
#include <QWidget>
#include <QEvent>
#include <QPoint>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtBodyFullscreenHelper class
 * 帮助Qt App子窗口全屏或者恢复显示。
 *
 * 暂时实现桌面，后续补充嵌入式桌面。
 */
class QQTEXQUISITESHARED_EXPORT QQtBodyFullscreenHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtBodyFullscreenHelper ( QObject* parent = 0 );
    virtual ~QQtBodyFullscreenHelper();

    //用户手动调用，自动切换全屏和正常显示
    void showFullscreenHelper ( QWidget* target = 0 );

protected:
    virtual void mouseDoubleClickEvent ( QMouseEvent* event, QWidget* target = 0 );

private:


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTBODYFULLSCREENHELPER_H

