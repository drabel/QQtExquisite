#ifndef QQTMULTIPAGEWIDGET_H
#define QQTMULTIPAGEWIDGET_H

#include "qqtexquisite_global.h"

#include <QObject>
#include <QQtWidget>
#include <QImage>

/**
 * @brief The QQtMultiPageWidget class
 * 包含三层Image的QQtWidget。
 * 用户只可以操作核心层，上层和下层是用来做效果用的，建议在eventFilter里面使用。
 *
 * 用户可以继承、可以安装eventFilter，对三层Image进行自定义绘制、设置图片。
 * 以middleImage为基准，
 * 用户可以修改下层和上层Image，对图片的上表面、背衬面进行修饰，但是不会影响原图片的显示。
 *
 * 存在的问题：
 * 这种Widget，即可以用以前的效果器，也可以使用专用的效果器。
 * 这种Widget，建议创建专用效果器。但是。。。好麻烦。。。而且重复工作过多。
 * 所以，不建议使用这种Widget。
 * QQtMultiEventFilterWidget是这个产品的美好替代品，完美的实现了event之前filter、之后catcher，复用过去的效果器。
 */
class QQTEXQUISITESHARED_EXPORT QQtMultiPageWidget: public QQtWidget
{
    Q_OBJECT
public:
    explicit QQtMultiPageWidget ( QWidget* parent = 0 );
    virtual ~QQtMultiPageWidget();

    //1. foreground image
    QImage& foreImage();
    //2. middle image 核心image 中间Image
    QImage& overlayImage();
    //3. background image
    QImage& backImage();

    //以下是对核心image，也就是中间Image的设置
    void setPixmap ( const QImage& image );
    void setPixmap ( const QString& pic );
    void setPixmap ( const QPixmap& pixmap );
    QImage image();
    void setImage ( const QImage& image );

    // QWidget interface
protected:
    virtual void paintEvent ( QPaintEvent* event ) override;

    virtual void paintBackgroundEvent ( QPaintEvent* event );
    virtual void paintOverLayEvent ( QPaintEvent* event );
    virtual void paintForegroundEvent ( QPaintEvent* event );

private:
    QImage mForeImage;
    QImage mOverImage;
    QImage mBackImage;
};

#endif // QQTMULTIPAGEWIDGET_H
