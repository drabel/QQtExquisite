﻿#include <qqtmultipagewidget.h>

QQtMultiPageWidget::QQtMultiPageWidget ( QWidget* parent ) : QQtWidget ( parent )
{
}

QQtMultiPageWidget::~QQtMultiPageWidget() {}

QImage& QQtMultiPageWidget::foreImage()
{
    return mForeImage;
}

QImage& QQtMultiPageWidget::overlayImage()
{
    return mOverImage;
}

QImage& QQtMultiPageWidget::backImage()
{
    return mBackImage;
}

void QQtMultiPageWidget::setPixmap ( const QImage& image )
{
    mOverImage = image;
    update();
}

void QQtMultiPageWidget::setPixmap ( const QString& pic )
{
    mOverImage.load ( pic );
    update();
}

void QQtMultiPageWidget::setPixmap ( const QPixmap& pixmap )
{
    mOverImage = pixmap.toImage();
    update();
}

QImage QQtMultiPageWidget::image()
{
    return mOverImage;
}

void QQtMultiPageWidget::setImage ( const QImage& image )
{
    setPixmap ( image );
}

void QQtMultiPageWidget::paintEvent ( QPaintEvent* event )
{
    paintBackgroundEvent ( event );
    paintOverLayEvent ( event );
    paintForegroundEvent ( event );
}

void QQtMultiPageWidget::paintBackgroundEvent ( QPaintEvent* event )
{
    if ( mBackImage.isNull() )
        return;
    QQtWidget::setImage ( mBackImage );
    QQtWidget::paintEvent ( event );
}

void QQtMultiPageWidget::paintOverLayEvent ( QPaintEvent* event )
{
    if ( mOverImage.isNull() )
        return QQtWidget::paintEvent ( event );
    QQtWidget::setImage ( mOverImage );
    QQtWidget::paintEvent ( event );
}

void QQtMultiPageWidget::paintForegroundEvent ( QPaintEvent* event )
{
    if ( mForeImage.isNull() )
        return;
    QQtWidget::setImage ( mForeImage );
    QQtWidget::paintEvent ( event );
}
