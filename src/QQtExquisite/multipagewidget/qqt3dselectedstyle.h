﻿#ifndef QQT3DSELECTEDSTYLE_H
#define QQT3DSELECTEDSTYLE_H

#include <QObject>

#include <qqtexquisite_global.h>

/**
 * @brief The QQt3DSelectedStyle class
 * 窗体选中效果
 *
 * 仅对QQt3DMultiPageWidget及其子类有效。
 * 能把选中框画在主图的上边。
 */
class QQTEXQUISITESHARED_EXPORT QQt3DSelectedStyle : public QObject
{
    Q_OBJECT

public:
    enum SelectedStyle
    {
        //default
        SelectedStyle_QtDesigner,
        SelectedStyle_QRCodeScaner,
        SelectedStyle_DottedLine,

        SelectedStyle_Max
    } ;

public:
    QQt3DSelectedStyle ( QObject* parent = 0 );
    virtual ~QQt3DSelectedStyle();

    void setSelectedStyle ( SelectedStyle style );
    SelectedStyle selectedStyle();

    void setSelected ( bool bSelected );
    bool selectedStatus();

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

private:
    bool hasSelected;
    SelectedStyle hasStyle;
};

#endif
