﻿#include <qqt3dmultipagewidget.h>

QQt3DMultiPageWidget::QQt3DMultiPageWidget ( QWidget* parent )
    : QQt3DWidget ( parent )
{
    fore3D = true;
    back3D = true;
}

QQt3DMultiPageWidget::~QQt3DMultiPageWidget() {}

void QQt3DMultiPageWidget::set_3d_for_background ( bool open )
{
    back3D = open;
    update();
}

void QQt3DMultiPageWidget::set_3d_for_foreground ( bool open )
{
    fore3D = open;
    update();
}

QImage& QQt3DMultiPageWidget::foreImage()
{
    return mForeImage;
}

QImage& QQt3DMultiPageWidget::overlayImage()
{
    return mOverImage;
}

QImage& QQt3DMultiPageWidget::backImage()
{
    return mBackImage;
}

void QQt3DMultiPageWidget::setPixmap ( const QImage& image )
{
    mOverImage = image;
    update();
}

void QQt3DMultiPageWidget::setPixmap ( const QString& pic )
{
    mOverImage.load ( pic );
    update();
}

void QQt3DMultiPageWidget::setPixmap ( const QPixmap& pixmap )
{
    mOverImage = pixmap.toImage();
    update();
}

QImage QQt3DMultiPageWidget::image()
{
    return mOverImage;
}

void QQt3DMultiPageWidget::setImage ( const QImage& image )
{
    setPixmap ( image );
}

void QQt3DMultiPageWidget::paintEvent ( QPaintEvent* event )
{
    //把3张图片render到1张图片上。
    paintBackgroundEvent ( event );
    paintOverLayEvent ( event );
    paintForegroundEvent ( event );
}


void QQt3DMultiPageWidget::paintBackgroundEvent ( QPaintEvent* event )
{
    if ( mBackImage.isNull() )
        return;
    if ( back3D )
    {
        QQt3DWidget::setImage ( mBackImage );
        QQt3DWidget::paintEvent ( event );
    }
    else
    {
        QQtWidget::setImage ( mBackImage );
        QQtWidget::paintEvent ( event );
    }
}

void QQt3DMultiPageWidget::paintOverLayEvent ( QPaintEvent* event )
{
    if ( mOverImage.isNull() )
        return QQtWidget::paintEvent ( event );
    QQt3DWidget::setImage ( mOverImage );
    QQt3DWidget::paintEvent ( event );
}

void QQt3DMultiPageWidget::paintForegroundEvent ( QPaintEvent* event )
{
    if ( mForeImage.isNull() )
        return;
    if ( fore3D )
    {
        QQt3DWidget::setImage ( mForeImage );
        QQt3DWidget::paintEvent ( event );
    }
    else
    {
        QQtWidget::setImage ( mForeImage );
        QQtWidget::paintEvent ( event );
    }
}
