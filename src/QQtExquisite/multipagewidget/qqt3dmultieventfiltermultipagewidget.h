#ifndef QQT3DMULTIEVENTFILTERMULTIPAGEWIDGET_H
#define QQT3DMULTIEVENTFILTERMULTIPAGEWIDGET_H


#include <QObject>
#include <QWidget>
#include <QEvent>

#include <qqt3dmultipagewidget.h>
#include <qqteventcatcherobject.h>
#include <qqtexquisite_global.h>
/**
 * @brief The QQt3DMultiEventFilterMultiPageWidget class
 */
class QQTEXQUISITESHARED_EXPORT QQt3DMultiEventFilterMultiPageWidget : public QQt3DMultiPageWidget
{
    Q_OBJECT

public:
    explicit QQt3DMultiEventFilterMultiPageWidget ( QWidget* parent = 0 );
    virtual ~QQt3DMultiEventFilterMultiPageWidget();

    //QObject interface
    void installEventFilter ( QObject* filterObj );
    void removeEventFilter ( QObject* obj );

    //在 paintEvent ... 之前
    //paintEvent() ...
    //在 paintEvent ... 之后

    //QQtMultiEventFilterObject interface
    void installEventCatcher ( QObject* catcherObj );
    void removeEventCatcher ( QObject* obj );

protected:

private:
    QQtEventCatcherObject catcherList;

    // QObject interface
public:
    virtual bool event ( QEvent* event ) override;

};

#endif // QQT3DMULTIEVENTFILTERMULTIPAGEWIDGET_H

