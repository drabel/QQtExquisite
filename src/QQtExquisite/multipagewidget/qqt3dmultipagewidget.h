﻿#ifndef QQT3DMULTIPAGEWIDGET_H
#define QQT3DMULTIPAGEWIDGET_H

#include <QObject>
#include <QQtWidget>
#include <QImage>

#include <qqtexquisite_global.h>
#include <qqt3dwidget.h>

/**
 * @brief The QQt3DMultiPageWidget class
 * 可以在xyz三个平面扭曲图片的3d multipage widget。
 * 有三层图片。用户应该使用核心层图片。上下两层用来做效果。
 * 上下两层可以选择是否开启3D效果。默认开启。
 *
 * 可以设置向三个平面的扭曲角度；
 * 可以设置旋转中心 --原点;
 * 可以按照画面中的比例位置设置；
 */
class QQTEXQUISITESHARED_EXPORT QQt3DMultiPageWidget : public QQt3DWidget
{
    Q_OBJECT
public:
    explicit QQt3DMultiPageWidget ( QWidget* parent = 0 );
    virtual ~QQt3DMultiPageWidget();

    void set_3d_for_background ( bool open = true );
    void set_3d_for_foreground ( bool open = true );


    //1. foreground image
    QImage& foreImage();
    //2. middle image 核心image 中间Image
    QImage& overlayImage();
    //3. background image
    QImage& backImage();

    //以下是对核心image，也就是中间Image的设置
    void setPixmap ( const QImage& image );
    void setPixmap ( const QString& pic );
    void setPixmap ( const QPixmap& pixmap );
    QImage image();
    void setImage ( const QImage& image );

    // QWidget interface
protected:
    virtual void paintEvent ( QPaintEvent* event ) override;

    // QQt3DMultiPageWidget interface
protected:
    virtual void paintBackgroundEvent ( QPaintEvent* event );
    virtual void paintOverLayEvent ( QPaintEvent* event );
    virtual void paintForegroundEvent ( QPaintEvent* event );

private:
    QImage mForeImage;
    QImage mOverImage;
    QImage mBackImage;
    bool fore3D, back3D;
};

#endif // QQT3DMULTIPAGEWIDGET_H
