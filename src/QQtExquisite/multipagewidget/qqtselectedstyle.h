﻿#ifndef QQTSELECTEDSTYLE_H
#define QQTSELECTEDSTYLE_H

#include <QObject>

#include <qqtexquisite_global.h>

/**
 * @brief The QQtSelectedStyle class
 * 窗体选中效果
 *
 * 仅对QQtMultiPageWidget及其子类有效。
 * 能把选中框画在主图的上边。
 */
class QQTEXQUISITESHARED_EXPORT QQtSelectedStyle : public QObject
{
    Q_OBJECT

public:
    enum SelectedStyle
    {
        //default
        SelectedStyle_QtDesigner,
        SelectedStyle_QRCodeScaner,
        SelectedStyle_DottedLine,

        SelectedStyle_Max
    } ;

public:
    QQtSelectedStyle ( QObject* parent = 0 );
    virtual ~QQtSelectedStyle();

    void setSelectedStyle ( SelectedStyle style );
    SelectedStyle selectedStyle();

    void setSelected ( bool bSelected );
    bool selectedStatus();

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

private:
    bool hasSelected;
    SelectedStyle hasStyle;
};

#endif
