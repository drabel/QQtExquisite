#include <qqtmultieventfiltermultipagewidget.h>


QQtMultiEventFilterMultiPageWidget::QQtMultiEventFilterMultiPageWidget ( QWidget* parent )
    : QQtMultiPageWidget ( parent )
{

}

QQtMultiEventFilterMultiPageWidget::~QQtMultiEventFilterMultiPageWidget()
{

}

void QQtMultiEventFilterMultiPageWidget::installEventFilter ( QObject* filterObj )
{
    catcherList.installEventFilter ( filterObj );
}

void QQtMultiEventFilterMultiPageWidget::removeEventFilter ( QObject* obj )
{
    catcherList.removeEventFilter ( obj );
}

void QQtMultiEventFilterMultiPageWidget::installEventCatcher ( QObject* catcherObj )
{
    catcherList.installEventCatcher ( catcherObj );
}

void QQtMultiEventFilterMultiPageWidget::removeEventCatcher ( QObject* obj )
{
    catcherList.removeEventCatcher ( obj );
}

bool QQtMultiEventFilterMultiPageWidget::event ( QEvent* event )
{
    bool ret = false;

    //eventFilter ... 先安装谁，就先执行谁。
    bool ret2 = catcherList.filter ( this, event );
    if ( ret2 )
        return ret2;

    //paintEvent ...
    ret = QQtMultiPageWidget::event ( event );
    //这里基本上都返回了 true ， 很少返回 false ， FocusAboutToChange 这个返回了false。
    //pline() << event->type() << ret;
    //if ( ret )
    //    return ret;

    //event被销毁了吗？没有。
    //paint event 现在还有效吗？还能用吗？能。

    //eventCatcher ... 先安装谁，就先执行谁。
    bool ret1 = catcherList.catcher ( this, event );
    if ( ret1 )
        return ret1;

    //这里我全都返回false，好不好？不好？我决定返回event()的返回值。
    return ret;
}
