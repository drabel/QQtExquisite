#ifndef QQTMULTIEVENTFILTERMULTIPAGEWIDGET_H
#define QQTMULTIEVENTFILTERMULTIPAGEWIDGET_H

#include <QObject>
#include <QWidget>
#include <QEvent>

#include <qqtmultipagewidget.h>
#include <qqteventcatcherobject.h>
#include <qqtexquisite_global.h>
/**
 * @brief The QQtMultiEventFilterMultiPageWidget class
 */
class QQTEXQUISITESHARED_EXPORT QQtMultiEventFilterMultiPageWidget : public QQtMultiPageWidget
{
    Q_OBJECT

public:
    explicit QQtMultiEventFilterMultiPageWidget ( QWidget* parent = 0 );
    virtual ~QQtMultiEventFilterMultiPageWidget();

    //QObject interface
    void installEventFilter ( QObject* filterObj );
    void removeEventFilter ( QObject* obj );

    //在 paintEvent ... 之前
    //paintEvent() ...
    //在 paintEvent ... 之后

    //QQtMultiEventFilterObject interface
    void installEventCatcher ( QObject* catcherObj );
    void removeEventCatcher ( QObject* obj );
protected:

private:
    QQtEventCatcherObject catcherList;

    // QObject interface
public:
    virtual bool event ( QEvent* event ) override;

};

#endif // QQTMULTIEVENTFILTERMULTIPAGEWIDGET_H

