﻿#include <qqt3dselectedstyle.h>

#include <QEvent>
#include <QPainter>
#include <QWidget>
#include <QImage>
#include <qqtcore.h>
#include <qqt3dmultipagewidget.h>

QQt3DSelectedStyle::QQt3DSelectedStyle ( QObject* parent ) : QObject ( parent )
{
    hasSelected = false;
    hasStyle = SelectedStyle_QtDesigner;
}

QQt3DSelectedStyle::~QQt3DSelectedStyle()
{

}

void QQt3DSelectedStyle::setSelectedStyle ( QQt3DSelectedStyle::SelectedStyle style )
{
    hasStyle = style;
}

QQt3DSelectedStyle::SelectedStyle QQt3DSelectedStyle::selectedStyle()
{
    return hasStyle;
}

void QQt3DSelectedStyle::setSelected ( bool bSelected )
{
    hasSelected = bSelected;
}

bool QQt3DSelectedStyle::selectedStatus()
{
    return hasSelected;
}


bool QQt3DSelectedStyle::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QQt3DMultiPageWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
    {
        if ( hasSelected )
        {
            QQt3DMultiPageWidget* target0 = qobject_cast<QQt3DMultiPageWidget*> ( watched );
            QImage& target1 = target0->foreImage();

            QImage target2 ( target0->size(), QImage::Format_ARGB32 );
            QImage* target = &target2;
            target->fill ( Qt::transparent );

            QPainter painter ( target );
            //不管用？
            //painter.fillRect ( target0->foreImage().rect(), Qt::transparent );
            painter.setRenderHint ( QPainter::Antialiasing );
            painter.setRenderHint ( QPainter::TextAntialiasing );
            painter.setRenderHint ( QPainter::SmoothPixmapTransform );
            painter.setRenderHint ( QPainter::HighQualityAntialiasing );

            int w = 10;
            switch ( hasStyle )
            {
                case SelectedStyle_QtDesigner:
                {
                    painter.setPen ( Qt::black );
                    painter.drawRect ( target->rect() );
                    painter.drawRect ( 0, 0, w, w );
                    painter.drawRect ( target->size().width() - w, 0, w, w );
                    painter.drawRect ( 0, target->size().height() - w, w, w );
                    painter.drawRect ( target->size().width() - w, target->size().height() - w, w, w );
                }
                break;
                case SelectedStyle_QRCodeScaner:
                {
                    painter.setPen ( Qt::black );
                    painter.drawLine ( 0, 0, w, 0 );
                    painter.drawLine ( 0, 0, 0, w );

                    painter.drawLine ( target->size().width() - w, 0, target->size().width(), 0 );
                    painter.drawLine ( target->size().width(), 0, target->size().width(), w );

                    painter.drawLine ( 0, target->size().height() - w, 0, target->size().height() );
                    painter.drawLine ( 0, target->size().height(), w, target->size().height() );

                    painter.drawLine ( target->size().width(), target->size().height() - w,
                                       target->size().width(), target->size().height() );
                    painter.drawLine ( target->size().width() - w, target->size().height(),
                                       target->size().width(), target->size().height() );
                }
                break;
                case SelectedStyle_DottedLine:
                {
                    painter.setPen ( Qt::black );
                    painter.setPen ( Qt::DotLine );
                    painter.drawRect ( target->rect() );
                }
                break;
                default:
                    break;
            }

            target1 = target2;
        }
        return false;
    }

    //static int i = 0;
    //p2line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        //用户有需要自行添加
        //case QEvent::Enter:
        //case QEvent::Leave:

        case QEvent::FocusIn:
        {
            hasSelected = true;
            //QWidget* target = qobject_cast<QWidget*> ( watched );
            //target->update();
            return false;
        }
        break;
        case QEvent::FocusOut:
        {
            hasSelected = false;
            //QWidget* target = qobject_cast<QWidget*> ( watched );
            //target->update();
            return false;
        }
        break;
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
