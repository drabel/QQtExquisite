#ifndef QQTLIGHTWIDGETHELPER_H
#define QQTLIGHTWIDGETHELPER_H

#include <QObject>
#include <QWidget>
#include <QEvent>
#include <QPaintEvent>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtLightWidgetHelper class
 * 灯控件的事件过滤器
 *
 * 设置灯样式
 * 开启、关闭灯
 *
 * 未实现
 */
class QQTEXQUISITESHARED_EXPORT QQtLightWidgetHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtLightWidgetHelper ( QObject* parent = 0 );
    virtual ~QQtLightWidgetHelper();

    enum LightStyle
    {
        LightStyle_Circle_Dot,

        LightStyle_Max
    };

    void setLightStyle ( LightStyle style );
    LightStyle lightStyle();

    void setLightStatus ( bool on );
    bool lightStatus();

protected:
    virtual void paintEvent ( QPaintEvent* e, QWidget* target );

private:
    LightStyle mStyle;
    bool mON;

    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // QQTLIGHTWIDGETHELPER_H

