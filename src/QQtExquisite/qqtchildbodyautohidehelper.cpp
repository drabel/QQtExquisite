#include <qqtchildbodyautohidehelper.h>

#include <qqtbodyautohidehelper.h>

#include <QEvent>
#include <QWidget>

#include <qqtcore.h>

QQtChildBodyAutoHideHelper::QQtChildBodyAutoHideHelper ( QObject* parent ) : QObject ( parent )
{
    mTimerInterval = 1000;

    timer = new QTimer ( this );
    timer->setSingleShot ( true );
    connect ( timer, SIGNAL ( timeout() ), this, SLOT ( slotHide() ) );

    direction = None;
    localTarget = 0;

    timer2 = new QTimer ( this );
    timer2->setSingleShot ( true );
    connect ( timer2, SIGNAL ( timeout() ), this, SLOT ( slotShow() ) );

    mShowWidth = 10;
    mEffectWidth = 80;

    csArray[0] = CornerStyle_Default;
    csArray[1] = CornerStyle_Default;
    csArray[2] = CornerStyle_Default;
    csArray[3] = CornerStyle_Default;
}

QQtChildBodyAutoHideHelper::~QQtChildBodyAutoHideHelper() {}

void QQtChildBodyAutoHideHelper::setCornerStyle ( QQtChildBodyAutoHideHelper::MoveDirection direction,
                                                  QQtChildBodyAutoHideHelper::CornerStyle style )
{
    switch ( direction )
    {
        case LeftTop:
            csArray[0] = style;
            break;
        case LeftBottom:
            csArray[1] = style;
            break;
        case RightTop:
            csArray[2] = style;
            break;
        case RightBottom:
            csArray[3] = style;
            break;
        default:
            break;
    }
}

QQtChildBodyAutoHideHelper::CornerStyle QQtChildBodyAutoHideHelper::cornerStyle()
{
    switch ( direction )
    {
        case LeftTop:
            return csArray[0];
            break;
        case LeftBottom:
            return csArray[1];
            break;
        case RightTop:
            return csArray[2];
            break;
        case RightBottom:
            return csArray[3];
            break;
        default:
            break;
    }
    return CornerStyle_Max;
}

void QQtChildBodyAutoHideHelper::setTimerInterval ( int millsecond )
{
    mTimerInterval = millsecond;
}

int QQtChildBodyAutoHideHelper::timerInterval()
{
    return mTimerInterval;
}

void QQtChildBodyAutoHideHelper::setShowWidth ( int width )
{
    mShowWidth = width;
}

int QQtChildBodyAutoHideHelper::showWidth()
{
    return mShowWidth;
}

void QQtChildBodyAutoHideHelper::setEffectWidth ( int width )
{
    mEffectWidth = width;
}

int QQtChildBodyAutoHideHelper::effectWidth()
{
    return mEffectWidth;
}


void QQtChildBodyAutoHideHelper::enterEvent ( QEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    //无论如何关闭timer
    timer->stop();

    //这里必须用target，这代表子窗口。
    QWidget* win = target;//target->window();

    if ( win->isMaximized() )
    {
        event->ignore();
        return;
    }
    checkDirection ( win );

    localTarget = target;

    if ( checkHideStatus ( target ) )
        timer2->start();

    event->accept();
}

void QQtChildBodyAutoHideHelper::leaveEvent ( QEvent* event, QWidget* target )
{
    Q_ASSERT ( target );

    //但凡应用于主窗口，一律使用window()。
    QWidget* win = target;//target->window();

    if ( win->isMaximized() )
    {
        event->ignore();
        return;
    }
    checkDirection ( win );

    localTarget = target;

    //判断目标区域，如果包含鼠标，那么不能启动隐藏。
    //if ( checkIfMousePositionLegal ( target ) )
    timer->start ( mTimerInterval );

    event->accept();
}

//根据target和DesktopWidget的位置关系，来确定方向。
void QQtChildBodyAutoHideHelper::checkDirection ( QWidget* target )
{
    //判断位置
    //子窗体，根据？，判断位置 parent 如果parent=0，那么direction=None
    //顶级窗体，根据和DesktopWidget的位置关系，判断位置
    Q_ASSERT ( target );

    QMargins m_margins = target->contentsMargins();

#if 1
    //maptoGlobal(rect()) 与 globalPos 对比
    QRect rectMustIn = QRect ( target->mapToGlobal ( target->rect().topLeft() ),
                               target->mapToGlobal ( target->rect().bottomRight() ) );
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    QRect rectMustNotIn = rectMustIn.adjusted ( m_margins.left(), m_margins.top(), m_margins.right(), m_margins.bottom() );
#else
    QRect rectMustNotIn = rectMustIn.marginsRemoved ( m_margins );
#endif
    QPoint cursorPos = QCursor::pos();
    //经过测试，这种方法，在子窗口、root窗口，得到的数据完全正常。
    //pline() << target->geometry() << rectMustIn
    //        << rectMustIn.contains ( event->globalPos() ) << rectMustNotIn.contains ( event->globalPos() );
#endif

    if ( target->isMaximized() )
    {
        direction = None;
        return;
    }

    int baseLeftCoord, baseTopCoord, baseRightCoord, baseBottomCoord;
    rectMustNotIn.getCoords ( &baseLeftCoord, &baseTopCoord, &baseRightCoord, &baseBottomCoord );

    //废弃
    int x, y;
    x = cursorPos.x();
    y = cursorPos.y();

    QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
    QRect deskGeometry = QRect ( parentWidget->mapToGlobal ( parentWidget->rect().topLeft() ),
                                 parentWidget->mapToGlobal ( parentWidget->rect().bottomRight() ) );
    //QRect deskGeometry = parentWidget->rect();

    int effectWidth = mEffectWidth;

    direction = None;
    //left 左侧边缘和桌面左侧对比
    if ( rectMustIn.left() < deskGeometry.left()  + effectWidth )
    {
        if ( rectMustIn.top()  < deskGeometry.top() + effectWidth )
        {
            direction = LeftTop;
        }
        else if ( rectMustIn.bottom() > deskGeometry.bottom() - effectWidth )
        {
            direction = LeftBottom;
        }
        else
        {
            direction = Left;
        }
    }
    //right
    else if ( rectMustIn.right() > deskGeometry.right() - effectWidth )
    {
        if ( rectMustIn.top()  < deskGeometry.top() + effectWidth )
        {
            direction = RightTop;
        }
        else if ( rectMustIn.bottom() > deskGeometry.bottom() - effectWidth )
        {
            direction = RightBottom;
        }
        else
        {
            direction = Right;
        }
    }
    //middle
    else
    {
        if ( rectMustIn.top()  < deskGeometry.top() + effectWidth )
        {
            direction = Top;
        }
        else if ( rectMustIn.bottom() > deskGeometry.bottom() - effectWidth )
        {
            direction = Bottom;
        }
    }

}

//无论如何和slotHide代码相同。
bool QQtChildBodyAutoHideHelper::checkIfMousePositionLegal ( QWidget* target )
{
    //QWidget* target = localTarget;
    QWidget* win = target;//target->window();

    //根据位置，设置geometry
    //如果鼠标pressed，那么移动。
    if ( !win->isMaximized() )
    {
        QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
        QRect deskGeometry = parentWidget->rect();
        int showWidth = mShowWidth;

        QRect oldGeometry = win->geometry();
        QPoint widgetPos = oldGeometry.topLeft();

        int rx, ry;
        int x, y, width = 0, height = 0;

        QRect newGeometry = oldGeometry;

        switch ( direction )
        {
            case Left: // left
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );
                break;

            case Right: //right
                //widget
                x = deskGeometry.right() - showWidth;
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );
                break;

            case Top: //top
                //widget
                x = oldGeometry.left();
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );
                break;

            case Bottom: //bottom
                //widget
                x = oldGeometry.left();
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );
                break;

            case LeftTop: //left top
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );
                break;

            case RightTop: //right top
                //widget
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );
                break;

            case LeftBottom: //left bottom
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );
                break;

            case RightBottom: //right bottom
                //widget
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );
                break;

            default:
                break;
        }

        //期望geometry不一定能设置到。
        //在角上隐藏的时候，有三个彧，这三个域都不能包含鼠标
        QRect newG1 = newGeometry, newG2 = newGeometry;
#if 0
        switch ( direction )
        {
            case LeftTop: //left top
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );

                //固定left
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();
                newG1 = QRect ( x, y, width, height );

                //固定top
                x = deskGeometry.left();
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newG2 = QRect ( x, y, width, height );

                break;

            case RightTop: //right top
                //widget
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );

                //固定Right
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();
                newG1 = QRect ( x, y, width, height );

                //固定Top
                x = deskGeometry.right() - oldGeometry.width();
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newG2 = QRect ( x, y, width, height );

                break;

            case LeftBottom: //left bottom
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );

                //固定left
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.bottom() - oldGeometry.height();
                width = oldGeometry.width();
                height = oldGeometry.height();
                newG1 = QRect ( x, y, width, height );
                //固定bottom
                x = deskGeometry.left();
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newG2 = QRect ( x, y, width, height );

                break;

            case RightBottom: //right bottom
                //widget
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newGeometry = QRect ( x, y, width, height );

                //固定right
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.bottom() - oldGeometry.height();
                width = oldGeometry.width();
                height = oldGeometry.height();
                newG1 = QRect ( x, y, width, height );

                //固定bottom
                x = deskGeometry.right() - oldGeometry.width();
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();
                newG2 = QRect ( x, y, width, height );

                break;

            default:
                break;
        }
#endif
        switch ( direction )
        {
            case Left:
            case Top:
            case Right:
            case Bottom:
            case LeftTop:
            case RightTop:
            case RightBottom:
            case LeftBottom:
                break;
            default:
                //pline() << "no need set hidden.";
                return false;
                break;
        }

#if 0
        QMargins m_margins = win->contentsMargins();
        if ( width > ( m_margins.left() + m_margins.right() ) &&
             height > ( m_margins.top() + m_margins.bottom() ) )
        {
            //win->setGeometry ( newGeometry );
            QPoint cursorPos = QCursor::pos();
            if ( newGeometry.contains ( cursorPos ) || newG1.contains ( cursorPos ) || newG2.contains ( cursorPos ) )
            {
                //pline() << "no need set hidden.";
                return false;
            }
        }
#endif
    }

    return true;

    //用geometry也是对的。
    //顶层窗口对target、对win设置geometry都是对的?错误。必须对win设置！！！它才是真正的root窗口。target不一定！
#if 0
    pline() << target->objectName() << win->objectName();
    pline() << target->parent();
    if ( target->parent() )
        pline() << target->parent()->objectName();
    pline() << direction << target->geometry()
            << win->geometry() << win->frameGeometry()
            << QApplication::desktop()->availableGeometry();
#endif

}

bool QQtChildBodyAutoHideHelper::checkHideStatus ( QWidget* target )
{
    QWidget* win = target;//target->window();

    //根据位置，设置geometry
    //如果鼠标pressed，那么移动。
    if ( !win->isMaximized() )
    {
        QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
        //QRect deskGeometry = QRect ( parentWidget->mapToGlobal ( parentWidget->rect().topLeft() ),
        //                             parentWidget->mapToGlobal ( parentWidget->rect().bottomRight() ) );
        QRect deskGeometry = parentWidget->rect();
        int showWidth = mShowWidth;

        QRect oldGeometry = win->geometry();
        QPoint widgetPos = oldGeometry.topLeft();

        int rx, ry;
        int x, y, width = 0, height = 0;

        QRect newGeometry = oldGeometry;

        switch ( direction )
        {
            case Left: // left
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Right: //right
                //widget
                x = deskGeometry.right() - showWidth;
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            case LeftTop: //left top
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            case RightTop: //right top
                //widget
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case LeftBottom: //left bottom
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case RightBottom: //right bottom
                //widget
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Top: //top
                //widget
                x = oldGeometry.left();
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Bottom: //bottom
                //widget
                x = oldGeometry.left();
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            default:
                break;
        }

        switch ( direction )
        {
            case LeftTop: //left top
                switch ( csArray[0] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.left() - oldGeometry.width() + showWidth;
                        y = deskGeometry.top() - oldGeometry.height() + showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //top
                        x = oldGeometry.left();
                        y = deskGeometry.top() - oldGeometry.height() + showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //left
                        x = deskGeometry.left() - oldGeometry.width() + showWidth;
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            case RightTop: //right top
                switch ( csArray[2] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.right() - showWidth;
                        y = deskGeometry.top() - oldGeometry.height() + showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //top
                        x = oldGeometry.left();
                        y = deskGeometry.top() - oldGeometry.height() + showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //right
                        x = deskGeometry.right() - showWidth;
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            case LeftBottom: //left bottom
                switch ( csArray[1] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.left() - oldGeometry.width() + showWidth;
                        y = deskGeometry.bottom() - showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //left
                        x = deskGeometry.left() - oldGeometry.width() + showWidth;
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //bottom
                        x = oldGeometry.left();
                        y = deskGeometry.bottom() - showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            case RightBottom: //right bottom
                switch ( csArray[3] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.right() - showWidth;
                        y = deskGeometry.bottom() - showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //right
                        x = deskGeometry.right() - showWidth;
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //bottom
                        x = oldGeometry.left();
                        y = deskGeometry.bottom() - showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            default:
                break;
        }

        switch ( direction )
        {
            case Left:
            case Top:
            case Right:
            case Bottom:
            case LeftTop:
            case RightTop:
            case RightBottom:
            case LeftBottom:
                break;
            default:
                //此处不会到达，被checkIfMousePostionLegal拦截
                //direction为None也会设置。
                //pline() << "no need set hidden.";
                return false;
                break;
        }

        QMargins m_margins = win->contentsMargins();

        if ( width > ( m_margins.left() + m_margins.right() ) &&
             height > ( m_margins.top() + m_margins.bottom() ) )
        {
#if 0
            //此处不会到达，被checkIfMousePostionLegal拦截
            //到达的时候，肯定是可以接受设置的。
            //此处不判断，无论如何都设置。
            QPoint cursorPos = QCursor::pos();
            if ( newGeometry.contains ( cursorPos ) )
            {
                //pline() << "no need set hidden.";
                return;
            }
#endif
            //win->setGeometry ( newGeometry );
            if ( oldGeometry == newGeometry )
                return true;
            return false;
        }
    }

    return false;

    //用geometry也是对的。
    //顶层窗口对target、对win设置geometry都是对的?错误。必须对win设置！！！它才是真正的root窗口。target不一定！
#if 0
    pline() << target->objectName() << win->objectName();
    pline() << target->parent();
    if ( target->parent() )
        pline() << target->parent()->objectName();
    pline() << direction << target->geometry()
            << win->geometry() << win->frameGeometry()
            << QApplication::desktop()->availableGeometry();
#endif
}

bool QQtChildBodyAutoHideHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    QWidget* target = qobject_cast<QWidget*> ( watched );
    if ( ! target->parent() )
        return QObject::eventFilter ( watched, event );

    if ( !target->parent()->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::Enter:
        {
            QEvent* e = ( QEvent* ) event;
            enterEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        case QEvent::Leave:
        {
            QEvent* e = ( QEvent* ) event;
            leaveEvent ( e, qobject_cast<QWidget*> ( watched ) );
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}

//铁定了用于隐藏
void QQtChildBodyAutoHideHelper::slotHide()
{
    QWidget* target = localTarget;
    QWidget* win = target;//target->window();

    //根据位置，设置geometry
    //如果鼠标pressed，那么移动。
    if ( !win->isMaximized() )
    {
        QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
        //QRect deskGeometry = QRect ( parentWidget->mapToGlobal ( parentWidget->rect().topLeft() ),
        //                             parentWidget->mapToGlobal ( parentWidget->rect().bottomRight() ) );
        QRect deskGeometry = parentWidget->rect();
        int showWidth = mShowWidth;

        QRect oldGeometry = win->geometry();
        QPoint widgetPos = oldGeometry.topLeft();

        int rx, ry;
        int x, y, width = 0, height = 0;

        QRect newGeometry = oldGeometry;

        switch ( direction )
        {
            case Left: // left
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Right: //right
                //widget
                x = deskGeometry.right() - showWidth;
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            case LeftTop: //left top
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            case RightTop: //right top
                //widget
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case LeftBottom: //left bottom
                //widget
                x = deskGeometry.left() - oldGeometry.width() + showWidth;
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case RightBottom: //right bottom
                //widget
                x = deskGeometry.right() - showWidth;
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Top: //top
                //widget
                x = oldGeometry.left();
                y = deskGeometry.top() - oldGeometry.height() + showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Bottom: //bottom
                //widget
                x = oldGeometry.left();
                y = deskGeometry.bottom() - showWidth;
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            default:
                break;
        }

        switch ( direction )
        {
            case LeftTop: //left top
                switch ( csArray[0] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.left() - oldGeometry.width() + showWidth;
                        y = deskGeometry.top() - oldGeometry.height() + showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //top
                        x = oldGeometry.left();
                        y = deskGeometry.top() - oldGeometry.height() + showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //left
                        x = deskGeometry.left() - oldGeometry.width() + showWidth;
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            case RightTop: //right top
                switch ( csArray[2] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.right() - showWidth;
                        y = deskGeometry.top() - oldGeometry.height() + showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //top
                        x = oldGeometry.left();
                        y = deskGeometry.top() - oldGeometry.height() + showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //right
                        x = deskGeometry.right() - showWidth;
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            case LeftBottom: //left bottom
                switch ( csArray[1] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.left() - oldGeometry.width() + showWidth;
                        y = deskGeometry.bottom() - showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //left
                        x = deskGeometry.left() - oldGeometry.width() + showWidth;
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //bottom
                        x = oldGeometry.left();
                        y = deskGeometry.bottom() - showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            case RightBottom: //right bottom
                switch ( csArray[3] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.right() - showWidth;
                        y = deskGeometry.bottom() - showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //right
                        x = deskGeometry.right() - showWidth;
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //bottom
                        x = oldGeometry.left();
                        y = deskGeometry.bottom() - showWidth;
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            default:
                break;
        }

        switch ( direction )
        {
            case Left:
            case Top:
            case Right:
            case Bottom:
            case LeftTop:
            case RightTop:
            case RightBottom:
            case LeftBottom:
                break;
            default:
                //此处不会到达，被checkIfMousePostionLegal拦截
                //direction为None也会设置。
                //pline() << "no need set hidden.";
                return;
                break;
        }

        QMargins m_margins = win->contentsMargins();

        if ( width > ( m_margins.left() + m_margins.right() ) &&
             height > ( m_margins.top() + m_margins.bottom() ) )
        {
#if 0
            //此处不会到达，被checkIfMousePostionLegal拦截
            //到达的时候，肯定是可以接受设置的。
            //此处不判断，无论如何都设置。
            QPoint cursorPos = QCursor::pos();
            if ( newGeometry.contains ( cursorPos ) )
            {
                //pline() << "no need set hidden.";
                return;
            }
#endif
            win->setGeometry ( newGeometry );
        }
    }

    //用geometry也是对的。
    //顶层窗口对target、对win设置geometry都是对的?错误。必须对win设置！！！它才是真正的root窗口。target不一定！
#if 0
    pline() << target->objectName() << win->objectName();
    pline() << target->parent();
    if ( target->parent() )
        pline() << target->parent()->objectName();
    pline() << direction << target->geometry()
            << win->geometry() << win->frameGeometry()
            << QApplication::desktop()->availableGeometry();
#endif
}

void QQtChildBodyAutoHideHelper::slotShow()
{
    QWidget* target = localTarget;
    QWidget* win = target;//target->window();

    //根据位置，设置geometry
    //如果鼠标pressed，那么移动。
    if ( !win->isMaximized() )
    {
        QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
        QRect deskGeometry = parentWidget->rect();

        QRect oldGeometry = win->geometry();
        QPoint widgetPos = oldGeometry.topLeft();

        int rx, ry;
        int x, y, width = 0, height = 0;

        QRect newGeometry = oldGeometry;

        switch ( direction )
        {
            case Left: // left
                //widget
                x = deskGeometry.left();
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            case Right: //right
                //widget
                x = deskGeometry.right() - oldGeometry.width();
                y = oldGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            case LeftTop: //left top
                //widget
                x = deskGeometry.left();
                y = deskGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            case RightTop: //right top
                //widget
                x = deskGeometry.right() - oldGeometry.width();
                y = deskGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case LeftBottom: //left bottom
                //widget
                x = deskGeometry.left();
                y = deskGeometry.bottom() - oldGeometry.height();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case RightBottom: //right bottom
                //widget
                x = deskGeometry.right() - oldGeometry.width();
                y = deskGeometry.bottom() - oldGeometry.height();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Top: //top
                //widget
                x = oldGeometry.left();
                y = deskGeometry.top();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );

                break;

            case Bottom: //bottom
                //widget
                x = oldGeometry.left();
                y = deskGeometry.bottom() - oldGeometry.height();
                width = oldGeometry.width();
                height = oldGeometry.height();

                newGeometry = QRect ( x, y, width, height );
                break;

            default:
                break;
        }

        switch ( direction )
        {
            case LeftTop: //left top
                switch ( csArray[0] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.left();
                        y = deskGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //top
                        x = oldGeometry.left();
                        y = deskGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //left
                        x = deskGeometry.left();
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            case RightTop: //right top
                switch ( csArray[2] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.right() - oldGeometry.width();
                        y = deskGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //top
                        x = oldGeometry.left();
                        y = deskGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //right
                        x = deskGeometry.right() - oldGeometry.width();
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            case LeftBottom: //left bottom
                switch ( csArray[1] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.left();
                        y = deskGeometry.bottom() - oldGeometry.height();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //left
                        x = deskGeometry.left();
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //bottom
                        x = oldGeometry.left();
                        y = deskGeometry.bottom() - oldGeometry.height();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            case RightBottom: //right bottom
                switch ( csArray[3] )
                {
                    case CornerStyle_Default:
                        //widget
                        x = deskGeometry.right() - oldGeometry.width();
                        y = deskGeometry.bottom() - oldGeometry.height();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_A:
                        //right
                        x = deskGeometry.right() - oldGeometry.width();
                        y = oldGeometry.top();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    case CornerStyle_B:
                        //bottom
                        x = oldGeometry.left();
                        y = deskGeometry.bottom() - oldGeometry.height();
                        width = oldGeometry.width();
                        height = oldGeometry.height();
                        newGeometry = QRect ( x, y, width, height );
                        break;
                    default:
                        break;
                }

                break;

            default:
                break;
        }

        switch ( direction )
        {
            case Left:
            case Top:
            case Right:
            case Bottom:
            case LeftTop:
            case RightTop:
            case RightBottom:
            case LeftBottom:
                break;
            default:
                //由于总是发生，此处添加return
                //pline() << "no need set shown.";
                return;
                break;
        }

        QMargins m_margins = win->contentsMargins();

        if ( width > ( m_margins.left() + m_margins.right() ) &&
             height > ( m_margins.top() + m_margins.bottom() ) )
        {
            win->setGeometry ( newGeometry );
        }
    }

    //用geometry也是对的。
    //顶层窗口对target、对win设置geometry都是对的?错误。必须对win设置！！！它才是真正的root窗口。target不一定！
#if 0
    pline() << target->objectName() << win->objectName();
    pline() << target->parent();
    if ( target->parent() )
        pline() << target->parent()->objectName();
    pline() << direction << target->geometry()
            << win->geometry() << win->frameGeometry()
            << QApplication::desktop()->availableGeometry();
#endif
}
