#ifndef QQTCHILDBODYKEEPSAMEWIDTHHELPER_H
#define QQTCHILDBODYKEEPSAMEWIDTHHELPER_H


#include <QObject>
#include <QEvent>
#include <QWidget>

#include <qqtexquisite_global.h>
/**
 * @brief The QQtChildBodyKeepSameWidthHelper class
 * 使target保持和parent相同宽度。
 */
class QQTEXQUISITESHARED_EXPORT QQtChildBodyKeepSameWidthHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtChildBodyKeepSameWidthHelper ( QObject* parent = 0 )
        : QObject ( parent ) {
        targetParentWidget = 0;
        targetWidget = 0;
    }
    virtual ~QQtChildBodyKeepSameWidthHelper() {}

protected:


private:
    QWidget* targetParentWidget;
    QWidget* targetWidget;


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

};

#endif // QQTCHILDBODYKEEPSAMEWIDTHHELPER_H

