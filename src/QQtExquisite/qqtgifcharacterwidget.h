#ifndef QQTGIFCHARACTERWIDGET_H
#define QQTGIFCHARACTERWIDGET_H

#include <qqtgifwidget.h>
#include <qqtexquisite_global.h>

/**
 * @brief The QQtGifCharacterWidget class
 * Gif图片使用字符图片显示。
 */
class QQTEXQUISITESHARED_EXPORT QQtGifCharacterWidget : public QQtGifWidget
{
    Q_OBJECT

public:
    explicit QQtGifCharacterWidget ( QWidget* parent = 0 );
    virtual ~QQtGifCharacterWidget();

protected:
    void setCharactorImage ( const QImage& image );

protected:
    virtual char makeChar ( int g );
    virtual int rgbtoGray ( int r, int g, int b );

protected slots:
    virtual void slotFramePlayback();

protected:

private:

};

#endif // QQTGIFCHARACTERWIDGET_H

