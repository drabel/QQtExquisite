#ifndef QQTSHAKEHELPER_H
#define QQTSHAKEHELPER_H

#include <qobject.h>
#include <qpropertyanimation.h>
#include <qwidget.h>

#include <qqtexquisite_global.h>

/**
 * @brief The QQtShakeHelper class
 * 帮助Widget震动。
 *
 * 新的shark会终止旧的shark。
 * 在更换内部widget的时候，无论是否还是原先的widget，都会终止原先的widget shark。
 * 不会飘。
 */
class QQTEXQUISITESHARED_EXPORT QQtShakeHelper : public QObject
{
    Q_OBJECT
public:
    explicit QQtShakeHelper ( QObject* parent = 0 );
    virtual ~QQtShakeHelper();

    void setShakeWidget ( QWidget* shakeWidget );

public slots:
    void shake();
    void endShake();

protected:
    virtual void setShakeSequeues();

protected:
    QWidget* wSkWidget;
    QPropertyAnimation* m_animation;
    QRect startGeometry;
};

#endif //QQTSHAKEHELPER_H
