#include <qqtchildbodykeepsameposhelper.h>
#include <QEvent>
#include <QWidget>
#include <qqtcore.h>
#include <qqtwidgets.h>


bool QQtChildBodyKeepSamePosHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
        return QObject::eventFilter ( watched, event );

    QWidget* target = qobject_cast<QWidget*> ( watched );
    if ( !target )
        return QObject::eventFilter ( watched, event );

    if ( target == targetParentWidget )
    {
        switch ( event->type() )
        {
            case QEvent::Resize:
            {
                QResizeEvent* e = ( QResizeEvent* ) event;

                int x = targetParentWidget->rect().left();
                int y = targetParentWidget->rect().top();
                int width  = targetWidget->rect().width();
                int height = targetWidget->rect().height();
                targetWidget->setGeometry ( QRect ( x, y, width, height ) );

                e->accept();
                return false;
            }
            default:
                break;
        }
        return QObject::eventFilter ( watched, event );
    }

    if ( ! target->parent() )
        return QObject::eventFilter ( watched, event );

    if ( !target->parent()->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::Show:
        {
            QShowEvent* e = ( QShowEvent* ) event;
            QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
            targetParentWidget = parentWidget;
            targetWidget = target;
            //这个函数是否应当保留？

            int x = targetParentWidget->rect().left();
            int y = targetParentWidget->rect().top();
            int width  = targetWidget->rect().width();
            int height = targetWidget->rect().height();
            targetWidget->setGeometry ( QRect ( x, y, width, height ) );

            parentWidget->installEventFilter ( this );
            e->accept();
            return false;
        }
        case QEvent::Hide:
        {
            QHideEvent* e = ( QHideEvent* ) event;
            QWidget* parentWidget = qobject_cast<QWidget*> ( target->parent() );
            targetParentWidget = 0;
            targetWidget = 0;
            parentWidget->removeEventFilter ( this );
            e->accept();
            return false;
        }
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
