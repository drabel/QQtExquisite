#include <qqtsplashscreenhelper.h>

#include <QObject>
#include <QTimer>
#include <QWidget>
#include <QCoreApplication>

QQtSplashScreenHelper::QQtSplashScreenHelper ( QObject* parent )
    : QObject ( parent )
{
    target = 0;

    timer = new QTimer ( this );
    timer->setSingleShot ( false );
    //timer->setInterval ( 20 );
    connect ( timer, SIGNAL ( timeout() ), this, SLOT ( slotTimeout() ) );
    timer->start();
}

QQtSplashScreenHelper::~QQtSplashScreenHelper() {}

void QQtSplashScreenHelper::setSplashScreenWidget ( QWidget* targetWidget )
{
    mutex.lock();
    target = targetWidget;
    mutex.unlock();
}

void QQtSplashScreenHelper::finish()
{
    timer->stop();
}

void QQtSplashScreenHelper::slotTimeout()
{
    QCoreApplication::processEvents();
}
