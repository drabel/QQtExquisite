#ifndef QQTBODYMOUSEWHEELHELPER_H
#define QQTBODYMOUSEWHEELHELPER_H

#include <QObject>
#include <QWidget>
#include <QWheelEvent>

#include <qqtexquisite_global.h>

class QQTEXQUISITESHARED_EXPORT QQtBodyMouseWheelHelper : public QObject
{
    Q_OBJECT

public:
    explicit QQtBodyMouseWheelHelper ( QObject* parent = 0 )
        : QObject ( parent ) {}
    virtual ~QQtBodyMouseWheelHelper() {}

protected:

private:

signals:
    //1.2, >0, scaling, UP
    //0.8, <0, scaling, DOWN
    void mouseWheel ( qreal = 1.0, Qt::Orientation = Qt::Vertical );

protected:
    virtual void mouseWheelEvent ( QWheelEvent* event, QWidget* target );
private:


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;

};

#endif // QQTBODYMOUSEWHEELHELPER_H

