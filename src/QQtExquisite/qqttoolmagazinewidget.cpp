﻿#include <qqttoolmagazinewidget.h>

#include <QStylePainter>
#include <qmath.h>

QQtToolMagazineWidget::QQtToolMagazineWidget ( QWidget* parent ) : QWidget ( parent )
{
    m_toolPairNum = 1;
    m_curRotate = 0;
}

QQtToolMagazineWidget::~QQtToolMagazineWidget() {}

void QQtToolMagazineWidget::setToolPairNum ( int num )
{
    m_toolPairNum = num;
}

int QQtToolMagazineWidget::toolPairNum()
{
    return m_toolPairNum;
}

void QQtToolMagazineWidget::setCurRatate ( int rotate )
{
    m_curRotate = rotate;
}

int QQtToolMagazineWidget::curRatate()
{
    return m_curRotate;
}

void QQtToolMagazineWidget::drawRing ( QPainter& painter, int ringWidth, int marin )
{
    painter.save();
    painter.setPen ( Qt::NoPen );
    painter.setBrush ( Qt::black );
    QPainterPath p;
    QRect rect ( -100 + marin, -100 + marin, 200 - marin * 2, 200 - marin * 2 );
    p.arcMoveTo ( rect, 0 );
    p.arcTo ( rect, 0, 360 );
    rect.adjust ( ringWidth, ringWidth, -ringWidth, -ringWidth );
    p.arcMoveTo ( rect, 0 );
    p.arcTo ( rect, 0, 360 );
    painter.drawPath ( p );
    painter.restore();
}

void QQtToolMagazineWidget::drawToolPair ( QPainter& painter, double radius, double bigRadius )
{
    painter.save();
    QFont font ( this->font() );
    font.setPixelSize ( radius * 2 - 2 );
    painter.setFont ( font );
    painter.setPen ( QColor ( "#4ECCA3" ) );

    double dAngle = ( double ) 360.0 / m_toolPairNum;
    for ( int i = 0; i < m_toolPairNum; ++i )
    {
        painter.save();
        double x = bigRadius * std::cos ( qDegreesToRadians ( -dAngle * i + m_curRotate ) );
        double y = bigRadius * std::sin ( qDegreesToRadians ( -dAngle * i + m_curRotate ) );

        painter.drawText ( QRectF ( x - radius, y - radius, radius * 2, radius * 2 ),
                           QString().sprintf ( "%02d", i + 1 ), QTextOption ( Qt::AlignCenter ) );
        painter.restore();
    }
    painter.restore();
}

void QQtToolMagazineWidget::drawTool ( QPainter& painter, double radius, double bigRadius )
{
    painter.save();
    QFont font ( this->font() );
    font.setPixelSize ( radius * 2 - 2 );
    painter.setFont ( font );
    painter.setPen ( QColor ( "#FFFFFF" ) );

    double dAngle = ( double ) 360.0 / m_toolPairNum;
    for ( int i = 0; i < m_toolPairNum; ++i )
    {
        painter.save();
        double x = bigRadius * std::cos ( qDegreesToRadians ( -dAngle * i + m_curRotate ) );
        double y = bigRadius * std::sin ( qDegreesToRadians ( -dAngle * i + m_curRotate ) );

        painter.drawText ( QRectF ( x - radius, y - radius, radius * 2, radius * 2 ),
                           QString().sprintf ( "%02d", i + 1 ), QTextOption ( Qt::AlignCenter ) );
        painter.restore();
    }
    painter.restore();
}

void QQtToolMagazineWidget::paintEvent ( QPaintEvent* event )
{
    Q_UNUSED ( event )
    int width = this->width();
    int height = this->height();
    int side = qMin ( width, height );
    QPainter painter ( this );
    painter.fillRect ( this->rect(), QColor ( 57, 62, 70 ) );
    painter.setRenderHint ( QPainter::Antialiasing );
    painter.scale ( side / 230.0, side / 230.0 );
    painter.translate ( 100, 100 );

    int marin = 2;
    int ringWidth = 40;
    drawRing ( painter, ringWidth, marin );

    double span = 2.0;
    double radius = ringWidth / 4.0 - span;
    double bigRadius = 100 - radius - marin - span;
    drawTool ( painter, radius, bigRadius );
    bigRadius -= ( radius * 2 + span );
    drawToolPair ( painter, radius, bigRadius );
}


