#include <qqt3dbodyhelper.h>
#include <QPainter>
#include <QWidget>
#include <QEvent>

QQt3DBodyHelper::QQt3DBodyHelper ( QObject* parent ) : QObject ( parent )
{
    degreeX = degreeY = degreeZ = 0;
    basePointX = basePointY = 0;
    basePointXRatio = basePointYRatio = 0;
}

QQt3DBodyHelper::~QQt3DBodyHelper()
{

}

void QQt3DBodyHelper::set_degree_of_axis_x ( qreal degree )
{
    degreeX = degree;
}

void QQt3DBodyHelper::set_degree_of_axis_y ( qreal degree )
{
    degreeY = degree;
}

void QQt3DBodyHelper::set_degree_of_axis_z ( qreal degree )
{
    degreeZ = degree;
}

qreal QQt3DBodyHelper::degree_of_axis_x()
{
    return degreeX;
}

qreal QQt3DBodyHelper::degree_of_axis_y()
{
    return degreeY;
}

qreal QQt3DBodyHelper::degree_of_axis_z()
{
    return degreeZ;
}

void QQt3DBodyHelper::set_original_point ( qreal x, qreal y )
{
    basePointX = x;
    basePointY = y;
}

void QQt3DBodyHelper::set_original_point_x ( qreal x )
{
    basePointX = x;
}

void QQt3DBodyHelper::set_original_point_y ( qreal y )
{
    basePointY = y;
}

void QQt3DBodyHelper::original_point ( qreal& x, qreal& y )
{
    x = basePointX;
    y = basePointY;
}

qreal QQt3DBodyHelper::original_point_x()
{
    return basePointX;
}

qreal QQt3DBodyHelper::original_point_y()
{
    return basePointY;
}

void QQt3DBodyHelper::set_original_point_ratio ( qreal xRatio, qreal yRatio )
{
    basePointXRatio = xRatio;
    basePointYRatio = yRatio;
}

void QQt3DBodyHelper::set_original_point_x_ratio ( qreal xRatio )
{
    basePointXRatio = xRatio;
}

void QQt3DBodyHelper::set_original_point_y_ratio ( qreal yRatio )
{
    basePointYRatio = yRatio;
}

void QQt3DBodyHelper::original_point_ratio ( qreal& xRatio, qreal& yRatio )
{
    xRatio = basePointXRatio;
    yRatio = basePointYRatio;
}

qreal QQt3DBodyHelper::original_point_x_ratio()
{
    return basePointXRatio;
}

qreal QQt3DBodyHelper::original_point_y_ratio()
{
    return basePointYRatio;
}

bool QQt3DBodyHelper::eventFilter ( QObject* watched, QEvent* event )
{
    Q_UNUSED ( event );

    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    QWidget* target = qobject_cast<QWidget*> ( watched );

    switch ( event->type() )
    {
        case QEvent::Paint:
        {
            QPainter painter ( target );

            // 反走样
            painter.setRenderHint ( QPainter::Antialiasing );
            painter.setRenderHint ( QPainter::TextAntialiasing );
            painter.setRenderHint ( QPainter::SmoothPixmapTransform );
            painter.setRenderHint ( QPainter::HighQualityAntialiasing );

            QRect r0 = target->rect();

            qreal basePointXResult, basePointYResult;
            basePointXResult = 0;
            basePointYResult = 0;
            if ( original_point_x() != 0 )
                basePointXResult = original_point_x();
            if ( original_point_y() != 0 )
                basePointYResult = original_point_y();

            if ( original_point_x_ratio() != 0 )
                basePointXResult = r0.width() * original_point_x_ratio();
            if ( original_point_y_ratio() != 0 )
                basePointYResult = r0.height() * original_point_y_ratio();

            QTransform transform;
            // 平移
            transform.translate ( basePointXResult, basePointYResult );
            // 旋转
            transform.rotate ( degree_of_axis_x(), Qt::XAxis );
            transform.rotate ( degree_of_axis_y(), Qt::YAxis );
            transform.rotate ( degree_of_axis_z(), Qt::ZAxis );
            transform.translate ( -basePointXResult, -basePointYResult );
            painter.setTransform ( transform );

            //paint jobs

            return false;
        }
        break;
        default:
            break;
    }

    return QObject::eventFilter ( watched, event );
}
