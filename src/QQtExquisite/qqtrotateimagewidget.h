﻿#ifndef QQTROTATEIMAGEWIDGET_H
#define QQTROTATEIMAGEWIDGET_H

#include "qqtexquisite_global.h"

#include <qqtwidget.h>

/**
 * @brief The QQtRotateImageWidget class
 * 可以图片旋转控件
 * 图片style：CENTER。
 */
class QQTEXQUISITESHARED_EXPORT QQtRotateImageWidget: public QQtWidget
{
    Q_OBJECT
public:
    explicit QQtRotateImageWidget ( QWidget* parent = 0 );
    virtual ~QQtRotateImageWidget();

    //默认为0 deg。
    void setRotate ( qreal rotate );
    qreal rotate();

    //默认为0,0位置 0,0 定位
    void setRotatePos ( qreal dx, qreal dy );
    QPointF rotatePos();

    //x y的圆心位置 比例 0,0 变位 最终有效
    void setRotatePosRatio ( qreal dxR, qreal dyR );
    QPointF rotatePosRatio();

    // QWidget interface
protected:
    virtual void paintEvent ( QPaintEvent* event ) override;

private:
    qreal mRotate;

    qreal dx, dy;
    qreal dxR, dyR;
};

#endif // QQTROTATEIMAGEWIDGET_H
