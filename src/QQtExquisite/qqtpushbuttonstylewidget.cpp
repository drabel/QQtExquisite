#include <qqtpushbuttonstylewidget.h>


QQtPushButtonStyleWidget::QQtPushButtonStyleWidget ( QWidget* parent )
    : QQtWidget ( parent )
{
    mWorkState = BTN_NORMAL;
}

QQtPushButtonStyleWidget::~QQtPushButtonStyleWidget()
{

}

void QQtPushButtonStyleWidget::translateImage()
{
    int state = mWorkState;

    if ( !isEnabled() )
        state = BTN_DISABLE;

    setImage ( mImageCache[state] );
}

int QQtPushButtonStyleWidget::workState() const
{
    return mWorkState;
}

void QQtPushButtonStyleWidget::setWorkState ( int index )
{
    mWorkState = ( EBtnStatus ) index;
    translateImage();
}


QImage QQtPushButtonStyleWidget::stateImage ( int index )
{
    if ( index < BTN_NORMAL || index > BTN_MAX - 1 )
        return mImageCache[BTN_NORMAL];
    return mImageCache[index];
}

void QQtPushButtonStyleWidget::setStateImage ( int index, const QImage& image )
{
    if ( index < BTN_NORMAL || index > BTN_MAX - 1 )
        return;
    mImageCache[index] = image;
    translateImage();
}

void QQtPushButtonStyleWidget::setNormalImage ( const QImage& normal, const QImage& press )
{
    mImageCache[BTN_NORMAL] = normal;
    mImageCache[BTN_PRESS] = press;
    translateImage();
}

void QQtPushButtonStyleWidget::setHoverImage ( const QImage& hover )
{
    mImageCache[BTN_HOVER] = hover;
    translateImage();
}

void QQtPushButtonStyleWidget::setDisableImage ( const QImage& disable )
{
    mImageCache[BTN_DISABLE] = disable;
    translateImage();
}

void QQtPushButtonStyleWidget::setEnabled ( bool stat )
{
    QQtWidget::setEnabled ( stat );
    if ( stat )
        setWorkState ( BTN_NORMAL );
    else
        setWorkState ( BTN_DISABLE );
}

void QQtPushButtonStyleWidget::setDisabled ( bool stat )
{
    QQtWidget::setDisabled ( stat );
    if ( !stat )
        setWorkState ( BTN_NORMAL );
    else
        setWorkState ( BTN_DISABLE );
}

void QQtPushButtonStyleWidget::mousePressEvent ( QMouseEvent* event )
{
    if ( event->button() == Qt::LeftButton )
        mWorkState = BTN_PRESS;
    QQtWidget::mousePressEvent ( event );
    translateImage();
}

void QQtPushButtonStyleWidget::mouseReleaseEvent ( QMouseEvent* event )
{
    if ( event->button() == Qt::LeftButton )
    {
#ifdef __EMBEDDED_LINUX__
        mWorkState = BTN_NORMAL;
#else
        if ( rect().contains ( event->pos() ) )
            mWorkState = BTN_HOVER;
        else
            mWorkState = BTN_NORMAL;
#endif
    }
    QQtWidget::mouseReleaseEvent ( event );
    translateImage();
}

void QQtPushButtonStyleWidget::enterEvent ( QEvent* event )
{
    mWorkState = BTN_HOVER;
    QQtWidget::enterEvent ( event );
    translateImage();
}

void QQtPushButtonStyleWidget::leaveEvent ( QEvent* event )
{
    mWorkState = BTN_NORMAL;
    QQtWidget::leaveEvent ( event );
    translateImage();
}
