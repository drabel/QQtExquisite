﻿#ifndef QQTTOOLMAGAZINEWIDGET_H
#define QQTTOOLMAGAZINEWIDGET_H

#include <QWidget>
#include <qqtexquisite_global.h>

/**
 * @brief The QQtToolMagazineWidget class
 * 圆形的刀套、刀盘选择控件
 *
 * 页面未完成。
 */
class QQTEXQUISITESHARED_EXPORT QQtToolMagazineWidget : public QWidget
{
    Q_OBJECT
public:
    explicit QQtToolMagazineWidget ( QWidget* parent = 0 );
    virtual ~QQtToolMagazineWidget();

    void setToolPairNum ( int num );
    int toolPairNum();

    void setCurRatate ( int rotate );

    int curRatate();

protected:
    void drawRing ( QPainter& painter, int ringWidth, int marin );
    void drawTool ( QPainter& painter, double radius, double bigRadius );
    void drawToolPair ( QPainter& painter, double radius, double bigRadius );

    // QWidget interface
protected:
    virtual void paintEvent ( QPaintEvent* event ) override;

private:
    int m_toolPairNum;
    int m_curRotate;
};

#endif // QQTTOOLMAGAZINEWIDGET_H
