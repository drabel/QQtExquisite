#include <qqtcirclewaitinghelper.h>


#include <QEvent>
#include <QWidget>
#include <qqtcore.h>
#include <qqtwidgets.h>

#include <QtMath>


QQtCircleWaitingHelper::QQtCircleWaitingHelper ( QObject* parent )
    : QObject ( parent ) {}

QQtCircleWaitingHelper::~QQtCircleWaitingHelper() {}

#if 0
int QQtCircleWaitingHelper::round ( double value )
{
    return ( int ) ( value + 0.5 );
}

double QQtCircleWaitingHelper::cos ( double degree )
{
    int index = round ( degree / 0.25 );
    return cosine[index];
}

double QQtCircleWaitingHelper::sin ( double degree )
{
    int index = round ( degree / 0.25 );
    return sine[index];
}

double QQtCircleWaitingHelper::CovertRadian ( double degree )
{
    return Math.PI * degree / 180;
}

public Win10Process()
{
    InitializeComponent();
    initialize_trigonometry_tables();
    adjust_control_dimensions_from_height ( Control_Height );
    initialize_indicators();

    time = new System.Timers.Timer();
    this.time.Elapsed += new ElapsedEventHandler ( tick );
    time.Interval = 100.0;
    time.Enabled = true;
}


private void Win10Process_Paint ( object sender, PaintEventArgs e )
{
    Graphics graphics = e.Graphics;
    draw_background_graphic ( graphics );
}


void initialize_trigonometry_tables()
{
    for ( int i = 0; i < sine.Length; i++ )
    {
        double degree = i * 0.25;
        double radian = CovertRadian ( degree );

        cosine[i] = Math.Cos ( radian );
        sine[i] = Math.Sin ( radian );
    }
}

void adjust_control_dimensions_from_height ( int height )
{
    indicator_radius = height / Control_Offset;//半径
    indicator_diameter = indicator_radius * 2; //直径


    int control_height = indicator_radius * Control_Offset; //经过修改过之后的高度
    int control_width  = control_height;    //经过修改之后的宽度

    outer_radius = control_height / 2;
    inner_radius = outer_radius - indicator_diameter;

    indicator_center_radius = outer_radius - indicator_radius;


    this.Height = control_height;
    this.Width = control_width;
}

//初始化指示器
void initialize_indicators()
{
    double degree = Start_At;
    for ( int i = 0; i < Indicator_Size; i++ )
    {
        indicators[i] = new Indicator ( degree );
        if ( degree < 0 )
        {
            degree += 360;
        }
        degree -= Indicators_Offet;
    }
}


void draw_background_graphic ( Graphics graphics )
{

    Rectangle rect = new Rectangle();
    Brush brush = new SolidBrush ( Color.Red );

    for ( int i = 0; i < Indicator_Size; i++ )
    {
        double degree = indicators[i].Degree;
        if ( degree < 0 )
        {

        }

        int dx = indicator_center_radius + round ( indicator_center_radius * cos ( degree ) );
        int dy = indicator_center_radius - round ( indicator_center_radius * sin ( degree ) );

        rect.Size = new Size ( indicator_diameter, indicator_diameter );
        rect.Location = new Point ( dx, dy );
        // graphics.DrawLine(Pens.Red, 0,100,dx,dy);

        graphics.FillEllipse ( brush, rect );
        degree -= Indicators_Offet * indicators[i].Fast;


        if ( indicators[i].Fast > 1.0 )
        {
            indicators[i].Fast += 0.25;
        }
        if ( degree < 0.0 )
        {
            degree += 360;
            indicators[i].Fast = 1.25;
        }
        else if ( degree < Start_At )
        {
            indicators[i].Fast = 1.0;

        }
        indicators[i].Degree = degree;
    }
    brush.Dispose();

}
#endif

bool QQtCircleWaitingHelper::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    //bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    //if ( atti )
    //    return QObject::eventFilter ( watched, event );

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    QWidget* target = qobject_cast<QWidget*> ( watched );
    if ( event->type() == QEvent::Paint )
    {
        return false;
    }

    return QObject::eventFilter ( watched, event );
}
