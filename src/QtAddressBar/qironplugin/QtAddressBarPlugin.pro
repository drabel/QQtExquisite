TEMPLATE = lib
TARGET = QtAddressBarPlugin
CONFIG += designer plugin

include (../../../multi-link/add_base_manager.pri)
DESTDIR = ../plugins

add_dynamic_library_project()
add_version(1,0,0,0)

build_pass:CONFIG(debug, debug|release) {
        #TARGET = $$join(TARGET,,,d)
}
macx {
	CONFIG += absolute_library_soname
}
include(qironplugin.pri)

RESOURCES += qironplugin.qrc
