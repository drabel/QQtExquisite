# -----------------------------------------------------------
# This file is generated by the Qt Visual Studio Integration.
# -----------------------------------------------------------
INCLUDEPATH += $$PWD $$PWD/ui


#Header files
HEADERS += $${PWD}/qirexpander.h \
    $${PWD}/ui/qirexpander_ui.h \
    $${PWD}/qirexpanderbox.h \
    $${PWD}/qirexpanderstyle.h

#Source files
SOURCES += $${PWD}/qirexpander.cpp \
    $${PWD}/qirexpanderbox.cpp \
    $${PWD}/qirexpanderstyle.cpp
