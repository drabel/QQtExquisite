INCLUDEPATH += $$PWD $$PWD/ui

HEADERS += \
        $${PWD}/qirstyleoptionbreadcrumbbar.h \
        $${PWD}/qirdir_helper.h \
        $${PWD}/ui/qirbreadcrumbbar_ui.h \
        $${PWD}/qirabstractbreadcrumbmodel.h \
        $${PWD}/qirbreadcrumbbar.h \
        $${PWD}/qirbreadcrumbdirmodel.h \
        $${PWD}/qirbreadcrumbbarstyle.h


SOURCES += \
        $${PWD}/qirstyleoptionbreadcrumbbar.cpp \
        $${PWD}/qirdir_helper.cpp \
        $${PWD}/qirabstractbreadscrumbmodel.cpp \
        $${PWD}/qirbreadcrumbdirmodel.cpp \
        $${PWD}/qirbreadcrumbbar.cpp \
        $${PWD}/qirbreadcrumbbarstyle.cpp
