INCLUDEPATH += $$PWD $$PWD/ui

HEADERS += \
        $${PWD}/ui/qirquadsplitter_ui.h \
        $${PWD}/qirquadsplitter.h \
        $${PWD}/qirquadsplitterlayout.h \

SOURCES += \
        $${PWD}/qirquadsplitter.cpp \
        $${PWD}/qirquadsplitterlayout.cpp
