TEMPLATE = lib
TARGET = QtAddressBar
CONFIG += debug_and_release build_all

include (../../../multi-link/add_base_manager.pri)
DESTDIR = ../lib

add_dynamic_library_project()

contains(DEFINES, LIB_LIBRARY):DEFINES += QIRON_LIBRARY_EXPORT
else:contains(DEFINES, LIB_STATIC_LIBRARY):DEFINES +=
else:DEFINES += QIRON_LIBRARY_IMPORT
add_version(1,0,0,0)

build_pass:CONFIG(debug, debug|release) {
        #TARGET = $$join(TARGET,,,d)
}
macx {
	CONFIG += absolute_library_soname
}

system(touch QIrQuadSplitter/qirquadsplitter.cpp)
include(Common/Common.pri)
include(QIrQuadSplitter/QIrQuadSplitter.pri)
include(QIrBreadCrumbBar/QIrBreadCrumbBar.pri)
include(QIrExpander/QIrExpander.pri)
include(QIrDock/QIrDock.pri)

message($$TARGET defines $$DEFINES)
