INCLUDEPATH += $$PWD $$PWD/ui $$PWD/private

HEADERS += \
        $${PWD}/private/qirobject_p.h \
        $${PWD}/ui/qirwidget_ui.h \
        $${PWD}/qiron_export.h \
        $${PWD}/qirmacros.h \
        $${PWD}/qirwidget.h \
        $${PWD}/qirboolblocker.h \
        $${PWD}/qirsubstyle.h \
        $${PWD}/qirsmartpointer.h

win32 {
    HEADERS += $${PWD}/qirwindows.h
}

SOURCES += \
        $${PWD}/qirwidget.cpp \
        $${PWD}/qirobject.cpp \
        $${PWD}/qirsubstyle.cpp \
        $${PWD}/qirboolblocker.cpp \
