#ifndef LOG4QT_SHARED_H
#define LOG4QT_SHARED_H

#include <QtGlobal>

// Define LOG4QT_STATIC in you applikation if you want to link against the
// static version of Log4Qt

#ifdef LOG4QT_STATIC
#   define LOG4QT_EXPORT
#else
#  if defined(LOG4QT_LIBRARY)
#    define LOG4QT_EXPORT Q_DECL_EXPORT
#  else
#    define LOG4QT_EXPORT Q_DECL_IMPORT
#  endif
#endif

#if QT_VERSION < QT_VERSION_CHECK(5,7,0)
// this adds const to non-const objects (like std::as_const)
template <typename T>
Q_DECL_CONSTEXPR typename std::add_const<T>::type& qAsConst ( T& t ) Q_DECL_NOTHROW { return t; }
// prevent rvalue arguments:
template <typename T>
void qAsConst ( const T&& ) Q_DECL_EQ_DELETE;
#endif

#endif // LOG4QT_SHARED_H
