TEMPLATE = subdirs
CONFIG += ordered
QMAKE_PROJECT_NAME=Log4Qt

SUBDIRS +=  src \
            #tests \
            #examples

OTHER_FILES += LICENSE \
               Readme.md \
               .travis.yml \
               appveyor.yml \
               ChangeLog.md
