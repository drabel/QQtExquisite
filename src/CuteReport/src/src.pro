TEMPLATE = subdirs
SUBDIRS = core

win32|macx|linux:!cross_compiled {
    SUBDIRS += \
            thirdparty \
            plugins \
            apps \
            widgets

    widgets.depends = core
    apps.depends = core thirdparty
    plugins.depends = core widgets apps thirdparty
}
