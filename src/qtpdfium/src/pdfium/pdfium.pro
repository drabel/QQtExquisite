TARGET = QtPdfium
TEMPLATE = lib

include (../../../../multi-link/add_base_manager.pri)
contains(DEFINES, LIB_LIBRARY):DEFINES += QT_BUILD_PDFIUM_LIB
else:contains(DEFINES, LIB_STATIC_LIBRARY):DEFINES += QT_BUILD_STATIC_PDFIUM_LIB

win32:LIBS+= -lgdi32 -luuid
#ios使用mac那一套不行，所以使用Qt这一套。
ios:DEFINES += __QT__

DEFINES += \
    OPJ_STATIC \
    PNG_PREFIX \
    PNG_USE_READ_MACROS

# This is to prevent an undefined reference of qt_version_tag
# when on Linux, x86 architecture and the GNU tools.
DEFINES += QT_NO_VERSION_TAGGING

QT = core-private core gui
CONFIG += warn_on strict_flags
#load(qt_module)

#QMAKE_DOCS = $$PWD/doc/qtpdfium.qdocconf
DEFINES += FPDFSDK_EXPORTS
include($$PWD/../3rdparty/pdfium.pri)


PRIVATE_HEADERS += \
    $$PWD/qpdfiumglobal.h

PUBLIC_HEADERS += \
    $$PWD/qpdfium.h \
    $$PWD/qpdfiumpage.h
SOURCES += \
    $$PWD/qpdfiumglobal.cpp \
    $$PWD/qpdfium.cpp \
    $$PWD/qpdfiumpage.cpp

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS

system(touch $$PWD/qpdfium.cpp)
add_sdk()
message($$TARGET config $$CONFIG)
message($$TARGET defines $$DEFINES)
QMAKE_CXXFLAGS += -std=c++0x

#msvc的莫名其妙的错误，这个参数与/source-charset:utf-8冲突
#msvc编译这个库令人想哭。
message (QMAKE_CFLAGS $$QMAKE_CFLAGS)
message (QMAKE_CFLAGS_DEBUG $$QMAKE_CFLAGS_DEBUG)
message (QMAKE_CFLAGS_RELEASE $$QMAKE_CFLAGS_RELEASE)

message (QMAKE_CXXFLAGS $$QMAKE_CXXFLAGS)
message (QMAKE_CXXFLAGS_DEBUG $$QMAKE_CXXFLAGS_DEBUG)
message (QMAKE_CXXFLAGS_RELEASE $$QMAKE_CXXFLAGS_RELEASE)

CC0FLAGS = /source-charset:utf-8 #/utf-8 -utf-8
CCFLAGS = /MP

msvc:QMAKE_CFLAGS -= $${CC0FLAGS}
msvc:QMAKE_CXXFLAGS -= $${CC0FLAGS}
msvc:QMAKE_CFLAGS += $${CCFLAGS}
msvc:QMAKE_CXXFLAGS += $${CCFLAGS}

message (QMAKE_CFLAGS $$QMAKE_CFLAGS)
message (QMAKE_CFLAGS_DEBUG $$QMAKE_CFLAGS_DEBUG)
message (QMAKE_CFLAGS_RELEASE $$QMAKE_CFLAGS_RELEASE)

message (QMAKE_CXXFLAGS $$QMAKE_CXXFLAGS)
message (QMAKE_CXXFLAGS_DEBUG $$QMAKE_CXXFLAGS_DEBUG)
message (QMAKE_CXXFLAGS_RELEASE $$QMAKE_CXXFLAGS_RELEASE)
