TARGET = QtXlsx
#QMAKE_DOCS = $$PWD/doc/qtxlsx.qdocconf

#load(qt_module)

TEMPLATE = lib
include(../../../../multi-link/add_base_manager.pri)
contains(DEFINES, LIB_LIBRARY):DEFINES += QT_BUILD_XLSX_LIB
else:contains(DEFINES, LIB_STATIC_LIBRARY):DEFINES += XLSX_NO_LIB
system(touch $${PWD}/xlsxdocument.cpp)
add_version(0,3,0,0)
add_sdk(QtXlsx, $$add_target_name())
CONFIG += build_all

CONFIG += build_xlsx_lib
include(qtxlsx.pri)

msvc:QMAKE_CFLAGS -= /source-charset:utf-8
msvc:QMAKE_CXXFLAGS -= /source-charset:utf-8


#Define this macro if you want to run tests, so more AIPs will get exported.
#DEFINES += XLSX_TEST

QMAKE_TARGET_COMPANY = "Debao Zhang"
QMAKE_TARGET_COPYRIGHT = "Copyright (C) 2013-2014 Debao Zhang <hello@debao.me>"
QMAKE_TARGET_DESCRIPTION = ".Xlsx file wirter for Qt5"
message($$QMAKE_CFLAGS)
message (.... $${QMAKE_CXXFLAGS})
