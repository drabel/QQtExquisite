#include "testdialog.h"
#include "ui_testdialog.h"

TestDialog::TestDialog ( QWidget* parent ) :
    QDialog ( parent ),
    ui ( new Ui::TestDialog )
{
    ui->setupUi ( this );
    setWindowFlag ( Qt::WindowStaysOnTopHint );
}

TestDialog::~TestDialog()
{
    delete ui;
}

void TestDialog::on_pushButton_clicked()
{
    close();
}
