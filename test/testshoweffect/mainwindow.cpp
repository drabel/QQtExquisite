#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtdialog.h>
#include <qqtshowmaskhelper.h>
#include <testdialog.h>
#include <qqtcore.h>
#include <qqtframe.h>
#include <qqtchildbodymover.h>
#include <qqtchildbodykeepcenterhelper.h>

static TestDialog* localDialog = 0;
static QQtShowMaskHelper* localMask = 0;
MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    localDialog = new TestDialog ( this );
    localMask = new QQtShowMaskHelper ( this );
    localMask->setBackgroundColor ( QColor ( 255, 0, 0, 60 ) );
    localMask->setOpacity ( 0.5 );
    localMask->maskWidget()->setPixmap ( conf_root ( "a2.png" ) );
    localDialog->installEventFilter ( localMask );
    localDialog->hide();
    localDialog->installEventFilter ( new QQtChildBodyKeepCenterHelper ( this ) );

    QQtChildBodyMover* mov = new QQtChildBodyMover ( this );

    QQtWidget* testWidget = new QQtWidget ( centralWidget() );
    testWidget->setPixmap ( conf_root ( "logo.png" ) );
    //testWidget->show();
    testWidget->installEventFilter ( mov );

    QQtWidget* testWidget2 = new QQtWidget ( );
    testWidget2->setParent ( this );
    testWidget2->setPixmap ( conf_root ( "logo.png" ) );
    testWidget2->show();
    testWidget2->installEventFilter ( mov );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    if ( localDialog->isHidden() )
        localDialog->show();
    else
        localDialog->hide();
}
