#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtframe.h>

#include <qqtbodymover.h>
#include <qqtchildbodymover.h>

#include <qqtbodystayonsidehelper.h>
#include <qqtbodyautohidehelper.h>

#include <qqtchildbodystayonsidehelper.h>
#include <qqtchildbodyautohidehelper.h>

#include <qqtbodyselectedstyle.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->setPixmap ( conf_root ( "logo.png" ) );

    //ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    //ui->widget->

#if 1

    //安装在子控件上，主窗口移动、贴靠
    ui->widget->installEventFilter ( new QQtBodyMover ( this ) );
    installEventFilter ( new QQtBodyStayOnSideHelper ( this ) );
    QQtBodyAutoHideHelper* h1 = new QQtBodyAutoHideHelper ( this );
    installEventFilter ( h1 );
#if 1
    h1->setCornerStyle ( QQtBodyAutoHideHelper::LeftTop, QQtBodyAutoHideHelper::CornerStyle_Default );
    h1->setCornerStyle ( QQtBodyAutoHideHelper::RightTop, QQtBodyAutoHideHelper::CornerStyle_Default );
    h1->setCornerStyle ( QQtBodyAutoHideHelper::LeftBottom, QQtBodyAutoHideHelper::CornerStyle_Default );
    h1->setCornerStyle ( QQtBodyAutoHideHelper::RightBottom, QQtBodyAutoHideHelper::CornerStyle_Default );
#elif 0
    h1->setCornerStyle ( QQtBodyAutoHideHelper::LeftTop, QQtBodyAutoHideHelper::CornerStyle_A );
    h1->setCornerStyle ( QQtBodyAutoHideHelper::RightTop, QQtBodyAutoHideHelper::CornerStyle_A );
    h1->setCornerStyle ( QQtBodyAutoHideHelper::LeftBottom, QQtBodyAutoHideHelper::CornerStyle_A );
    h1->setCornerStyle ( QQtBodyAutoHideHelper::RightBottom, QQtBodyAutoHideHelper::CornerStyle_A );
#else
    h1->setCornerStyle ( QQtBodyAutoHideHelper::LeftTop, QQtBodyAutoHideHelper::CornerStyle_B );
    h1->setCornerStyle ( QQtBodyAutoHideHelper::RightTop, QQtBodyAutoHideHelper::CornerStyle_B );
    h1->setCornerStyle ( QQtBodyAutoHideHelper::LeftBottom, QQtBodyAutoHideHelper::CornerStyle_B );
    h1->setCornerStyle ( QQtBodyAutoHideHelper::RightBottom, QQtBodyAutoHideHelper::CornerStyle_B );
#endif

#elif 0

    //安装在子控件上，子窗口移动，主窗口贴靠
    ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget->installEventFilter ( new QQtBodyStayOnSideHelper ( this ) );

#elif 1

    //安装在子控件上，子窗口移动，子窗口贴靠
    ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    //ui->widget->installEventFilter ( new QQtChildBodyStayOnSideHelper ( this ) );
    QQtChildBodyAutoHideHelper* h1 = new QQtChildBodyAutoHideHelper ( this );
    ui->widget->installEventFilter ( h1 );
#if 0
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::LeftTop, QQtChildBodyAutoHideHelper::CornerStyle_Default );
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::RightTop, QQtChildBodyAutoHideHelper::CornerStyle_Default );
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::LeftBottom, QQtChildBodyAutoHideHelper::CornerStyle_Default );
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::RightBottom, QQtChildBodyAutoHideHelper::CornerStyle_Default );
#elif 1
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::LeftTop, QQtChildBodyAutoHideHelper::CornerStyle_A );
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::RightTop, QQtChildBodyAutoHideHelper::CornerStyle_A );
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::LeftBottom, QQtChildBodyAutoHideHelper::CornerStyle_A );
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::RightBottom, QQtChildBodyAutoHideHelper::CornerStyle_A );
#else
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::LeftTop, QQtChildBodyAutoHideHelper::CornerStyle_B );
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::RightTop, QQtChildBodyAutoHideHelper::CornerStyle_B );
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::LeftBottom, QQtChildBodyAutoHideHelper::CornerStyle_B );
    h1->setCornerStyle ( QQtChildBodyAutoHideHelper::RightBottom, QQtChildBodyAutoHideHelper::CornerStyle_B );
#endif
    qDebug() << ui->widget->parent();

#endif

    ui->widget->installEventFilter ( new QQtBodySelectedStyle ( this ) );
    ui->widget->setFocusPolicy ( Qt::StrongFocus );

    installEventFilter ( this );
}

MainWindow::~MainWindow()
{
    delete ui;
}


bool MainWindow::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。鼠标穿透没有效果
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::Paint )
        return QObject::eventFilter ( watched, event );

    if ( event->type() == QEvent::MouseMove )
    {
        //p2line() << QCursor::pos();
        return QObject::eventFilter ( watched, event );
    }

    //static int i = 0;
    //p3line() << i++ << watched->objectName() << event->type() ;

    switch ( event->type() )
    {
        case QEvent::Enter:
        {
            pline() << "enter";
            return false;
        }
        //过滤掉标题栏
        case QEvent::Leave:
        {
            QEvent* e = ( QEvent* ) event;
            QWidget* target = qobject_cast<QWidget*> ( watched );
            QWidget& w = *this;
            QRect frameR0 = frameGeometry();
            QPoint p0, p1;
            p0 = frameR0.topLeft();
            p1 = frameR0.bottomRight();
            //p0 = w.mapToGlobal ( p0 );
            //p1 = w.mapToGlobal ( p1 );

            QPoint mousePos = QCursor::pos();

            qreal ratio = 1; //w.devicePixelRatioF();
            QRect r0 = QRect ( p0, p1 );
            QRect qr0 = QRect ( QPoint ( r0.left() * ratio, r0.top() * ratio ),
                                QPoint ( r0.right() * ratio, r0.bottom() * ratio ) );

            QRect r1 = rect();
            QRect qr1 = QRect ( w.mapToGlobal ( r1.topLeft() ), w.mapToGlobal ( r1.bottomRight() ) );
            r1 = geometry();

            //titlebar  y=20;
            //OK 准确了。
            QRect r00 = QRect ( qr1.left(), qr1.top() - 20, qr1.width(), 20 + 5 );
            pline() << r00;
            pline() << qr0 << qr1 << mousePos;

            pline() << "leave";
            //在多个屏幕的时候，这个不准确。
            //其实，鼠标的坐标是准确的，frameGeometry的不准确，rect()的是准确的。
            if ( r00.contains ( mousePos ) )
            {
                //过滤掉标题栏
                event->ignore();
                pline() << "leave, but ignore";
                return true;
            }
            else
                pline() << "leave, accept";
            event->accept();

            return false;
        }
        default:
            break;
    }

    return QMainWindow::eventFilter ( watched, event );
}
