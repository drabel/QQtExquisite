#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtchildbodymover.h>
#include <qqtbodyresizer.h>
#include <qqtbodymousewheelscalingeffect.h>

#include <qqtselectstylewidget.h>
#include <qqtframe.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->setPixmap ( conf_root ( "logo.png" ) );
    ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget->setMouseTracking ( true );
    ui->widget->setContentsMargins ( 5, 5, 5, 5 );
    ui->widget->setFocusPolicy ( Qt::StrongFocus );
    centralWidget()->setFocusPolicy ( Qt::StrongFocus );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_radioButton_toggled ( bool checked )
{
    ui->widget->setSelectedStyle ( QQtSelectStyleWidget::SelectedStyle_QtDesigner );
}

void MainWindow::on_radioButton_2_toggled ( bool checked )
{
    ui->widget->setSelectedStyle ( QQtSelectStyleWidget::SelectedStyle_QRCodeScaner );
}

void MainWindow::on_radioButton_3_toggled ( bool checked )
{
    ui->widget->setSelectedStyle ( QQtSelectStyleWidget::SelectedStyle_DottedLine );
}

void MainWindow::on_radioButton_4_toggled ( bool checked )
{
    ui->widget->setSelectedStyle ( QQtSelectStyleWidget::SelectedStyle_FourCheck );
}
