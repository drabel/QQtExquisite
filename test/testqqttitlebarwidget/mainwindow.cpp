#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->pushButton->setCheckable ( true );
    ui->pushButton_2->setCheckable ( true );
    ui->pushButton_3->setCheckable ( true );
    ui->pushButton_4->setCheckable ( true );
    ui->pushButton_5->setCheckable ( true );
    ui->pushButton_6->setCheckable ( true );
    ui->pushButton_7->setCheckable ( true );
    ui->pushButton_8->setCheckable ( true );

    //this->showFullScreen();
    connect ( ui->widget_2, SIGNAL ( menuButtonToggled ( bool ) ), this, SLOT ( menuButtonToggled ( bool ) ) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::menuButtonToggled ( bool checked )
{
    qDebug() << checked;
}

void MainWindow::on_pushButton_clicked ( bool checked )
{
    ui->widget->setMinButtonVisable ( checked );
}

void MainWindow::on_pushButton_2_clicked ( bool checked )
{
    ui->widget->setMaxButtonVisable ( checked );
}

void MainWindow::on_pushButton_3_clicked ( bool checked )
{
    ui->widget->setCloseButtonVisable ( checked );
}

void MainWindow::on_pushButton_4_clicked ( bool checked )
{
    ui->widget_2->setCloseButtonVisable ( checked );
}

void MainWindow::on_pushButton_5_clicked ( bool checked )
{
    ui->widget_2->setMaxButtonVisable ( checked );
}

void MainWindow::on_pushButton_6_clicked ( bool checked )
{
    ui->widget_2->setMinButtonVisable ( checked );
}

void MainWindow::on_pushButton_7_clicked ( bool checked )
{
    ui->widget_2->setFullButtonVisable ( checked );
}

void MainWindow::on_pushButton_8_clicked ( bool checked )
{
    ui->widget_2->setMenuButtonVisable ( checked );
}
