﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qqtframe.h>

#include <qqtchildbodymover.h>
#include <qqtbodyresizer.h>
#include <qqtbodymousewheelscalingeffect.h>
#include <qqtbodyselectedstyle.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->setPixmap ( conf_root ( "logo.png" ) );

    ui->horizontalSlider->setRange ( 0, 360 );
    ui->horizontalSlider->setValue ( 0 );

    //ui->widget->setRotatePos ( 100, 100 );
    ui->widget->setRotatePosRatio ( 0.5, 0.5 );

    ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget->setMouseTracking ( true );
    ui->widget->setContentsMargins ( 5, 5, 5, 5 );

    ui->widget->installEventFilter ( new QQtBodySelectedStyle ( this ) );
    ui->widget->setFocusPolicy ( Qt::StrongFocus );
    //centralWidget()->setFocusPolicy ( Qt::StrongFocus );

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_horizontalSlider_valueChanged ( int value )
{
    ui->widget->setRotate ( value );
    ui->widget->update();;
}


void MainWindow::paintEvent ( QPaintEvent* event )
{
    //QPainter painter ( this );
    //painter.rotate ( 60 );

    return QMainWindow::paintEvent ( event );
}
