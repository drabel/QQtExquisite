﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtframe.h>
#include <qqtcore.h>

#include <qqtdragtopnghelper.h>
#include <qqtbodyclipboardhelper.h>
#include <qqtchildbodymover.h>
#include <qqtbodyresizer.h>
#include <qqtbodyselectedstyle.h>
#include <qqtbodymousewheelscalingeffect.h>
#include <qqt3dwidget.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->setPixmap ( conf_root ( "logo.png" ) );
    ui->widget->setAcceptDrops ( true );
    ui->widget->installEventFilter ( new QQtDragToPngHelper ( this ) );
    ui->widget->installEventFilter ( new QQtBodyClipBoardHelper ( this ) );
    //ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget->setMouseTracking ( true );
    ui->widget->setContentsMargins ( 5, 5, 5, 5 );
    ui->widget->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget->setFocusPolicy ( Qt::StrongFocus );
    ui->widget->installEventFilter ( new QQtBodySelectedStyle ( this ) );


    ui->horizontalSlider->setRange ( 0, 100 );
    ui->horizontalSlider_2->setRange ( 0, 100 );
    ui->horizontalSlider_3->setRange ( 0, 100 );
    ui->horizontalSlider_4->setRange ( 0, 100 );
    ui->horizontalSlider_5->setRange ( 0, 100 );

    ui->horizontalSlider->setValue ( 0 );
    ui->horizontalSlider_2->setValue ( 0 );
    ui->horizontalSlider_3->setValue ( 0 );
    ui->horizontalSlider_4->setValue ( 0 );
    ui->horizontalSlider_5->setValue ( 0 );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_horizontalSlider_valueChanged ( int value )
{
    //basePoint X
    qreal x_r = ( qreal ) value / 100;
    ui->widget->set_original_point_x_ratio ( x_r );
}

void MainWindow::on_horizontalSlider_2_valueChanged ( int value )
{
    //basePoint Y
    qreal y_r = ( qreal ) value / 100;
    ui->widget->set_original_point_y_ratio ( y_r );
}

void MainWindow::on_horizontalSlider_3_valueChanged ( int value )
{
    //x
    qreal x_d = ( qreal ) value / 100 * 360;
    ui->widget->set_degree_of_axis_x ( x_d );
}

void MainWindow::on_horizontalSlider_4_valueChanged ( int value )
{
    //y
    qreal y_d = ( qreal ) value / 100 * 360;
    ui->widget->set_degree_of_axis_y ( y_d );
}

void MainWindow::on_horizontalSlider_5_valueChanged ( int value )
{
    //z
    qreal z_d = ( qreal ) value / 100 * 360;
    ui->widget->set_degree_of_axis_z ( z_d );
}
