#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtchildbodyautohidehelper.h>
#include <qqtchildbodystayonsidehelper.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->installEventFilter ( new QQtChildBodyStayOnSideHelper ( this ) );
    QQtChildBodyAutoHideHelper* helper = new QQtChildBodyAutoHideHelper ( this ) ;
    helper->setCornerStyle ( QQtChildBodyAutoHideHelper::LeftBottom, QQtChildBodyAutoHideHelper::CornerStyle_B );
    helper->setCornerStyle ( QQtChildBodyAutoHideHelper::RightBottom, QQtChildBodyAutoHideHelper::CornerStyle_B );
    ui->widget->installEventFilter ( helper );
}

MainWindow::~MainWindow()
{
    delete ui;
}
