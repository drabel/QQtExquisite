#ifndef TESTFILTEROBJECT2_H
#define TESTFILTEROBJECT2_H

#include <QObject>

class TestFilterObject2 : public QObject
{
    Q_OBJECT

public:
    explicit TestFilterObject2 ( QObject* parent = 0 );
    virtual ~TestFilterObject2();

protected:

private:


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // TESTFILTEROBJECT2_H

