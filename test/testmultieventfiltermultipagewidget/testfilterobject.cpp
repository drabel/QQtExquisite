#include <testfilterobject.h>
#include <QPainter>
#include <QWidget>

TestFilterObject::TestFilterObject ( QObject* parent ) : QObject ( parent ) {}

TestFilterObject::~TestFilterObject() {}

bool TestFilterObject::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    switch ( event->type() )
    {
        case QEvent::Paint:
        {
            QWidget* target = qobject_cast<QWidget*> ( watched );
            QPainter painter ( target );
            int w = 10;
            painter.setPen ( Qt::black );
            QRect srcRect = target->rect();
            QRect tarRect = QRect ( srcRect.left() + w / 2, srcRect.top() + w / 2, srcRect.width() - w, srcRect.height() - w );
            painter.drawRect ( tarRect );
            //left top
            painter.drawRect ( 0, 0, w, w );
            //right top
            painter.drawRect ( target->size().width() - w, 0, w, w );
            //left bottom
            painter.drawRect ( 0, target->size().height() - w, w, w );
            //right bottom
            painter.drawRect ( target->size().width() - w, target->size().height() - w, w, w );
            //left
            painter.drawRect ( 0, target->size().height() / 2 - w / 2, w, w );
            //right
            painter.drawRect ( target->size().width() - w, target->size().height() / 2 - w / 2, w, w );
            //top
            painter.drawRect ( target->size().width() / 2 - w / 2, 0, w, w );
            //bottom
            painter.drawRect ( target->size().width() / 2 - w / 2, target->size().height() - w, w, w );
            return false;
        }
        break;
        default:
            break;
    }
    return QObject::eventFilter ( watched, event );
}
