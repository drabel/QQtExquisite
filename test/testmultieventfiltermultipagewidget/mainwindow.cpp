#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtmultieventfiltermultipagewidget.h>
#include <testcatcherobject.h>
#include <testfilterobject.h>

#include <qqtframe.h>
#include <qqtbodymousewheelscalingeffect.h>
#include <qqtchildbodymover.h>
#include <qqtbodyresizer.h>

#include <testfilterobject2.h>
MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->setPixmap ( conf_root ( "logo.png" ) );
    ui->widget->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget->installEventFilter ( new TestFilterObject ( this ) );
    ui->widget->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget->setContentsMargins ( 5, 5, 5, 5 );
    ui->widget->setMouseTracking ( true );

    ui->widget->installEventCatcher ( new TestCatcherObject ( this ) );
    ui->widget->installEventCatcher ( new QQtChildBodyMover ( this ) );

    ui->widget_2->setPixmap ( conf_root ( "logo.png" ) );
    ui->widget_2->installEventFilter ( new TestFilterObject2 ( this ) );
}

MainWindow::~MainWindow()
{
    delete ui;
}
