#ifndef TESTFILTEROBJECT_H
#define TESTFILTEROBJECT_H

#include <QObject>
#include <QEvent>

class TestFilterObject : public QObject
{
    Q_OBJECT

public:
    explicit TestFilterObject ( QObject* parent = 0 );
    virtual ~TestFilterObject();

protected:

private:


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // TESTFILTEROBJECT_H

