#ifndef TESTCATCHEROBJECT_H
#define TESTCATCHEROBJECT_H

#include <QObject>
#include <QEvent>

class TestCatcherObject : public QObject
{
    Q_OBJECT

public:
    explicit TestCatcherObject ( QObject* parent = 0 );
    virtual ~TestCatcherObject();

protected:

private:


    // QObject interface
public:
    virtual bool eventFilter ( QObject* watched, QEvent* event ) override;
};

#endif // TESTCATCHEROBJECT_H

