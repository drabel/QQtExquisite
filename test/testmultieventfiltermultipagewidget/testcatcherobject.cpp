#include <testcatcherobject.h>

#include <QPainter>
#include <QWidget>

TestCatcherObject::TestCatcherObject ( QObject* parent ) : QObject ( parent ) {}

TestCatcherObject::~TestCatcherObject() {}

bool TestCatcherObject::eventFilter ( QObject* watched, QEvent* event )
{
    if ( !watched->inherits ( "QWidget" ) )
        return QObject::eventFilter ( watched, event );

    //修复鼠标穿透。
    bool atti = ( qobject_cast<QWidget*> ( watched ) )->testAttribute ( Qt::WA_TransparentForMouseEvents );
    if ( atti )
        return QObject::eventFilter ( watched, event );

    switch ( event->type() )
    {
        case QEvent::Paint:
        {
            QWidget* target = qobject_cast<QWidget*> ( watched );

            QPainter painter ( target );
            int w = 10;
            painter.setPen ( Qt::black );
            QRect srcRect = target->rect();
            QRect tarRect = QRect ( 100, 100, 200, 200 );
            painter.drawRect ( tarRect );
            return false;
        }
        break;
        default:
            break;
    }
    return QObject::eventFilter ( watched, event );
}
