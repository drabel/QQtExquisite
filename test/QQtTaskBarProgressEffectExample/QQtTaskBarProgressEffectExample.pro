TEMPLATE = app

QT += core gui widgets

TARGET = QQtTaskBarProgressEffectExample

include($$PWD/../../multi-link/add_base_manager.pri)
add_deploy()
add_custom_dependent_manager(QQtTaskBarProgressEffect)
system(touch main.cpp)

HEADERS += \
	widget.h

SOURCES += \
	main.cpp \
	widget.cpp

FORMS += \
	widget.ui

message($$TARGET $$DEFINES)
message($$TARGET $$CONFIG)
