﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtframe.h>
#include <qqt3dmultipagewidget.h>
#include <qqtchildbodymover.h>
#include <qqtbodyresizer.h>
#include <qqt3dselectedstyle.h>
#include <qqtbodymousewheelscalingeffect.h>
#include <qqtdragtopnghelper.h>
#include <qqtbodyclipboardhelper.h>
QQt3DSelectedStyle* s0;

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->backImage() = QImage ( conf_root ( "a1.png" ) );
    ui->widget->overlayImage() = QImage ( conf_root ( "a2.png" ) );
    ui->widget->foreImage() = QImage ( conf_root ( "a3.png" ) );

    ui->widget->setAcceptDrops ( true );
    ui->widget->installEventFilter ( new QQtDragToPngHelper ( this ) );
    ui->widget->installEventFilter ( new QQtBodyClipBoardHelper ( this ) );
    //ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget->setMouseTracking ( true );
    ui->widget->setContentsMargins ( 5, 5, 5, 5 );
    s0 = new QQt3DSelectedStyle ( this );
    ui->widget->installEventFilter ( s0 );
    ui->widget->setFocusPolicy ( Qt::StrongFocus );
    ui->widget;



    ui->horizontalSlider->setRange ( 0, 100 );
    ui->horizontalSlider_2->setRange ( 0, 100 );
    ui->horizontalSlider_3->setRange ( 0, 100 );
    ui->horizontalSlider_4->setRange ( 0, 100 );
    ui->horizontalSlider_5->setRange ( 0, 100 );

    ui->horizontalSlider->setValue ( 50 );
    ui->horizontalSlider_2->setValue ( 50 );
    ui->horizontalSlider_3->setValue ( 0 );
    ui->horizontalSlider_4->setValue ( 0 );
    ui->horizontalSlider_5->setValue ( 0 );

    ui->checkBox->setChecked ( true );
    ui->checkBox_2->setChecked ( true );

    ui->radioButton->setChecked ( true );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_radioButton_toggled ( bool checked )
{
    s0->setSelectedStyle ( QQt3DSelectedStyle::SelectedStyle_QtDesigner );
}

void MainWindow::on_radioButton_2_toggled ( bool checked )
{
    s0->setSelectedStyle ( QQt3DSelectedStyle::SelectedStyle_QRCodeScaner );
}

void MainWindow::on_radioButton_3_toggled ( bool checked )
{
    s0->setSelectedStyle ( QQt3DSelectedStyle::SelectedStyle_DottedLine );
}

void MainWindow::on_horizontalSlider_valueChanged ( int value )
{
    //basePoint X
    qreal x_r = ( qreal ) value / 100;
    ui->widget->set_original_point_x_ratio ( x_r );
    ui->xposr->setText ( QString::number ( x_r ) );
    ui->xpos->setText ( QString::number ( ui->widget->width() *x_r ) );
}

void MainWindow::on_horizontalSlider_2_valueChanged ( int value )
{
    //basePoint Y
    qreal y_r = ( qreal ) value / 100;
    ui->widget->set_original_point_y_ratio ( y_r );
    ui->yposr->setText ( QString::number ( y_r ) );
    ui->ypos->setText ( QString::number ( ui->widget->height() *y_r ) );
}

void MainWindow::on_horizontalSlider_3_valueChanged ( int value )
{
    //x
    qreal x_d = ( qreal ) value / 100 * 360;
    ui->widget->set_degree_of_axis_x ( x_d );
    ui->degX->setText ( QString::number ( x_d ) );
}

void MainWindow::on_horizontalSlider_4_valueChanged ( int value )
{
    //y
    qreal y_d = ( qreal ) value / 100 * 360;
    ui->widget->set_degree_of_axis_y ( y_d );
    ui->degY->setText ( QString::number ( y_d ) );
}

void MainWindow::on_horizontalSlider_5_valueChanged ( int value )
{
    //z
    qreal z_d = ( qreal ) value / 100 * 360;
    ui->widget->set_degree_of_axis_z ( z_d );
    ui->degZ->setText ( QString::number ( z_d ) );
}

void MainWindow::on_checkBox_toggled ( bool checked )
{
    //back 3d
    ui->widget->set_3d_for_background ( checked );
}

void MainWindow::on_checkBox_2_toggled ( bool checked )
{
    //fore 3d
    ui->widget->set_3d_for_foreground ( checked );
}
