﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow ( QWidget* parent = 0 );
    ~MainWindow();

private slots:
    void on_radioButton_toggled ( bool checked );

    void on_radioButton_2_toggled ( bool checked );

    void on_radioButton_3_toggled ( bool checked );

private slots:
    void on_horizontalSlider_valueChanged ( int value );

    void on_horizontalSlider_2_valueChanged ( int value );

    void on_horizontalSlider_3_valueChanged ( int value );

    void on_horizontalSlider_4_valueChanged ( int value );

    void on_horizontalSlider_5_valueChanged ( int value );

    void on_checkBox_toggled ( bool checked );

    void on_checkBox_2_toggled ( bool checked );

private:
    Ui::MainWindow* ui;
};

#endif // MAINWINDOW_H
