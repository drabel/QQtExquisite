﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qqtframe.h>
#include <qqtchildbodymover.h>
#include <qqtbodyresizer.h>
#include <qqtbodyselectedstyle.h>
#include <qqtbodymousewheelscalingeffect.h>

#include <qqtdragtopnghelper.h>
#include <qqtbodydragdropimagehelper.h>
#include <qqtdragfromfilehelper.h>

#include <qqtcore.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->setPixmap ( conf_root ( "logo.png" ) );
    ui->widget->setAcceptDrops ( true );
    QQtDragToPngHelper* effect = new QQtDragToPngHelper ( this );
    ui->widget->installEventFilter ( effect );


    QQtBodyDragDropImageHelper* d0 = new QQtBodyDragDropImageHelper ( this );
    ui->widget_2->installEventFilter ( d0 );
    ui->widget_2->setAcceptDrops ( true );

    QQtDragFromFileHelper* e1 = new QQtDragFromFileHelper ( this );
    ui->widget_3->installEventFilter ( e1 );
    ui->widget_3->setAcceptDrops ( true );
    connect ( e1, SIGNAL ( dragFileName ( QString, Qt::DropActions ) ),
              this, SLOT ( receivedFile ( QString, Qt::DropActions ) ) );


    //ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget->setMouseTracking ( true );
    ui->widget->setContentsMargins ( 5, 5, 5, 5 );
    ui->widget->installEventFilter ( new QQtBodySelectedStyle ( this ) );
    ui->widget->setFocusPolicy ( Qt::StrongFocus );

    //ui->widget_2->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget_2->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget_2->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget_2->setMouseTracking ( true );
    ui->widget_2->setContentsMargins ( 5, 5, 5, 5 );
    ui->widget_2->installEventFilter ( new QQtBodySelectedStyle ( this ) );
    ui->widget_2->setFocusPolicy ( Qt::StrongFocus );

    ui->widget_3->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget_3->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget_3->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget_3->setMouseTracking ( true );
    ui->widget_3->setContentsMargins ( 5, 5, 5, 5 );
    ui->widget_3->installEventFilter ( new QQtBodySelectedStyle ( this ) );
    ui->widget_3->setFocusPolicy ( Qt::StrongFocus );

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::receivedFile ( QString filename, Qt::DropActions action )
{
    pline() << filename << action;
}
