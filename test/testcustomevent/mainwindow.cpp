﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <testcustomevent.h>
#include <QDebug>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );
    installEventFilter ( this );
}

MainWindow::~MainWindow()
{
    delete ui;
}


bool MainWindow::eventFilter ( QObject* watched, QEvent* event )
{
    if ( event->type() != QEvent::Paint && event->type() != TestCustomEvent::eventType() )
        return QMainWindow::eventFilter ( watched, event );

    static int i = 0;
    qDebug() << i++ << event->type() << watched->objectName();

    if ( event->type() == TestCustomEvent::eventType() )
    {
        TestCustomEvent* e = ( TestCustomEvent* ) event;
        qDebug() << "after paint:" << event->type() << watched->objectName() << e->getString();
        return false;
    }

    if ( event->type() == QEvent::Paint )
    {
        TestCustomEvent* ce0 = new TestCustomEvent();
        ce0->setString ( "Paint: post event: " + QString::number ( ce0->type() ) + " " + QString::number ( i ) );
        QCoreApplication::postEvent ( watched, ce0 );
        return false;
    }

    return QMainWindow::eventFilter ( watched, event );
}
