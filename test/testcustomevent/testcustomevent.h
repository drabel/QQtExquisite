﻿#ifndef CUSTOMEVENT_H
#define CUSTOMEVENT_H

#include <QEvent>
#include <QString>

class TestCustomEvent : public QEvent
{
    Q_GADGET
public:
    explicit TestCustomEvent();
    virtual ~TestCustomEvent();

    void setString ( QString testString ) {
        mString = testString;
    }
    QString getString() {
        return mString;
    }

    static Type eventType() {
        if ( m_EventType == QEvent::MaxUser )
            m_EventType = ( QEvent::Type ) registerEventType();
        return m_EventType;
    }
private:
    static Type m_EventType;

    QString mString;
};

#endif
