﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtframe.h>
#include <qqtmultipagewidget.h>
#include <qqtchildbodymover.h>
#include <qqtbodyresizer.h>
#include <qqtselectedstyle.h>
#include <qqtbodymousewheelscalingeffect.h>
QQtSelectedStyle* s0;

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->backImage() = QImage ( conf_root ( "a1.png" ) );
    ui->widget->overlayImage() = QImage ( conf_root ( "a2.png" ) );
    ui->widget->foreImage() = QImage ( conf_root ( "a3.png" ) );

    ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget->setMouseTracking ( true );
    ui->widget->setContentsMargins ( 5, 5, 5, 5 );
    s0 = new QQtSelectedStyle ( this );
    ui->widget->installEventFilter ( s0 );
    ui->widget->setFocusPolicy ( Qt::StrongFocus );
    ui->widget;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_radioButton_toggled ( bool checked )
{
    s0->setSelectedStyle ( QQtSelectedStyle::SelectedStyle_QtDesigner );
}

void MainWindow::on_radioButton_2_toggled ( bool checked )
{
    s0->setSelectedStyle ( QQtSelectedStyle::SelectedStyle_QRCodeScaner );
}

void MainWindow::on_radioButton_3_toggled ( bool checked )
{
    s0->setSelectedStyle ( QQtSelectedStyle::SelectedStyle_DottedLine );
}
