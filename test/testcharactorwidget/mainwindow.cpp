#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtframe.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    QImage image ( conf_root ( "timg.jpeg" ) );

    ui->widget->setCharactorImage ( image );
    ui->widget_2->setImage ( image );

    QString giffile = conf_root ( "a.gif" ) ;
    ui->widget_3->setGifFile ( giffile );
    ui->widget_4->setGifFile ( giffile );
}

MainWindow::~MainWindow()
{
    delete ui;
}
