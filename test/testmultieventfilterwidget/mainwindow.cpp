#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtbodyselectedstyle.h>
#include <qqtframe.h>
#include <qqt3dbodyselectedstyle.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->setPixmap ( conf_root ( "logo.png" ) );
    ui->widget->installEventFilter ( new QQtBodySelectedStyle ( this ) );
    ui->widget->setFocusPolicy ( Qt::StrongFocus );

    ui->widget_2->setPixmap ( conf_root ( "logo.png" ) );
    ui->widget_2->installEventCatcher ( new QQtBodySelectedStyle ( this ) );
    ui->widget_2->setFocusPolicy ( Qt::StrongFocus );

    //3d
    ui->widget_3->setPixmap ( conf_root ( "logo.png" ) );
    QQt3DBodySelectedStyle* style = new QQt3DBodySelectedStyle ( this );
    ui->widget_3->installEventCatcher ( style );
    ui->widget_3->setFocusPolicy ( Qt::StrongFocus );

    ui->widget_3->set_original_point_ratio ( 0.5, 0.5 );
    ui->widget_3->set_degree_of_axis_x ( 30 );
    ui->widget_3->set_degree_of_axis_y ( 30 );
    ui->widget_3->set_degree_of_axis_z ( 30 );

    style->set_original_point_ratio ( 0.5, 0.5 );
    style->set_degree_of_axis_x ( 30 );
    style->set_degree_of_axis_y ( 30 );
    style->set_degree_of_axis_z ( 30 );
}

MainWindow::~MainWindow()
{
    delete ui;
}
