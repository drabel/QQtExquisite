#include "mainwindow.h"
#include "ui_mainwindow.h"


#include <qqtcarouselimagewindow.h>
#include <qqtframe.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    QQtCarouselImageWindow& w = * ( ui->widget );
    w.addImage ( conf_root ( "a1.png" ) );
    w.addImage ( conf_root ( "a2.png" ) );
    w.addImage ( conf_root ( "a3.png" ) );
    w.addImage ( conf_root ( "a4.png" ) );
    w.addImage ( conf_root ( "a5.png" ) );
    w.startPlay();
    //w.show();
}

MainWindow::~MainWindow()
{
    delete ui;
}
