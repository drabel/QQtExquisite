﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtframe.h>
#include <qqtcore.h>
#include <qqtbodyclipboardhelper.h>

#include <qqtchildbodymover.h>
#include <qqtbodyresizer.h>
#include <qqtbodyselectedstyle.h>
#include <qqtbodymousewheelscalingeffect.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->installEventFilter ( new QQtBodyClipBoardHelper ( this ) );
    ui->widget_2->installEventFilter ( new QQtBodyClipBoardHelper ( this ) );
    ui->widget_2->setPixmap ( conf_root ( "logo.png" ) );

    ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget->setMouseTracking ( true );
    ui->widget->setContentsMargins ( 5, 5, 5, 5 );
    ui->widget->installEventFilter ( new QQtBodySelectedStyle ( this ) );
    ui->widget->setFocusPolicy ( Qt::StrongFocus );

    ui->widget_2->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget_2->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget_2->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget_2->setMouseTracking ( true );
    ui->widget_2->setContentsMargins ( 5, 5, 5, 5 );
    ui->widget_2->installEventFilter ( new QQtBodySelectedStyle ( this ) );
    ui->widget_2->setFocusPolicy ( Qt::StrongFocus );

}

MainWindow::~MainWindow()
{
    delete ui;
}
