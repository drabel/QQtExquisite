#include "mainwindow.h"
#include "ui_mainwindow.h"
//creator
#include <qpdfium.h>
//printer
#include <qpdfwriter.h>
#include <qqtcore.h>
MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );
    QPdfium pdf;
    pdf.loadFile ( "21.pdf" );
    pline() << pdf.status();
    QPdfiumPage page = pdf.page ( 0 );
    QImage img = page.image();
    ui->qqtwidget->setPixmap ( img );
}

MainWindow::~MainWindow()
{
    delete ui;
}
