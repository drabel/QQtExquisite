#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qqtcore.h>

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    helper = new QQtShakeHelper ( this );
    helper->setShakeWidget ( this );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    qDebug() << this->pos()
             << this->mapToGlobal ( pos() )
             << this->mapToParent ( pos() );

    qDebug() << this->geometry() << this->frameGeometry();
    helper->setShakeWidget ( this );
    helper->shake();
}

void MainWindow::on_pushButton_2_clicked()
{
    helper->endShake();
}
