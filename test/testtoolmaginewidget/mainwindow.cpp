﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtchildbodymover.h>
#include <qqtbodyresizer.h>
#include <qqtbodymousewheelscalingeffect.h>
#include <qqtbodyselectedstyle.h>
#include <qqtshakehelper.h>
QQtShakeHelper* sharkH;
MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget->installEventFilter ( new QQtChildBodyMover ( this ) );
    ui->widget->installEventFilter ( new QQtBodyResizer ( this ) );
    ui->widget->installEventFilter ( new QQtBodyMouseWheelScalingEffect ( this ) );
    ui->widget->setMouseTracking ( true );
    ui->widget->setContentsMargins ( 5, 5, 5, 5 );

    //ui->widget->installEventFilter ( new QQtBodySelectedStyle ( this ) );
    //ui->widget->setFocusPolicy ( Qt::StrongFocus );
    //centralWidget()->setFocusPolicy ( Qt::StrongFocus );

    ui->widget->setToolPairNum ( 16 );

    ui->verticalSlider->setRange ( 0, 360 );
    ui->verticalSlider->setValue ( 0 );

    sharkH = new QQtShakeHelper ( this );
    sharkH->setShakeWidget ( ui->widget );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_verticalSlider_valueChanged ( int value )
{
    ui->widget->setCurRatate ( value );
    ui->widget->update();
}

void MainWindow::on_pushButton_clicked()
{
    sharkH->setShakeWidget ( ui->widget );
    sharkH->shake();
}

void MainWindow::on_pushButton_2_clicked()
{
    sharkH->endShake();
}
