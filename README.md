# QQt应用程序框架中间件   

中文全名 ****QQt应用程序框架中间件****。  
英文全名 ****QQt Application Framework Middleware****，英文曾用名QQt Foundation Class。  
英文简称 ****LibQQt****。  
主库简称 ****LibQQt****。  
*QQt应用程序框架中间件是应用程序框架层的一系列库集合，持续实现这一层的功能。主库是LibQQt，辅助（平级扩展）库有QQtExquisite、QQtHighGrade、QQtIndustrialControl、QQtInput、QQtInstallFramework、QQtMediaExtention、QQtStyle、QQtTool等，很多流行Qt Wrapper库都在辅助库中。全部使用Multi-link技术实施工程管理。*    

# 辅助库 QQtExquisite

## 项目介绍  

为App准备的精美控件和模块，属于QQt的平级扩展。    
1. 全面由精美控件组成，他们被编译成相应的库，而不是完全集成为一个QQtExquisite库。  
2. 全面通过Multi-link技术生成和调用，每个库都做了编译修正，支持桌面和嵌入式平台。  
3. QQtExquisite依赖LibQQt。    

## 项目组成  

- Log4Qt  
- QtXlsxWriter  
- QtPdfium   
- Quc  
- Qwt  
- QwtPlot3D  
- QQtExquisite   

#### 未详细测试   
- CuteReport  
- QQtCSVLib  
- QQtTaskBarProgressEffect  
- QtAddressBar  

## 联系我  

QQ:2657635903  
邮箱：tianduanrui@163.com  

